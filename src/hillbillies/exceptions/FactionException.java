package hillbillies.exceptions;

import hillbillies.model.Faction;

public class FactionException extends WorldException{
	
	private static final long serialVersionUID = 11L;
	private final Faction faction;
	
	public FactionException(Faction faction){
		
		super("Cannot add faction");
		this.faction = faction;
	}
	

}
