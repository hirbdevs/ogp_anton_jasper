package hillbillies.exceptions;

public class GameObjectException extends Exception{
	
	
	private static final long serialVersionUID = 3L;
	
	private final Object object;
	
	public GameObjectException(Object object){
		super("GameObject Exception");
		this.object = object;
		
	}
	
}
