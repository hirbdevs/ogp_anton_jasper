package hillbillies.exceptions;

import be.kuleuven.cs.som.annotate.*;

/**
 * A class for signaling illegal inputs for the unit's name.
 * 
 * @author Jasper
 */
public class IllegalNameException extends UnitException{
		

	/**
	 * 
	 */
	private static final long serialVersionUID = 12L;

	public IllegalNameException (String unitName){
		super("Illegal Name given.");
		this.unitName = unitName;
	}
	
	private final String unitName;
	
	@Basic @Raw @Immutable 								
	public String getUnitName(){
		return this.unitName;
	}

}
