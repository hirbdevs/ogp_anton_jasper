package hillbillies.exceptions;
import be.kuleuven.cs.som.annotate.*;

/**
 * A class of exceptions signalling illegal co�rdinates in the game world.
 * 	Each illegal position exception involves the illegal co�rdinate.
 * @author Anton
 *
 */
public class IllegalPositionException extends Exception {
	
	private static final long serialVersionUID = 13L;

	public IllegalPositionException (double position){
		super("Illegal Position");
		this.position = position;
	}
	
	private final double position;
	
	@Basic @Raw @Immutable 
	public double getPosition(){
		return this.position;
	}
	
	
}
