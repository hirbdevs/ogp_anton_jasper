package hillbillies.exceptions;

public class IllegalStateException extends Exception{

	private static final long serialVersionUID = 2L;

	public IllegalStateException (String message){
		super(message);
	}
}
