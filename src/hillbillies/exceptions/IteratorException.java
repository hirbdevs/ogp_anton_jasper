package hillbillies.exceptions;

public class IteratorException extends Exception {

	private static final long serialVersionUID = 4L;

	public IteratorException (String message){
		super(message);
	}
}