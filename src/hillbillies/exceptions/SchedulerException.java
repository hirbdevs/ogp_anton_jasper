package hillbillies.exceptions;

public class SchedulerException extends Exception{

	private static final long serialVersionUID = 5L;

	public SchedulerException (String message){
		super(message);			
	}
}
