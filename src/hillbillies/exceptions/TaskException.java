package hillbillies.exceptions;

public class TaskException extends Exception {

	private static final long serialVersionUID = 6L;

	public TaskException (String message){
		super(message);			
	}

}
