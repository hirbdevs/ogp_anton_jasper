package hillbillies.exceptions;

public class TooManyFactionsException extends WorldException {

	private static final long serialVersionUID = 7L;

	public TooManyFactionsException(){
		super("Too many factions.");
	}
	
}
