package hillbillies.exceptions;



public class UnitException extends Exception{
	
	
	private static final long serialVersionUID = 8L;
	
	
	public UnitException (String message){
		super(message);			
	}
	
	
}
