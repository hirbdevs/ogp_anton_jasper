package hillbillies.exceptions;

public class WorldException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 9L;

	public WorldException (String message){
		super(message);
	}
}
