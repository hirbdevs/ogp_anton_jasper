package hillbillies.exceptions;

public class WrongStatementTypeException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 10L;

	public WrongStatementTypeException(String message){
		super(message);
	}
}


