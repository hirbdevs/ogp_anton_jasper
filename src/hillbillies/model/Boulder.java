package hillbillies.model;

import hillbillies.exceptions.GameObjectException;
import hillbillies.exceptions.IllegalPositionException;
import hillbillies.exceptions.UnitException;
import hillbillies.exceptions.WorldException;

/**
 * A class of boulders as a special case of GameObject.
 * 
 * @author Anton Van Gompel
 * @author Jasper Bergmans
 * @version	2.0
 * 
 */
public class Boulder extends Material implements IMaterial{	

    /**
     * A constructor for a Boulder. Constructs a Boulder at a given position.
     * 
     * @param 	position
     * 		The inititial position of the cube where the boulder will be positioned.
     * @effect	
     * 		The new boulder is initialized as a new GameObject with a given position.
     * 
     * @throws GameObjectException
     * @throws WorldException 
     * @throws IllegalPositionException 
     */	 
	public Boulder(double[] position) throws GameObjectException, WorldException, IllegalPositionException{
    	super (new Position(position[0],position[1], position[2] ));
    }

	/**
	 * @post
	 * 		The unit has dropped the boulder it was carrying at the given dropposition.
	 * @effect	
	 * 		The boulder's carrier is set to null
	 * @effect	
	 * 		The weight of the boulder is subtracted from the unit's weight.
	 * @effect	
	 * 		A unit will stop carrying this boulder.
	 * @effect 	
	 * 		The boulder will be added to the boulderset of the cube where the boulder is dropped.
	 * @effect
	 * 		The boulder is added to the set of boulders in this world. 
	 */
	@Override
	public void pickUp(Unit unit) throws UnitException, GameObjectException, WorldException {
		if (unit != null){
			World.getInstance().getCubeByPosition(this.currentPosition).removeBoulderFromBoulderSet(this);
			World.getInstance().removeBoulderFromSet(this);
			this.setCarrier(unit);
			unit.carryMaterial(this);
			unit.setWeightMaterial(this.getWeight());
		}
		else
			throw new UnitException("Not a valid unit");
	}

	/**
	 * @post
	 * 		The unit has dropped the boulder it was carrying at the given dropposition.
	 * @effect	
	 * 		The boulder's carrier is set to null
	 * @effect	
	 * 		The weight of the boulder is subtracted from the unit's weight.
	 * @effect	
	 * 		A unit will stop carrying this boulder.
	 * @effect 	
	 * 		The boulder will be added to the boulderset of the cube where the boulder is dropped.
	 * @effect
	 * 		The boulder is added to the set of boulders in this world.
	 */
	@Override
	public void drop(Unit unit, Cube dropPosition) throws UnitException, WorldException, GameObjectException {
		if (unit != null){
			if(unit.isCarryingMaterial() && unit.getBoulder()!=null){
				if (dropPosition == null)
					throw new WorldException("Not a valid cube to drop the boulder in.");
				else {
					this.setCarrier(null);
					unit.carryMaterial(null);
					unit.setWeightMaterial(0);
					this.setObjectPosition(dropPosition.getPositionX() + World.LENGTH_CUBE / 2,
							dropPosition.getPositionY() + World.LENGTH_CUBE / 2,
							dropPosition.getPositionZ() + World.LENGTH_CUBE / 2);
					World.getInstance().addBoulderToSet(this);
				} 
			}
			else
				throw new UnitException("Unit is not carrying boulder.");
			} else throw new UnitException("Not a valid Unit.");
	}

	/**
	 * @post
	 * 		The boulder is no longer part of the game.
	 * @effect	
	 * 		The boulder is removed from the set of boulders in the world.
	 * @effect	
	 * 		The boulder is removed from its current boulderset.
	 */
	@Override
	public void consume() throws GameObjectException {
		World.getInstance().removeBoulderFromSet(this);
		World.getInstance().getCubeByPosition(this.currentPosition).removeBoulderFromBoulderSet(this);
		
	}

}