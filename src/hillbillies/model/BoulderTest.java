package hillbillies.model;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import hillbillies.exceptions.GameObjectException;
import hillbillies.exceptions.IllegalPositionException;
import hillbillies.exceptions.UnitException;
import hillbillies.exceptions.WorldException;
import hillbillies.part2.listener.DefaultTerrainChangeListener;

public class BoulderTest {
	
	//World 10 X 10 X 10
	World world;
	static int[][][] cubeType = new int[10][10][10];
	DefaultTerrainChangeListener listener = new DefaultTerrainChangeListener();
	
	private static double [] positionArray1 = {1.5,2.5,3.5};
	private static double [] OutOfBoundsArray = {10,11,10};
	private static int [] PosArray3 = {1,2,3};

	Unit	testUnit, testUnit2;
	Boulder boulder, boulder2;
	Cube	dropPosition, illegalDropPosition;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		for (int x = 0; x<10; x++)
			for(int y = 0; y<10; y++)
				for(int z = 0; z<10; z++)
					cubeType[x][y][z] = 0;
	}
	
	@Before
	public void setUp() throws Exception {
		// World 10 X 10 X 10
		world = World.getInstance();
		World.getInstance().setTerrainChangeListener(listener);
		World.getInstance().setBoundaries(10, 10, 10);
		World.getInstance().setConnectedToBorder(World.MAXPOSITION_X, World.MAXPOSITION_Y, World.MAXPOSITION_Z);
		World.getInstance().setCubes(cubeType);
		
		testUnit = new Unit ("Jasper", PosArray3, 50,50,50,50, false);
		testUnit2 = new Unit ("Anton", PosArray3, 50,50,50,50, false);
		boulder = new Boulder(BoulderTest.positionArray1);
		boulder2 = new Boulder(BoulderTest.positionArray1);
		dropPosition = new Cube(0,1,1,1);
	}

	@Test
	public void constructor_LegalCase() throws GameObjectException, WorldException, IllegalPositionException{
		Boulder boulder2 = new Boulder(positionArray1);
		assertEquals(boulder2.getPositionGameObjectX(), 1.5, 0.05);
		assertEquals(boulder2.getPositionGameObjectY(), 2.5, 0.05);
		assertEquals(boulder2.getPositionGameObjectZ(), 3.5, 0.05);
	}
	
	@Test (expected=IllegalPositionException.class)
	public void constructor_IllegalCase() throws GameObjectException, WorldException, IllegalPositionException{
		Boulder boulder3 = new Boulder(BoulderTest.OutOfBoundsArray);
		//The only reason of the next line is to remove the warning message that var boulder3 is never used.
		boulder3.getCarrier();
	}
	
	@Test
	public void pickUpBoulder_Legal() throws GameObjectException, UnitException, WorldException{
		boulder.pickUp(testUnit);
		assertTrue(testUnit == boulder.getCarrier());
		assertEquals(testUnit.getWeight()+boulder.getWeight(),testUnit.getTotalWeight(),0.05);
		assertTrue(testUnit.isCarryingBoulder() == true);
		//TODO
		//assertFalse(World.getInstance().getCubeByPosition(	(int)testUnit.getPositionX(), 
		//													(int)testUnit.getPositionY(), 
		//													(int)testUnit.getPositionZ()).getBoulders().contains(boulder));
	}

	@Test (expected=UnitException.class)
	public void pickUpBoulder_Illegal() throws GameObjectException, UnitException, WorldException{
		boulder.pickUp(null);
	}
	
	@Test
	public void dropBoulder_Legal() throws GameObjectException, UnitException, WorldException{
		testUnit.carryMaterial(boulder);
		boulder.drop(testUnit, dropPosition);
		assertEquals(testUnit.getWeight()+testUnit.getWeightMaterial(),testUnit.getTotalWeight(),0.05);
		assertEquals(testUnit.getWeight()+testUnit.getWeightMaterial(),testUnit.getWeight(),0.05);
		assertTrue(testUnit.isCarryingBoulder() == false);
		assertEquals(boulder.getPositionGameObjectX(),dropPosition.getPositionX()+ World.LENGTH_CUBE/2,0.05);
		assertEquals(boulder.getPositionGameObjectY(),dropPosition.getPositionX()+ World.LENGTH_CUBE/2,0.05);
		assertEquals(boulder.getPositionGameObjectZ(),dropPosition.getPositionX()+ World.LENGTH_CUBE/2,0.05);
		//TODO
		//assertTrue(World.getInstance().getCubeByPosition(	(int)dropPosition.getPositionX(),
		//													(int)dropPosition.getPositionY(), 
		//													(int)dropPosition.getPositionZ()).getBoulders().contains(boulder));
	}
	
	@Test (expected=GameObjectException.class)
	public void dropBoulder_LegalCheckCarrier() throws GameObjectException, UnitException, WorldException{
		testUnit.carryMaterial(boulder);
		boulder.drop(testUnit, dropPosition);
		boulder.getCarrier();
	}
	
//TODO	
//	@Test (expected=IllegalPositionException.class)
//	public void dropBoulder_IllegalOutOfBounds() throws GameObjectException, UnitException, WorldException, IllegalPositionException{
//		illegalDropPosition = new Cube(0,11,11,11);
//		boulder.dropBoulder(testUnit, illegalDropPosition);		
//	}
	
	@Test (expected=UnitException.class)
	public void dropBoulder_IllegalNullValueUnit() throws GameObjectException, UnitException, WorldException{
		boulder.drop(null, dropPosition);
	}
	
	@Test (expected=WorldException.class)
	public void dropBoulder_IllegalNullValuePosition() throws GameObjectException, UnitException, WorldException{
		testUnit.carryMaterial(boulder);
		boulder.drop(testUnit, null);
	}
	
	@Test
	public void consumeBoulder_Legal() throws GameObjectException{
		boulder.consume();
		assertFalse(World.getInstance().boulderSet.contains(boulder));
		assertFalse(World.getInstance().getCubeByPosition(boulder.currentPosition).boulderSet.contains(boulder));
	}
}
