package hillbillies.model;

/**
 * A class to implement the different conditions for the checkConditions method in Scheduler.
 * 
 * @author Jasper Bergmans
 * @Version 1.0
 */
public class Condition {

	/**
	 * Constructor: creates a new condition for the given scheduler.
	 * @param thisScheduler
	 * 		The scheduler to which this condition belongs.
	 */
	Condition(Scheduler thisScheduler){
		scheduler = thisScheduler;
	}
	
	/**
	 * The scheduler that uses this condition object.
	 */
	public final Scheduler scheduler;
	
	/**
	 * Returns a boolean reflecting whether the given task has a positive priority or not.
	 * @param taskToCheck
	 * 		To task of which the priority-value must be checked to be positive.
	 * @return
	 * 		True if and only if the given task has a priority larger of equal then zero.
	 */
	public boolean hasPositivePriority(Task taskToCheck){
		return(taskToCheck.getPriority()>=0);
	}
	
	/**
	 * Returns a boolean reflecting whether the given task has a unit assigned to it.
	 * @param taskToCheck
	 * 		The task of which that needs to checked if a unit is assigned or not.
	 * @return
	 * 		True if and only is the given task has a unit assigned to it.
	 */
	public boolean isAssigned(Task taskToCheck){
		return(taskToCheck.isAssigned());
	}
}
