package hillbillies.model;

import java.util.HashSet;
import java.util.Set;
import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Raw;
import hillbillies.exceptions.GameObjectException;
import hillbillies.exceptions.IllegalPositionException;
import hillbillies.exceptions.WorldException;
import ogp.framework.util.Util;

public class Cube {

	/**
	 * Contains the position of the cube.
	 */
	private Position positionCube;
	
	private TerrainType type;
	
	/**
	 * Contains all the unit's occupying a cube;
	 */
	public Set<Unit> unitSet;
	
	/**
	 * Set of boulders in this cube.
	 */

	public Set<Boulder> boulderSet;

	/**
	 * Set of logs in this cube.
	 */
	public Set<Log> logSet;
	
	/**
	 * The time that has passed since the cube has collapsed.
	 */
	private double TimeSinceCollapse = 0;
	
	/**
	 * True if the cube is caving in.
	 */
	private boolean isCavingIn = false;
 
	/**
	 * Method to construct a cube with the given terraintype and the given position as parameters.
	 * @param type
	 * 		the TerrainType of a cube
	 * @param x
	 * 		the x coordinate of the cube
	 * @param y
	 * 		the y coordinate of the cube
	 * @param z
	 * 		the z coordinate of the cube
	 * @throws IllegalPositionException 
	 * @throws WorldException 
	 * @throws GameObjectException 
	 * @effect Sets the position of the cube to the given x,y,z coordinates.
	 * @effect Sets the terrainType of the cube.
	 */
	public Cube (int type, int x, int y, int z) throws IllegalPositionException, WorldException{
		
		this.setPosition(x, y, z);
		
		this.setTerrainType(type);
		
		unitSet = new HashSet<Unit>();
		boulderSet = new HashSet<Boulder>();
		logSet = new HashSet<Log>();
	}
	
	/**
	 * Controls the timed behaviour of a cube.
	 * @param GameTime
	 * 		the time that needs to be added.
	 * @throws GameObjectException 
	 * @throws WorldException 
	 * @throws IllegalPositionException 
	 */
	public void advanceTime(double GameTime) throws GameObjectException, WorldException, IllegalPositionException{
		
		if (Util.fuzzyEquals(TimeSinceCollapse, getCaveInTime(), 0.1)){
			this.caveIn();
			this.isCavingIn = false;
			TimeSinceCollapse = 0;
		}
		
		if (this.isSolidCube()){
			if (!this.isConnectedCube() || this.isCavingIn){
				this.isCavingIn = true;
				this.TimeSinceCollapse += GameTime;
			}}

	}
	
	/**
	 * Returns the time it takes for a Cube to cave in.
	 * @return
	 */
	@Basic @Raw
	private double getCaveInTime(){
		return Math.random()*4+1;
	}
	
	/**
	 * Returns the X coordinate of the cube's position.
	 * @return
	 */
	@Basic @Raw
	public double getPositionX(){
		return this.positionCube.getPositionX();
	}
	
	/**
	 * Returns the Y coordinate of the cube's position.
	 * @return
	 */
	@Basic @Raw
	public double getPositionY(){
		return this.positionCube.getPositionY();
	}
	
	/**
	 * Returns the Z coordinate of the cube's position.
	 * @return
	 */
	@Basic @Raw
	public double getPositionZ(){
		return this.positionCube.getPositionZ();
	}
	
	@Basic @Raw
	public Position getPosition(){
		return this.positionCube;
	}
	
	/**
	 * Sets the position of the cube to the given coordinates x,y,z.
	 * @param x
	 * 		the x coordinate
	 * @param y
	 * 		the y coordinate
	 * @param z
	 * 		the z coordinate
	 * @throws IllegalPositionException
	 * @effect the position of the cube is set to the given coordinates.
	 */
	@Raw
	private void setPosition(int x, int y, int z) throws IllegalPositionException{
		if (this.positionCube == null)
			positionCube = new Position(x,y,z);
		else {
			this.positionCube.setPositionX((double)x);
			this.positionCube.setPositionY((double)y);
			this.positionCube.setPositionZ((double)z);
		}
	}
	
	/**
	 * Returns the terraintype of the cube.
	 * @return the TerrainType of the cube.
	 */
	@Basic @Raw
	public TerrainType getTerrainType(){
		return this.type;
	}
	
	/**
	 * Returns the integer that corresponds to the terraintype of the cube.
	 * @return integer value of the terraintype of the cube.
	 */
	@Basic @Raw
	public int getTerrainTypeNumber(){
		return this.type.getNumber();
	}
	
	/**
	 * Sets the terraintype of the cube to the given value.
	 * @param type
	 * 		the terraintype that must be set.
	 * @throws GameObjectException
	 * @throws WorldException
	 * @Post terraintype of the cube is set to the given value.
	 * @effect notifies the TerrainTypeListener that the terraintype has changed.
	 */
	@Raw
	public void setTerrainType(TerrainType type) throws GameObjectException, WorldException{
		this.type = type;
		World.getInstance().notifyTerrainTypeChange(this, this.isPassableCube());
	}
	
	
	public void setTerrainType(int number) throws WorldException{
		if (number == 0)
			this.type = TerrainType.air;
		else if (number == 1)
			this.type = TerrainType.rock;
		else if (number == 2)
			this.type = TerrainType.tree;
		else if (number == 3)
			this.type = TerrainType.workshop;
		World.getInstance().notifyTerrainTypeChange(this, this.isPassableCube());
	}
	
	/**
	 * Returns true if the cube is solid.
	 * @return true if the terraintype is tree or rock, false otherwise.
	 */
	public boolean isSolidCube(){
        if (this.getTerrainType() == TerrainType.air)
            return false;
        else if (this.getTerrainType() == TerrainType.workshop)
            return false;
        else return true;
    }
	
	/**
	 * Returns true if the cube is passable.
	 * @return true if the terraintype is air or workshop, false otherwise.
	 */
	public boolean isPassableCube(){
        if (this.getTerrainType() == TerrainType.air)
            return true;
        else if (this.getTerrainType() == TerrainType.workshop)
            return true;
        else return false;
	}
	
	/**
	 * Returns true if the cube is connected to the border.
	 * @return
	 * @throws WorldException 
	 */
	public boolean isConnectedCube() throws WorldException{
		return World.getInstance().isCubeConnectedToBorder(this);
	}

	/**
	 * Adds the unit given as parameter to the set of units that belongs to the cube.
	 * @param unit
	 * 		the unit that needs to be added to the set.
	 * @throws WorldException
	 * 		if the given unit is null, throw new WorldException.
	 */
	public void addUnitToCubeSet(Unit unit) throws WorldException{
		if (unit != null)
			this.unitSet.add(unit);
		else throw new WorldException("");
	}
	
	/**
	 * Returns the units present in the cube if there are some, null otherwise.
	 * @return
	 */
	@Raw @Basic
	public Set<Unit> getUnits(){
		if (unitSet.isEmpty() != true)
			return unitSet;
		else 
			return null;
	}
	
	/**
	 * Removes a given unit from the set of units that are currently present in the cube.
	 * @param unit
	 * 		the unit that needs to be removed.
	 * @throws WorldException
	 * 		if the given unit is not in the set, throw new WorldException.
	 * @Post the given unit is not in unitSet.
	 */
	public void removeUnitFromCubeSet(Unit unit) throws WorldException{
		if (unitSet.contains(unit))
			unitSet.remove(unit);
		else throw new WorldException("This unit is not in this set.");
	}

	/**
	 * Returns true if there is a unit present, false otherwise.
	 * @return true if the unitSet is not empty, false otherwise.
	 */
	public boolean isUnitPresent(){
		if (unitSet.isEmpty() == true)
			return false;
		else
			return true;
	}	

	//BOULDERSET
	
	/**
	 * Adds the boulder given as parameter to the set of boulders.
	 * @param boulder
	 * 		the boulder that needs to be added to the set.
	 * @throws GameObjectException
	 * 		if the given boulder is null, throw new GameObjectException.
	 * @Post if the given boulder is not null, the boulder is present in boulderSetCube.
	 */
	public void addBoulderToBoulderSet(Boulder boulder) throws GameObjectException{
		if (boulder != null)
			this.boulderSet.add(boulder);
		else throw new GameObjectException(boulder);
	}
	
	/**
	 * Removes the boulder given as parameter from the set of boulders.
	 * @param boulder
	 * 		the boulder that needs to be removed from the set.
	 * @throws GameObjectException
	 * 		if the boulderSetCube does not contain the boulder, throw new GameObjectException.
	 * @Post the given boulder is not in boulderSetCube.
	 */
	public void removeBoulderFromBoulderSet(IMaterial boulder) throws GameObjectException{
		if (boulderSet.contains(boulder))
			boulderSet.remove(boulder);
	}
	
	/**
	 * Returns a boulder if there is a boulder present in the cube, null otherwise.
	 * @return
	 */
	@Raw @Basic
	public Boulder getBoulder(){
		if(!boulderSet.isEmpty())
			return boulderSet.iterator().next();
		else return null;
	}
	
	/**
	 * Returns the boulders that are currently present in the cube.
	 * @return
	 */
	@Raw @Basic
	public Set<Boulder> getBoulders(){
		if (boulderSet.isEmpty()!= true)
			return boulderSet;
		else
			return null;
	}

	/**
	 * Returns true if a boulder is present in the cube.
	 * @return
	 */
	public boolean isBoulderPresent(){
		if (boulderSet.isEmpty() ==  true)
			return false;
		else
			return true;
	}

	/**
	 * Adds the log given as parameter to the set of logs that are present in the cube.
	 * @param log
	 * 		the log that needs to be added to the set.
	 * @throws GameObjectException
	 * 		if the given log is null, throw new GameObjectException.
	 * @Post if the given log is not null, it is present in logSet. 
	 */
	public void addLogToLogSet(Log log) throws GameObjectException{
		if (log != null)
			this.logSet.add(log);
		else throw new GameObjectException(log);
	}
	
	/**
	 * Removes the log given as parameter from the set of logs that are present in the cube.
	 * @param log
	 * 		the log that needs to be removed from the set.
	 * @throws GameObjectException
	 * 		if the given log is not in logSet, throw new GameObjectException.
	 * @Post the given log is not in logSet.
	 */
	public void removeLogFromLogSet(Log log) throws GameObjectException{
		if (logSet.contains(log))
			logSet.remove(log);
	}
	
	/**
	 * Returns the log if there is one present in the cube, null otherwise.
	 * @return
	 */
	@Raw @Basic
	public Log getLog(){
		if(!logSet.isEmpty())
			return logSet.iterator().next();
		else return null;
	}
	
	/**
	 * Returns the set of logs that are present in the cube.
	 * @return
	 */
	@Raw
	public Set<Log> getLogs(){
		if (logSet.isEmpty()!= true)
			return logSet;
		else
			return null;
	}
	
	/**
	 * Returns true if a log is present in the cube.
	 * @return
	 */
	public boolean isLogPresent(){
		if (logSet.isEmpty() ==  true)
			return false;
		else
			return true;
	}
	
	/**
	 * Returns the possibility a material is created when a cube collapses.
	 * @return
	 */
	private boolean materialCreated() {
        return (Math.random() <= 0.25); 
     }
	
	/**
	 * This method lets the cube cave in.
	 * @throws GameObjectException
	 * @throws WorldException
	 * @throws IllegalPositionException 
	 * @Post if the terraintype is rock and this.materialCreated() is true, a new boulder is created and added to the cube.
	 * @Post if the terraintype is tree and this.materialCreated() is true, a new log is created and added to the cube.
	 * @effect the terraintype of the cube is changed to air.
	 */
	public void caveIn() throws GameObjectException, WorldException, IllegalPositionException{
        if (this.isSolidCube()){
            if (this.getTerrainType() == TerrainType.rock){
                if (this.materialCreated()){
                    Boulder boulder = new Boulder(new double[]{	(this.getPositionX()) + (World.LENGTH_CUBE/2),
                    										 	(this.getPositionY()) + (World.LENGTH_CUBE/2),
                    										 	(this.getPositionZ()) + (World.LENGTH_CUBE/2)});
                    World.getInstance().addBoulderToSet(boulder);
                    //add to cube
                    
                    World.getInstance().notifyTerrainTypeChange(this, true);
                    this.setTerrainType(0);
                    this.addBoulderToBoulderSet(boulder);
                }
                else
                    this.setTerrainType(0);
            }
            
            if (this.getTerrainType() == TerrainType.tree){
                if (this.materialCreated()){
                    Log log = new Log(new double[]{	(this.getPositionX()) + (World.LENGTH_CUBE/2),
                    								(this.getPositionY()) + (World.LENGTH_CUBE/2),
                    								(this.getPositionZ()) + (World.LENGTH_CUBE/2)});
                    World.getInstance().addLogToSet(log);
                    this.setTerrainType(0);
                    World.getInstance().notifyTerrainTypeChange(this, true);
                    this.addLogToLogSet(log);
                }
                else
                    this.setTerrainType(0);
            }        

        }
    }
	
	/**
	 * Returns the position of the cube.
	 * @return an array of doubles that contains the coordinates of the position of the cube.
	 */
	@Basic @Raw
    public double[] getPositionInDoubles(){
        double[] position = new double[]{this.getPositionX(),this.getPositionY(),this.getPositionZ()};
        return position;
    }

}
