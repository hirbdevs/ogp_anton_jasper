package hillbillies.model;

import be.kuleuven.cs.som.annotate.Value;
import hillbillies.exceptions.IllegalPositionException;
import hillbillies.exceptions.UnitException;

/**
 * A class to implement default behaviour of a unit.
 * @author Anton
 * @note This class makes it possible to add actions to the default behaviour without having to change any of the code in the class Unit.
 *
 */
@Value
public class DefaultBehaviour {
	public final Unit unit;
	
	public DefaultBehaviour(Unit unit){
		this.unit = unit;
	}
	
	/**
	 * This methods makes the unit walk to a random position.
	 * @throws UnitException
	 */
	public void moveTo() throws UnitException{
		unit.moveToRandomPos();
	}
	
	/**
	 * This method makes the unit work on a random generated position.
	 * @throws UnitException
	 * @throws IllegalPositionException
	 */
	public void work() throws UnitException, IllegalPositionException{
		Position position = unit.getRandomWorkPos();
		unit.workAt((int)position.getPositionX(), (int)position.getPositionY(), (int)position.getPositionZ());
	}
	
	/**
	 * This method makes the unit rest.
	 * @throws UnitException
	 */
	public void rest() throws UnitException{
		unit.rest();
	}
	

}
