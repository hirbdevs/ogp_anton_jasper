package hillbillies.model;

import java.util.HashSet;
import java.util.Set;

import be.kuleuven.cs.som.annotate.Basic;
import hillbillies.exceptions.UnitException;


/**
 * A class of factions, involving an ID.
 * 
 * @invar	A faction can never have more than 50 units that belong to it.
 * 
 * @version	2.0
 * @author 	Anton Van Gompel
 * 			Jasper Bergmans
 */
public class Faction {	
		
	/**
	 * The identification number of a faction.
	 */
	private int factionId;
	
	/**
	 * Set of units that belong to this faction.
	 */
	public Set<Unit> units = new HashSet<Unit>();
		
	/**
	 * Initializes a new faction with a given ID.
	 * 
	 * @param 	id
	 * 			The Id for this new faction.
	 * @post   The id of this new faction is equal to the given id
	 * @effect
	 * 			The faction of the scheduler is set to this faction.
	 */
	public Faction(int id){
		this.setFactionId(id);
		this.thisScheduler = new Scheduler(this);
	}

	/**
	 * Sets the faction id to the given value.
	 * @param newId
	 */
	public void setFactionId(int newId){
		factionId = newId;
	}
	
	/**
	 * Returns the id of this faction.
	 * @return
	 */
	@Basic
	public int getFactionId(){
		return this.factionId;
	}

	/**
	 * A method that adds a given unit to the faction.
	 * @param 	unit
	 * 			The unit that must be added to the faction.
	 * 
	 * @post	Unit is added to this faction.
	 * @throws UnitException
	 */
	public void addUnitToFaction(Unit unit) throws UnitException{
		if (unit != null)
			units.add(unit);
		else throw new UnitException("Cannot add Unit to Set.");
	}
	
	/**
	 * Removes a unit from a faction.
	 * 
	 * @param unit
	 * 		  unit that must be removed from the faction.
	 * 
	 * @post	The unit does not belong to this faction anymore.
	 * @throws  UnitException
	 * 			If the unit is not in the factionset throw new UnitException			
	 */
	public void removeUnitFromFaction(Unit unit) throws UnitException{
		if (units.contains(unit))
			units.remove(unit);
		else throw new UnitException("No such Unit in the Set.");
	}
	
	/**
	 * returns the set of units that belong to the faction.
	 * @return
	 */
	@Basic
	public Set<Unit> getUnitsInFaction(){
		return units;
	}

	/**
	 * The scheduler that belongs to this faction.
	 */
	final Scheduler thisScheduler;
	
	/**
	 * Returns the scheduler-object of this faction.
	 * @return
	 */
	@Basic
	public Scheduler getScheduler(){
		return thisScheduler;
	}
}
