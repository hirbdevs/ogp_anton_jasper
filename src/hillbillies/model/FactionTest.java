package hillbillies.model;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import hillbillies.exceptions.GameObjectException;
import hillbillies.exceptions.IllegalPositionException;
import hillbillies.exceptions.UnitException;
import hillbillies.exceptions.WorldException;
import hillbillies.part2.listener.DefaultTerrainChangeListener;

public class FactionTest {
	World world;
	Faction testFaction, testFaction2;
	
	private static int [] PosArray = {1,1,1};
	private static int factionId = 1;
	
	static int[][][] cubeType = new int[3][3][3];
	Unit testUnit, testUnit2;
	
	DefaultTerrainChangeListener listener = new DefaultTerrainChangeListener();
	

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		for (int x = 0; x<3; x++)
			for(int y = 0; y<3; y++)
				for(int z = 0; z<3; z++)
					cubeType[x][y][z] = 0;
	}

	@Before
	public void setUp() throws Exception {
		world = World.getInstance();
		World.getInstance().setTerrainChangeListener(listener);
		World.getInstance().setBoundaries(2, 2, 2);
		World.getInstance().setConnectedToBorder(World.MAXPOSITION_X, World.MAXPOSITION_Y, World.MAXPOSITION_Z);
		World.getInstance().setCubes(cubeType);
		
		testFaction = new Faction(factionId);
		testFaction2 = new Faction(3);
	}

	@Test
	public void constructor_legal() {
		Faction testFaction2 = new Faction(2);
		assertEquals(testFaction2.getFactionId(), 2);
	}
	
	@Test
	public void addUnitToFaction_legalCase() throws UnitException, WorldException, GameObjectException, IllegalPositionException{
		testUnit2 = new Unit("Jasper", PosArray , 50,50,50,50, false);
		testFaction2.addUnitToFaction(testUnit2);
		assertTrue(testFaction2.units.contains(testUnit2));
	}
	
	@Test (expected=UnitException.class)
	public void addUnitToFaction_illegalCase() throws UnitException{
		testFaction.addUnitToFaction(null);
	}
	
	@Test
	public void removeUnitFromFaction_legalCase() throws UnitException, WorldException, GameObjectException, IllegalPositionException{
		testUnit = new Unit ("Jasper", PosArray, 50,50,50,50, false);
		
		testFaction.addUnitToFaction(testUnit);
		testFaction.removeUnitFromFaction(testUnit);
		assertFalse(testUnit.getFaction() == testFaction);
	}
	
	@Test (expected=UnitException.class)
	public void removeUnitFromFaction_illegalCase() throws UnitException{
		testFaction.removeUnitFromFaction(testUnit);
	}
	
	@Test
	public void IsSchedulerCreated(){
		assertFalse(testFaction.thisScheduler == null);
	}
	
	@Test
	public void getSchedulerOfFaction(){
		assertEquals(testFaction.thisScheduler, this.testFaction.getScheduler());
	}
	
	@Test
	public void SchedulerofFactionEmptyAfterCreation(){
		assertTrue(this.testFaction.getScheduler().getAllTasks().isEmpty());
	}
	
}
