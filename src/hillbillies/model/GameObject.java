package hillbillies.model;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Raw;
import hillbillies.exceptions.GameObjectException;
import hillbillies.exceptions.IllegalPositionException;
import hillbillies.exceptions.WorldException;
import ogp.framework.util.Util;

/**
 * A class of objects that can occur in the gameWorld and can be carried by units.
 * 
 * @invar	A gameObject must at all times have a position with coordinates that are bigger or equal to zero and smaller than the boundaries of the gameWorld.
 * 			|this.getPositionGameObjectX() >= 0 && this.getPositionGameObjectX() < World.MAXPOSITION_X
 * 			|this.getPositionGameObjectY() >= 0 && this.getPositionGameObjectY() < World.MAXPOSITION_Y
 * 			|this.getPositionGameObjectZ() >= 0 && this.getPositionGameObjectZ() < World.MAXPOSITION_Z
 * 
 * @invar	A gameObject must at all times have a weight that is bigger or equal to zero and smaller or equal to 200.
 * 			|this.getWeight >= 0 && this.getWeight <= 200.
 * 
 * @author Anton Van Gompel
 * @author Jasper Bergmans
 * @version	3.0
 */


abstract public class GameObject {
	
	/**
	 * Initialize this new object with given position.
	 * 
	 * @param 	position
	 * 			The position of this new object.
	 * @effect	The position of this object is set to the given position.
	 * 			| setObjectPosition(position.intToArray()[0] + (World.LENGTH_CUBE/2,
	 * 			|					position.intToArray()[1] + (World.LENGTH_CUBE/2,
	 * 			|					position.intToArray()[2] + (World.LENGTH_CUBE/2)
	 * @throws GameObjectException
	 * @throws WorldException 
	 */
	@Raw
	public GameObject(Position position) throws GameObjectException, WorldException {
		if (this instanceof Unit)
			this.setObjectPosition(	position.intToArray()[0] + (World.LENGTH_CUBE/2),
									position.intToArray()[1] + (World.LENGTH_CUBE/2),
									position.intToArray()[2] + (World.LENGTH_CUBE/2));
		else
			this.setObjectPosition(position.doubleToArray()[0], position.doubleToArray()[1], position.doubleToArray()[2]);
	}
	
	
	
	//// POSITION OF OBJECT ////
	
	/**
	 * Contains the x, y and z coordinate of the current position of the game-object. 
	 */
	protected Position currentPosition;
	
	/**
	 * Contains the weight of the gameObject.
	 */
	public int weight;
	
	/**
	 * Method to get the current x-position of the game-object in the gameworld.
	 * @return
	 */
	@Basic @Raw
	public double getPositionGameObjectX(){
		return this.currentPosition.position_x;
	}
		
	/**
	 * Method to get the current y-position of the game-object in the gameworld.
	 * @return
	 */
	@Basic @Raw
	public double getPositionGameObjectY(){
		return this.currentPosition.position_y;
	}
	
	/**
	 * Method to get the current z-position of the game-object in the gameworld.
	 * @return
	 */
	@Basic @Raw
	public double getPositionGameObjectZ(){
		return this.currentPosition.position_z;
	}
	
	/**
	 * Method to get the current position of the game-object in the gameworld.
	 * @return
	 */
	@Basic @Raw
	public Position getPositionGameObject(){
		return this.currentPosition;
	}
	
	/**
	 * Method to get the current x-coordinate of the cube the is currently occupied by the game-object in the gameworld.
	 * @return
	 */
	@Basic @Raw
	public double getGameObjectPositionXGameworld(){
		return Math.floor(this.currentPosition.position_x); 				
	}
	
	/**
	 * Method to get the current y-coordinate of the cube the is currently occupied by the game-object in the gameworld.
	 * @return
	 */
	@Basic @Raw
	public double getGameObjectPositionYGameworld(){
		return Math.floor(this.currentPosition.position_y);
	}
	
	/**
	 * Method to get the current z-coordinate of the cube the is currently occupied by the game-object in the gameworld.
	 * @return
	 */
	@Basic @Raw
	public double getGameObjectPositionZGameworld(){
		return Math.floor(this.currentPosition.position_z);
	}
	
	/**
	 * Returns the maximal x-coordinate a game-object can have in the gameworld.
	 * @return
	 */
	@Basic @Immutable @Raw
	public double getMaxPosition_X(){
		return World.MAXPOSITION_X;
	}
	
	/**
	 * Returns the maximal y-coordinate a game-object can have in the gameworld.
	 * @return
	 */
	@Basic @Immutable @Raw
	public double getMaxPosition_Y(){
		return World.MAXPOSITION_Y;
	}
	
	/**
	 * Returns the maximal z-coordinate a game-object can have in the gameworld.
	 * @return
	 */
	@Basic @Immutable @Raw
	public double getMaxPosition_Z(){
		return World.MAXPOSITION_Z;
	}
	
	/**
	 * Method to set the position of a gameObject to a given position.
	 * 
	 * @param 	x
	 * 		The x coordinate of the desired position to place the game object.
	 * @param 	y
	 * 		The y coordinate of the desired position to place the game object.
	 * @param 	z
	 * 		The z coordinate of the desired position to place the game object.
	 * 
	 * @post	
	 * 		The new position of the game object is equal to the given position
	 * @effect
	 * 		The gameObject is removed from the appropriate set of objects from it's initial cube,
	 * 		and added to the appropriate set of objects of it's new cube.
	 * @effect
	 * 		If the gameObject had no position before this method was called, the object is added to the
	 * 		appropriate set of game-objects of the world.
	 * @throws GameObjectException
	 * 		If the desired position to set the game-object to is not a valid position, throw new GameObjectPosition.
	 * @throws WorldException
	 */
	@Raw
	protected void setObjectPosition(double x, double y, double z) throws GameObjectException, WorldException{
		if (currentPosition == null){
			try{
				currentPosition = new Position(x,y,z);
				this.addToAppropriateSet();
				if (this instanceof Boulder)
					World.getInstance().addBoulderToSet((Boulder)this);
				else if (this instanceof Log)
					World.getInstance().addLogToSet((Log)this);					
			}catch (NullPointerException e){
				throw new GameObjectException("Position out of bounds.");
			} catch (IllegalPositionException e){
				throw new GameObjectException("Illegal position.");
			}
		}
		else{
			try{
				this.removeFromAppropriateSet();
					this.currentPosition.setPositionX(x);
					this.currentPosition.setPositionY(y);
					this.currentPosition.setPositionZ(z);
				this.addToAppropriateSet();
			} catch (IllegalPositionException e){
				throw new GameObjectException(e.getMessage());
			}
		}
	}
	
	/**
	 * Adds the gameObject to the appropiate set in the class World. (Add to the boulderSet if it is a boulder, add
	 * to the logSet if it is a log and add the gameobject to the unitSet if it is a unit).
	 *
	 * @effect
	 * 		if the given gameObject is not null and an instance of Log, it is present in logSet.
	 * 		| if (this instanceof Log)
	 * 		| 		World.getInstance().getCubeByPosition(currentPosition).addBoulderToBoulderSet((Boulder) this);
	 * @effect
	 * 		if the given gameObject is not null and an instance of boulder, it is present in boulderSet.
	 * 		| if (this instanceof Boulder)
	 * 		|		World.getInstance().getCubeByPosition(currentPosition).addLogToLogSet((Log) this);
	 * @effect
	 * 		if the given gameObject is not null and an instance of unit, it is present in logSet. 
	 * 		| if (this instanceof Unit)
	 * 		| 		World.getInstance().getCubeByPosition(currentPosition).addUnitToCubeSet((Unit) this);
	 * @throws GameObjectException
	 * 		If the game object that called this function is not an instance of the Boulder, Log or Unit class, throw 
	 * 		new GameObjectException.
	 * 		| !( this instanceof Boulder || this instanceof Log || this instanceof Unit)
	 * @throws WorldException
	 */
	private void addToAppropriateSet() throws GameObjectException, WorldException{
		if(this instanceof Boulder)
			World.getInstance().getCubeByPosition(currentPosition).addBoulderToBoulderSet((Boulder) this);
		else if (this instanceof Log)
			World.getInstance().getCubeByPosition(currentPosition).addLogToLogSet((Log) this);
		else if (this instanceof Unit)
			World.getInstance().getCubeByPosition(currentPosition).addUnitToCubeSet((Unit) this);
		else
			throw new GameObjectException("No such gameObjectType exists.");
	}
	
	/**
	 *  Removes the gameObject from the appropiate set in the class World. (remove from the boulderSet if it is a boulder,
	 *  remove from the logSet if it is a log and remove from gameobject from the unitSet if it is a unit).
	 *  
	 *  @post
	 *  	The GameObject is removed from the appropriate set.
	 * 	@effect
	 * 		if the given gameObject is not null and an instance of Log, it is no longer present in logSet.
	 * 	@effect
	 * 		if the given gameObject is not null and an instance of boulder, it is no longer present in boulderSet.
	 * 	@effect
	 * 		if the given gameObject is not null and an instance of Unit, it is no longer present in boulderSet.
	 * 
	 * 	@throws GameObjectException
	 * 		If the game object that called this function is not an instance of the Boulder, Log or Unit class, throw 
	 * 		new GameObjectException.
	 * 	@throws WorldException
	 */
	private void removeFromAppropriateSet() throws GameObjectException, WorldException{
		if(this instanceof Boulder)
			World.getInstance().getCubeByPosition(currentPosition).removeBoulderFromBoulderSet((IMaterial) this);
		else if (this instanceof Log)
			World.getInstance().getCubeByPosition(currentPosition).removeLogFromLogSet((Log) this);
		else if (this instanceof Unit)
			World.getInstance().getCubeByPosition(currentPosition).removeUnitFromCubeSet((Unit)this);
		else
			throw new GameObjectException("No such gameObjectType exists.");
	}
	
	/**
	 * Returns the current weight of the gameObject.
	 * @return
	 */
	@Basic @Raw
	public int getWeight(){
		return this.weight;
	}
	
	/**
	 * Sets the weight of the gameobject.
	 * @param weight
	 * 		the value that the variable weight should get.
	 * @post the value of weight will be set to the given value.
	 * 			|new.getWeight() = weight;
	 * @post the weight should be an integer value between 0 and 200.
	 * 			|new.getWeight() >= 0 && this.getWeight() <= 200
	 * @post if the value of weight is negative, we will take the absolute value.
	 * 			|if(weight < 0)
	 * 			|	new.getWeight() == Math.abs(weight)
	 * @post if the value of weight is bigger than the maximum value(200), we will take the maximum value of weight.
	 * 			|if(weight > 200)
	 * 			| 	new.getWeight() == 200;
	 */
	@Raw
	public abstract void setWeight(int weight);
	
	/**
	 * A boolean that returns whether the given weight is a valid one.
	 * @param weight
	 * 		the weight that must be checked.
	 * @return true if the weight is an integer between 0 and 200 inclusively.
	 * 			|if(weight >= 0 && weight <= 200)
	 * 			|	return true;
	 * 			|else return false;
	 */
	public boolean isValidWeight(int weight){
		if(weight >= 0 && weight <= 200)
			return true;
		else return false;
	}

	
	//// FALLING ////
	
	/**
	 * Boolean to indicate whether a game-object is falling or not.
	 */
	protected boolean falling = false;
	
	/**
	 * Targetposition to fall to.
	 */
	protected double targetPosition_z;
	
	
	/**
	 * Method to make a game-object fall for a given amount of gametime.
	 * 
	 * @param 	GameTime
	 * 		The length of time the game-object must fall.
	 * @effect
	 * 		The targetposition to fall to is located at exactly 1 cube length below the current 
	 * 		position of the gameObject.
	 * @post
	 * 		The game-object is located at a lower z-value as it's initial z-value.
	 * 
	 * @throws GameObjectException
	 * @throws IllegalPositionException 
	 * @throws WorldException 
	 */
	protected void fall(double GameTime) throws GameObjectException, IllegalPositionException, WorldException{
		if (falling == false)
			this.fallOneLevel();
			this.falling = true;
		this.calculateTemporaryFallingPosition(GameTime, this.getFallingSpeed());
	}
	
	/**
	 * Sets the targetposition to fall to, which is one level below the current z-coordinate.
	 * 
	 * @post the targetposition to fall to is located at exactly 1 cube length below the current position of 
	 * 		 the gameObject. 
	 * 		 | new.targetPosition_z = this.getPositionZ()-1
	 * 
	 * @throws IllegalPositionException
	 */
	private void fallOneLevel() throws IllegalPositionException{
			this.setTargetPositionz(this.getPositionGameObjectZ()-1);
	}

	/**
	 * Sets the attribute targetPosition_x to a value given as parameter.
	 * 
	 * @param 	z
	 * 			The new z-coordinate we want to set for the targetPostion.
	 *
	 * @post
	 * 		The value of the targetposition is equal to the given value and is a valid value 
	 * 		(bigger or equal to zero and smaller or equal to MAXPOSITION_Z).
	 * 
	 * @throws 	IllegalPositionException
	 * 			If the given targetPosition is not a valid position throw new IllegalPositionException.
	 */
	@Basic @Raw
	protected void setTargetPositionz(double z) throws IllegalPositionException{
		if (!World.isValidPosition(z, this.getMaxPosition_Z()))
			throw new IllegalPositionException(z);
		else this.targetPosition_z= z;
	}
	
	/**
	 * Recalculates the position of a gameObject while moving (falling) after a given period of gameTime has passed.
	 * 
	 * @param 	GameTime
	 * 			The length of gameTime an object will be falling.
	 * @param 	speed
	 * 			The speed at which the object will fall.
	 * @throws GameObjectException
	 * @throws WorldException 
	 */
	public void calculateTemporaryFallingPosition(double GameTime, double speed) throws GameObjectException, WorldException{
			double v_z = speed;
			double z = this.getPositionGameObjectZ() + v_z * GameTime;
			
			 if(Util.fuzzyEquals(z,this.targetPosition_z,0.05)) {
				   z = this.targetPosition_z;
				   falling = false;
			 }
			this.setObjectPosition(	this.getPositionGameObjectX(),this.getPositionGameObjectY(), z); 
	}
	
	/**
	 * Returns the fallingspeed of the game-objects.
	 * @return
	 */
	@Basic @Raw
	private double getFallingSpeed(){
		return (-3);
	}
	
	
	
	
}


