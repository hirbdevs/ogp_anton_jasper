package hillbillies.model;

import hillbillies.exceptions.GameObjectException;
import hillbillies.exceptions.UnitException;
import hillbillies.exceptions.WorldException;

public interface IMaterial {
	
	/**
	 * Method to pick up a material. This method will also assign a carrier for this material.
	 * 
	 * @param 	unit
	 * 		The unit that picks up the material and is set to the carrier of it.
	 * @throws 	GameObjectException
	 * @throws 	UnitException
	 * 		If unit == null throw new UnitException. 
	 * @throws WorldException 
	 */
	public void pickUp(Unit unit)throws GameObjectException, WorldException, UnitException;
	
	/**
	 * Method to drop a material at a given position.
	 * 
	 * @param 	unit
	 * 		The unit that is currently carrying the material and that must drop it now.
	 * @param 	dropPosition
	 * 		The position of the cube where to drop the material.  
	 * @throws 	GameObjectException
	 * @throws 	UnitException
	 * 		If unit == null throw new UnitException. 
	 * 		If unit is not carrying a boulder, throw new UnitException. 
	 * @throws 	WorldException
	 * 		If dropPosition = null throw new WorldException. 
	 */
	public void drop(Unit unit, Cube dropPosition) throws GameObjectException, WorldException, UnitException;
	
	/**
	 * Method to consume a Material. After consuming the material is removed from the world.
	 * @throws GameObjectException
	 */
	public void consume()throws GameObjectException;
	
}