package hillbillies.model;

import hillbillies.exceptions.GameObjectException;
import hillbillies.exceptions.IllegalPositionException;
import hillbillies.exceptions.UnitException;
import hillbillies.exceptions.WorldException;

/**
 * A class of logs as a special case of GameObject.
 * 
 * @author Anton Van Gompel
 * @author Jasper Bergmans
 * @version	2.0
 * 
 */
public class Log extends Material implements IMaterial{

    /**
     * A constructor for a Log. Constructs a Log at a given position.
     * 
     * @param 	position
     * 		The inititial position of the cube where the log will be positioned.
     * @effect	
     * 		The new log is initialized as a new GameObject with a given position.
     * 
     * @throws GameObjectException
     * @throws WorldException 
     * @throws IllegalPositionException 
     */
	public Log(double[] position) throws GameObjectException, WorldException, IllegalPositionException{
    	super (new Position(position[0],position[1], position[2] ));
    }

	/**
	 * @post
	 * 		The log is picked up by the given unit.
	 * @effect	
	 * 		The log is carried by a given unit.
	 * @effect	
	 * 		The weight of this log is added to the weight of the unit.
	 * @effect	
	 * 		A unit will be carrying this log.
	 * @effect	
	 * 		The log is removed from its current log-set.
	 * @effect
	 * 		This log is removed from the set of logs in this world.
	 */
	@Override
	public void pickUp(Unit unit) throws GameObjectException, WorldException, UnitException {
		if (unit != null){
			World.getInstance().getCubeByPosition(this.currentPosition).removeLogFromLogSet(this);
			World.getInstance().removeLogFromSet(this);
			this.setCarrier(unit);
			unit.carryMaterial(this);
			unit.setWeightMaterial(this.getWeight());
		}
		else
			throw new UnitException("Not a valid unit");
		
	}

	/**
	 * @post
	 * 		The unit has dropped the log it was carrying at the given dropposition.
	 * @effect	
	 * 		The log's carrier is set to null
	 * @effect	
	 * 		The weight of the log is subtracted from the unit's weight.
	 * @effect	
	 * 		A unit will stop carrying this log.
	 * @effect 	
	 * 		The log will be added to the logset of the cube where the log is dropped.
	 * @effect
	 * 		The log is added to the set of logs in this gameworld.
	 */
	@Override
	public void drop(Unit unit, Cube dropPosition) throws GameObjectException, WorldException, UnitException {
		if (unit != null){
			if(unit.isCarryingMaterial() && unit.getLog()!= null){			
				if (dropPosition == null)
					throw new WorldException("Not a valid cube to drop the log in.");
				else {
					this.setCarrier(null);
					unit.carryMaterial(null);
					unit.setWeightMaterial(0);
					this.setObjectPosition(dropPosition.getPositionX() + World.LENGTH_CUBE / 2,
							dropPosition.getPositionY() + World.LENGTH_CUBE / 2,
							dropPosition.getPositionZ() + World.LENGTH_CUBE / 2);
					World.getInstance().addLogToSet(this);
				} 
			} 
			else 
				throw new UnitException("Unit is not carrying log.");
			} else throw new UnitException("Not a valid unit.");
		
	}

	/**
	 * @post
	 * 		This log is no longer part of the game.
	 * @effect	
	 * 		The log is removed from the set of logs in the world.
	 * @effect	
	 * 		The log is removed from its current logset.
	 */
	@Override
	public void consume() throws GameObjectException {
		World.getInstance().removeLogFromSet(this);
		World.getInstance().getCubeByPosition(this.currentPosition).removeLogFromLogSet(this);
		
	}

}