package hillbillies.model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import hillbillies.exceptions.GameObjectException;
import hillbillies.exceptions.IllegalPositionException;
import hillbillies.exceptions.UnitException;
import hillbillies.exceptions.WorldException;
import hillbillies.part2.listener.DefaultTerrainChangeListener;

public class LogTest {

	//World 10 X 10 X 10
		World world;
		static int[][][] cubeType = new int[10][10][10];
		DefaultTerrainChangeListener listener = new DefaultTerrainChangeListener();
		
		private static double [] positionArray1 = {1.5,2.5,3.5};
		private static double [] OutOfBoundsArray = {10,11,10};
		private static int [] PosArray3 = {1,2,3};

		Unit	testUnit, testUnit2;
		Log 	log, log2;
		Cube	dropPosition, illegalDropPosition;
		
		@BeforeClass
		public static void setUpBeforeClass() throws Exception {
			for (int x = 0; x<10; x++)
				for(int y = 0; y<10; y++)
					for(int z = 0; z<10; z++)
						cubeType[x][y][z] = 0;
		}
		
		@Before
		public void setUp() throws Exception {
			// World 10 X 10 X 10
			world = World.getInstance();
			World.getInstance().setTerrainChangeListener(listener);
			World.getInstance().setBoundaries(10, 10, 10);
			World.getInstance().setConnectedToBorder(World.MAXPOSITION_X, World.MAXPOSITION_Y, World.MAXPOSITION_Z);
			World.getInstance().setCubes(cubeType);
			
			testUnit = new Unit ("Jasper", PosArray3, 50,50,50,50, false);
			testUnit2 = new Unit ("Anton", PosArray3, 50,50,50,50, false);
			log = new Log(LogTest.positionArray1);
			log2 = new Log(LogTest.positionArray1);
			dropPosition = new Cube(0,1,1,1);
		}

		@Test
		public void constructor_LegalCase() throws GameObjectException, WorldException, IllegalPositionException{
			Log log2 = new Log(positionArray1);
			assertEquals(log2.getPositionGameObjectX(), 1.5, 0.05);
			assertEquals(log2.getPositionGameObjectY(), 2.5, 0.05);
			assertEquals(log2.getPositionGameObjectZ(), 3.5, 0.05);
		}
		
		@Test (expected=IllegalPositionException.class)
		public void constructor_IllegalCase() throws GameObjectException, WorldException, IllegalPositionException{
			Log log3 = new Log(LogTest.OutOfBoundsArray);
			//The only reason of the next line is to remove the warning message that var log3 is never used.
			log3.getCarrier();
		}
		
		@Test
		public void pickUpLog_Legal() throws GameObjectException, UnitException, WorldException{
			log.pickUp(testUnit);
			assertTrue(testUnit == log.getCarrier());
			assertEquals(testUnit.getWeight()+log.getWeight(),testUnit.getTotalWeight(),0.05);
			assertTrue(testUnit.isCarryingLog() == true);
			}

		@Test (expected=UnitException.class)
		public void pickUpLog_Illegal() throws GameObjectException, UnitException, WorldException{
			log.pickUp(null);
		}
		
		@Test
		public void dropLog_Legal() throws GameObjectException, UnitException, WorldException{
			testUnit.carryMaterial(log);
			log.drop(testUnit, dropPosition);
			assertEquals(testUnit.getWeight()+testUnit.getWeightMaterial(),testUnit.getTotalWeight(),0.05);
			assertEquals(testUnit.getWeight()+testUnit.getWeightMaterial(),testUnit.getWeight(),0.05);
			assertTrue(testUnit.isCarryingLog() == false);
			assertEquals(log.getPositionGameObjectX(),dropPosition.getPositionX()+ World.LENGTH_CUBE/2,0.05);
			assertEquals(log.getPositionGameObjectY(),dropPosition.getPositionX()+ World.LENGTH_CUBE/2,0.05);
			assertEquals(log.getPositionGameObjectZ(),dropPosition.getPositionX()+ World.LENGTH_CUBE/2,0.05);
		}
		
		@Test (expected=GameObjectException.class)
		public void dropLog_LegalCheckCarrier() throws GameObjectException, UnitException, WorldException{
			testUnit.carryMaterial(log);
			log.drop(testUnit, dropPosition);
			log.getCarrier();
		}

		
		@Test (expected=UnitException.class)
		public void dropLog_IllegalNullValueUnit() throws GameObjectException, UnitException, WorldException{
			log.drop(null, dropPosition);
		}
		
		@Test (expected=WorldException.class)
		public void dropLog_IllegalNullValuePosition() throws GameObjectException, UnitException, WorldException{
			testUnit.carryMaterial(log);
			log.drop(testUnit, null);
		}
		
		@Test
		public void consumeLog_Legal() throws GameObjectException{
			log.consume();
			assertFalse(World.getInstance().logSet.contains(log));
			assertFalse(World.getInstance().getCubeByPosition(log.currentPosition).logSet.contains(log));
		}

}
