package hillbillies.model;

import java.util.Random;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Raw;
import hillbillies.exceptions.GameObjectException;
import hillbillies.exceptions.IllegalPositionException;
import hillbillies.exceptions.WorldException;

/**
 * A class of materials that can occur in the gameWorld and can be carried by units.
 * 
 * @invar 	Each material has a weight that never changes during its lifetime. The value of this weight is
 * 			between 10 and 50 inclusive.
 * 			|this.getWeight >= 10 && this.getWeight <= 50
 * 
 * @author Anton Van Gompel
 * @author Jasper Bergmans
 * @version	2.0
 */

public abstract class Material extends GameObject{

	/**
	 * Initialize this new Material with a given position.
	 * 
	 * @param position
	 * 		The position of this new material.
	 * @post	
	 * 		The value of final variable object_weight is assigned to a random value between 
	 * 		10 and 50 inclusive.
	 * @effect
	 * 		The position of the material object is equal to the given position.
	 * @throws GameObjectException
	 * @throws WorldException
	 * @throws IllegalPositionException
	 */
	@Raw
	protected Material(Position position) throws GameObjectException, WorldException, IllegalPositionException{
		super (position);
		Random random = new Random();
		this.setWeight(random.nextInt(50-10+1)+10);
    }
	
	/**
	 * Sets the weight of the Material.
	 * @post the weight should be an integer between 10 and 50 inclusively.
	 * 			|this.getWeight() >= 10 && this.getWeight() <= 50
	 * @post if the value is smaller than 10, we will take 10 for the value.
	 * 			|if(weight <10)
	 * 			|	new.getWeight() == 10
	 * @post if the value is bigger then 50, we will take 50 for the value.
	 * 			|if(weight > 50)
	 * 			|	new.getWeight() == 50
	 */
	@Override
	public void setWeight(int weight){
		if(weight <10)
			this.weight = 10;
		if(weight > 50)
			this.weight = 50;
		else
			this.weight = weight;
	}
	
	/**
	 * A boolean that returns whether the given weight is a valid one.
	 * @return true if the weight is an integer between 10 and 50 inclusively.
	 * 			|if(weight >= 10 && weight <= 200)
	 * 			|	return true;
	 * 			|else return false;
	 */
	@Override
	public boolean isValidWeight(int weight){
		if(weight >= 10 && weight <= 50)
			return true;
		else return false;
	}

	/**
	 * Method to check whether a material is located at a valid position. A valid position for a material is when
	 * it is located in a cube made of passable material and  right above a cube that is made of impassable material
	 * or z is at the bottom of the world.
	 *  
	 * @return 	True if and only if the occupied cube is made of passable material and has a solid cube that 
	 * 			is located directly under that cube or when the cube is located at z = 0.
	 */
	private boolean isValidMaterialPosition(){
		boolean result = false;
		int cube_x = (int)this.getGameObjectPositionXGameworld();
		int cube_y = (int)this.getGameObjectPositionYGameworld();
		int cube_z = (int)this.getGameObjectPositionZGameworld();
		
		if ((this.getGameObjectPositionZGameworld()==0)&&
			(World.getInstance().getCubeByPosition(cube_x, cube_y, cube_z).isPassableCube())) 
			return true;
		else if (World.getInstance().getCubeByPosition(	cube_x,cube_y, cube_z).getTerrainType() == TerrainType.workshop)
			return true;
		else if( (World.getInstance().getCubeByPosition(cube_x,cube_y,cube_z ).isPassableCube()))		
				if (!(this.getGameObjectPositionZGameworld() ==0))
					if (World.getInstance().getCubeByPosition(cube_x,cube_y,cube_z-1 ).isSolidCube())
						result =  true;
					else
						result = false;
			return result;
	}
	
	
	
	////CARRY MATERIALS ////
	
	/**
	 * The unit that is carrying the material.
	 */
	private Unit carrier = null;
	
	/**
	 * Method to assign a carrier to a material.
	 * 
	 * @param carrier
	 * 		The unit that is carrying the material
	 * @post The carrier of this material is the same as the given carrier.
	 */
	@Raw
	protected void setCarrier(Unit carrier){
		this.carrier = carrier;
	}

	/**
	 * Method to return the current carrier of this material.
	 * 
	 * @return
	 * @throws 	GameObjectException
	 * 		If this material is not carried, throw new GameObjecException.
	 */
	@Basic @Raw
	public Unit getCarrier() throws GameObjectException{
		if (this.isCarried() == true)
			return carrier;
		else throw new GameObjectException("No carrier assigned for this material.");
	}
	
	/**
	 * Boolean to indicate whether a material is beiing carried or not.
	 * 
	 * @return	true if and only if a material has a carrier.
	 */
	@Raw
	public boolean isCarried(){
		return(this.carrier != null);
	}
	
	/**
	 * This method is called maximum every 0.2 seconds. It keeps track of what's going on with a material.
	 * It checks whether a material is located at a stable position and if this is not the case, the method makes
	 * the object fall.
	 * 
	 * @param 	GameTime
	 * @throws 	GameObjectException
	 * @throws IllegalPositionException 
	 * @throws WorldException 
	 * @effect
	 * 		If the currentposition of this material is not valid, the material will be located at a 
	 * 		lower z-value as it's initial z-value.
	 * @note	
	 * 		If a material is not located at a stable location, it will fall to exactly one level lower and then
	 * 		re-check whether that location is stable. If this is not the case it will continue falling.
	 */
	public void advanceTime(double GameTime) throws GameObjectException, IllegalPositionException, WorldException{
		if (this.isValidMaterialPosition() == false)
			this.fall(GameTime);
	}	
}
