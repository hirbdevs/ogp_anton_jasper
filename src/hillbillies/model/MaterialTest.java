package hillbillies.model;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import hillbillies.exceptions.GameObjectException;
import hillbillies.exceptions.IllegalPositionException;
import hillbillies.exceptions.WorldException;
import hillbillies.part2.listener.DefaultTerrainChangeListener;

public class MaterialTest {

	World world;
	static int[][][] cubeType = new int[10][10][10];
	DefaultTerrainChangeListener listener = new DefaultTerrainChangeListener();
	
	GameObject boulder, log, boulder2,log2, boulder3;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		for (int x = 0; x<10; x++)
			for(int y = 0; y<10; y++)
				for(int z = 0; z<10; z++)
					cubeType[x][y][z] = 0;
	}

	@Before
	public void setUp() throws Exception {
		world = World.getInstance();
		World.getInstance().setTerrainChangeListener(listener);
		World.getInstance().setBoundaries(10, 10, 10);
		World.getInstance().setConnectedToBorder(World.MAXPOSITION_X, World.MAXPOSITION_Y, World.MAXPOSITION_Z);
		World.getInstance().setCubes(cubeType);
		
		double [] positionArray1 = {1.5,2.5,3.5};
		double [] positionArray2 = {9.5,9.5,9.5};
		double [] positionArray3 = {0.5,0.5,0.5};
	
		boulder = new Boulder(positionArray1);
		boulder2 = new Boulder(positionArray2);
		boulder3 = new Boulder(positionArray3);
		log = new Log(positionArray2);
		log2 = new Log(positionArray1);
	}
	
	
	@Test (expected=IllegalPositionException.class)
	public void constructor_IllegalCase() throws GameObjectException, WorldException, IllegalPositionException{
		double [] outOfBounds = {12,12,12};
		Boulder outOfBoundsBoulder = new Boulder(outOfBounds);
		//The only reason of the next line is to remove the warning message that var boulder3 is never used.
		outOfBoundsBoulder.getCarrier();
	}
	
	
	
	//// Weight tests ////
	
	@Test
	public void getPositionAfterCreation(){
		assertEquals(1.5, boulder.getPositionGameObjectX(),0.05);
		assertEquals(2.5, boulder.getPositionGameObjectY(),0.05);
		assertEquals(3.5, boulder.getPositionGameObjectZ(),0.05);
	}

	//TODO verplaatsen
//	@Test
//	public void checkWeightBetweenMinMax(){
//		assertTrue((boulder.getWeightMaterial()>=10)&&(boulder.getWeightMaterial() <= 50));
//	}
	
	
	
	//// Position tests ////
	@Test
	public void setPosition_legal() throws GameObjectException, WorldException{
		boulder.setObjectPosition(4.5, 5.5, 5.5);
		assertEquals(4.5, boulder.getPositionGameObjectX(),0.05);
		assertEquals(5.5, boulder.getPositionGameObjectY(),0.05);
		assertEquals(5.5, boulder.getPositionGameObjectZ(),0.05);
	}
	
	@Test (expected=GameObjectException.class)
	public void setPosition_illegalTooLow() throws GameObjectException, WorldException{
		boulder.setObjectPosition(-4.5, 5.5, 5.5);
	}
	
	@Test (expected=GameObjectException.class)
	public void setPosition_illegalTooHigh() throws GameObjectException, WorldException{
		boulder.setObjectPosition(90.5, 5.5, 5.5);
	}
	
	@Test
	//Test if a boulder is set to the correct cube after creation.
	public void boulderInSetCube(){
		assertTrue(World.getInstance().getCubeByPosition(	(int)boulder.getPositionGameObjectX(),
															(int)boulder.getPositionGameObjectY(), 
															(int)boulder.getPositionGameObjectZ()).getBoulders().contains(boulder));
	}
	
	@Test
	//Test if a log is set to the correct cube after creation.
	public void logInSetCube(){
		assertTrue(World.getInstance().getCubeByPosition(	(int)log.getPositionGameObjectX(),
															(int)log.getPositionGameObjectY(), 
															(int)log.getPositionGameObjectZ()).getLogs().contains(log));
	}
	
	@Test
	//Test if a boulder is removed from the correct cube's set after moving.
	public void removeBoulderFromSetAfterPositionChange() throws GameObjectException, WorldException{
		//boulder2.setObjectPosition(1.5, 1.5, 1.5);
		int a = (int)boulder2.getPositionGameObjectX();
		int b = (int)boulder2.getPositionGameObjectY();
		int c = (int)boulder2.getPositionGameObjectZ();
		boulder2.setObjectPosition(3.5, 4.5, 5.5);
		assertTrue(World.getInstance().getCubeByPosition(boulder2.currentPosition).getBoulders().contains(boulder2));
		assertFalse(World.getInstance().getCubeByPosition(a, b, c).getBoulders().contains(boulder2));
	}
	
	@Test
	public void addBoulderToSet() throws GameObjectException, WorldException{
		boulder2.setObjectPosition(3.5, 4.5, 5.5);
		assertTrue(World.getInstance().getBoulders().contains(boulder2));
		assertTrue(World.getInstance().getCubeByPosition(3, 4, 5).getBoulders().contains(boulder2));
	}
	
	@Test
	//Test if a log is removed from the correct cube's set after moving.
	public void removeLogFromSetAfterPositionChange() throws GameObjectException, WorldException{
		int x = (int)log2.getPositionGameObjectX();
		int y = (int)log2.getPositionGameObjectY();
		int z = (int)log2.getPositionGameObjectZ();
		
		log2.setObjectPosition(7.5, 7.5, 7.5);
		assertTrue(World.getInstance().getCubeByPosition(log2.currentPosition).getLogs().contains(log2));
		assertFalse(World.getInstance().getCubeByPosition(x, y, z).getLogs().contains(log2));
	}
	
	@Test
	//Test if a boulder is added to the correct cube's set after moving.
	public void addBoulderToSetAfterPositionChange() throws GameObjectException, WorldException{
		boulder.setObjectPosition(7.5, 7.5, 7.5);
		assertTrue(World.getInstance().getCubeByPosition(	(int)boulder.getPositionGameObjectX(),
															(int)boulder.getPositionGameObjectY(), 
															(int)boulder.getPositionGameObjectZ()).getBoulders().contains(boulder));
	}
	
	@Test
	//Test if a log is added to the correct cube's set after moving.
	public void addLogToSetAfterPositionChange() throws GameObjectException, WorldException{
		log.setObjectPosition(7.5, 7.5, 7.5);
		assertTrue(World.getInstance().getCubeByPosition(	(int)log.getPositionGameObjectX(),
															(int)log.getPositionGameObjectY(), 
															(int)log.getPositionGameObjectZ()).getLogs().contains(log));
	}
	
	@Test
	public void getGameWorldPosition(){
		assertEquals(1,boulder.getGameObjectPositionXGameworld(), 0.05);
		assertEquals(2,boulder.getGameObjectPositionYGameworld(), 0.05);
		assertEquals(3,boulder.getGameObjectPositionZGameworld(), 0.05);
	}

	@Test (expected=GameObjectException.class)
	public void IllegalSetPosition_tooHigh() throws GameObjectException, WorldException{
		log.setObjectPosition(20, 20.5, 40.5);
	}

	@Test (expected=GameObjectException.class)
	public void IllegalSetPosition_Negative() throws GameObjectException, WorldException{
		log.setObjectPosition(-3.5, 2.5, 4.5);
	}
	
	
	//// Falling tests ////
	
	@Test
	public void fall_legal() throws GameObjectException, IllegalPositionException, WorldException{
		double z_before = boulder.getPositionGameObjectZ();
		boulder.fall(0.2);
		assertEquals(boulder.targetPosition_z, z_before-1,0.05);
		assertTrue(boulder.falling);
		//3.5 + -3 * 0.2 = 0.9
		assertEquals(2.9,boulder.getPositionGameObjectZ(),0.05);
	}
	
	@Test (expected=IllegalPositionException.class)
	public void fall_illegal() throws GameObjectException, IllegalPositionException, WorldException{
		//double z_before = boulder3.getPositionGameObjectZ();
		boulder3.fall(0.2);
	}

}
