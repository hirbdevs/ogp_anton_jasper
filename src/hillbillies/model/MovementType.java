package hillbillies.model;

import be.kuleuven.cs.som.annotate.Value;

@Value
public enum MovementType {

	walking,
	sprinting,
	working,
	resting,
	fighting,
	default_behaviour,
	nothing,
	death,
	falling

}
