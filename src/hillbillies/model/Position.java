package hillbillies.model;

import be.kuleuven.cs.som.annotate.*;
import hillbillies.exceptions.IllegalPositionException;
import ogp.framework.util.Util;

/**
 * A class to represent a position in the gameWorld.
 * @author Anton
 *
 */
@Value
public class Position {

	/**
	 * the X coordinate
	 */
	double position_x;
	
	/**
	 * the Y coordinate
	 */
	double position_y;
	
	/**
	 * the Z coordinate
	 */
	double position_z;
	
	/**
	 * sets the position variables to a given value.
	 * @param 	x
	 * 			x-coordinate to set position_x to.
	 * @param 	y
	 * 			y-coordinate to set position_y to.
	 * @param 	z
	 * 			z-coordinate to set position_z to.
	 * @throws IllegalPositionException 
	 */
	public Position (double x, double y, double z) throws IllegalPositionException{
		this.setPositionX(x);
		this.setPositionY(y);
		this.setPositionZ(z);
	}
	
	/**
	 * Returns the value of position_x.
	 * @return
	 */
	@Basic
	public double getPositionX(){
		return this.position_x;
	}
	
	/**
	 * Returns the value of position_y.
	 * @return
	 */
	@Basic
	public double getPositionY(){
		return this.position_y;
	}
	
	/**
	 * Returns the value of position_z.
	 * @return
	 */
	@Basic
	public double getPositionZ(){
		return this.position_z;
	}
	
	/**
	 * Sets the attribute position_x to a value given as parameter.
	 * 
	 * @param 	x
	 * 			The new x-coordinate we want to set for the unit.
	 * @post	value of x must at all times be bigger or equal to zero and smaller or equal to MAXPOSITION_X;
	 * 			| new.isValidPosition(this.x);
	 * @throws	IllegalPositionException(x)
	 * 			the given position is not a valid position
	 * 			| ! isValidPosition(x, MAXPOSITION_X)
	 */
	public void setPositionX(double x) throws IllegalPositionException{
		if (!World.isValidPosition(x, World.MAXPOSITION_X)){
			System.out.println("line 81");
			throw new IllegalPositionException(x);}
		else this.position_x = x;
	}
	
	/**
	 * Sets the attribute position_y to a value given as parameter.
	 * 
	 * @param 	y
	 * 			The new y-coordinate we want to set for the unit.
	 * @post	value of y must at all times be bigger or equal to zero and smaller or equal to MAXPOSITION_Y;
	 * @throws	IllegalPositionException
	 * 			| ! isValidPosition(y, MAXPOSITION_Y)
	 */
	public void setPositionY(double y) throws IllegalPositionException{
		if (!World.isValidPosition(y, World.MAXPOSITION_Y)){
			System.out.println("line 97");
			throw new IllegalPositionException(y);}
		else this.position_y = y;
	}
	
	/**
	 * Sets the attribute position_z to a value given as parameter.
	 * 
	 * @param 	z
	 * 			The new z-coordinate we want to set for the unit.
	 * @ post	value of z must at all times be bigger or equal to zero and smaller or equal to MAXPOSITION_Z;
	 * @throws	IllegalPositionException
	 * 			| ! isValidPosition(z, MAXPOSITION_Z)
	 */
	public void setPositionZ(double z) throws IllegalPositionException{
		if (!World.isValidPosition(z, World.MAXPOSITION_Z)){
			System.out.println("line 113");
			throw new IllegalPositionException(z);}
		else this.position_z = z;
	}
	
	/**
	 * Calculates the distance between the currentPosition and targetPosition.
	 * 
	 * @param x
	 * 		current x coordinate
	 * @param y
	 * 		current y coordinate
	 * @param z
	 * 		current z coordinate
	 * @param targetx
	 * 		target x coordinate
	 * @param targety
	 * 		target y coordinate
	 * @param targetz
	 * 		target z coordinate
	 * @return
	 */
	public double getDistance(Position targetPosition){
		double first = Math.pow(targetPosition.position_x - this.getPositionX(), 2);
		double second = Math.pow(targetPosition.position_y - this.getPositionY(), 2);
		double third = Math.pow(targetPosition.position_z - this.getPositionZ(), 2);
		
		return Math.sqrt(first + second + third);
	}
	
	/**
	 * A method to compare two positions.
	 * 
	 * @param targetPosition
	 * 		The position we want to compare to our own position.
	 * @return true if the two positions are the same.
	 */
	public boolean isSamePosition(Position targetPosition, double epsilon){
		if ((Util.fuzzyEquals(this.position_x, targetPosition.position_x, epsilon)) 
			&& (Util.fuzzyEquals(this.position_y, targetPosition.position_y, epsilon))
			&& (Util.fuzzyEquals(this.position_z, targetPosition.position_z, epsilon)))
			return true;
		else return false;
	}
	
	@Override
	public boolean equals(Object other){
		if(other instanceof Position){
			if(this.isSamePosition((Position)other, 0.01))
				return true;
			else return false;
		}else return false;	
	}
	
	@Override
	public String toString(){
		return "[" + this.getPositionX() + ", " + this.getPositionY() + ", " + this.getPositionZ() + "]";
	}
	
	@Override
	public int hashCode(){
		Double x = this.position_x;
		Double y = this.position_y;
		Double z = this.position_z;
		return x.hashCode() + y.hashCode() + z.hashCode();
	}

	/**
	 * Transforms a position made of double values to a vector of double values.
	 * @return
	 * 		A vector that contains the position.
	 */
	public double[] doubleToArray(){
		double[] posArray = {this.getPositionX(), this.getPositionY(), this.getPositionZ()};
		return(posArray);
	}
	
	/**
	 * Transforms a position made of int values to a vector of int values.
	 * 
	 * @return
	 * 		A vector that contains the position.
	 */
	public int[] intToArray(){
		int[] posArray = {(int)this.getPositionX(), (int)this.getPositionY(), (int)this.getPositionZ()};
		return(posArray);
	}
	
	/**
	 * This method returns the distance between the Position and another given Position other.
	 * @param other
	 * @return
	 */
	public double distanceBetweenTwoPoints(Position other){
		double X = Math.pow((this.position_x - other.position_x), 2);
		double Y = Math.pow(this.position_y - other.position_y, 2);
		double Z = Math.pow(this.position_z - other.position_z, 2);
		return Math.sqrt(X + Y + Z);
	}
	
}
