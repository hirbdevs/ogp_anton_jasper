package hillbillies.model;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Immutable;
import be.kuleuven.cs.som.annotate.Value;

@Value
public class QueueElement<T1, T2> {
	
	private T1 first ;
	private T2 second ;
	
	public QueueElement(T1 value1,T2 value2){
		first = value1;
		second = value2;
	}
	
	@Basic @Immutable
	public T1 getFirstValue(){
		return first;
	}
	
	@Basic @Immutable
	public T2 getSecondValue(){
		return second;
	}
}
