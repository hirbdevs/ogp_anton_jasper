package hillbillies.model;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Raw;
import hillbillies.exceptions.IteratorException;
import hillbillies.exceptions.SchedulerException;
import hillbillies.exceptions.TaskException;


/**
 * Class that represents a scheduler, which is a list of prioritised tasks,
 * that is attached to a faction.
 * 
 * @author Jasper Bergmans
 * @version 1.0
 */
public class Scheduler{

	/**
	 * Method to construct a scheduler for a faction that is used to manage all the tasks
	 * for that faction.
	 * 
	 * @param faction
	 * 		The faction for this new Scheduler.
	 * @post
	 * 		The new faction of this scheduler is set to the given initial faction.
	 * 		| new.getFaction() == faction
	 */
	public Scheduler(Faction faction){
		taskList = new ArrayList<Task>();
		this.setFaction(faction);		
	}
	
	//// VARIABLES ////
	
	/**
	 * Contains all the tasks managed by this scheduler.
	 */
	private ArrayList<Task> taskList;
	
	/**
	 * The faction that this scheduler manages.
	 */
	private Faction faction;  
	
	
	
	//// GETTERS & SETTERS ////
	
	/**
	 * Returns the list of tasks that belongs to this scheduler.
	 * @return
	 */
	@Basic @Raw
	public ArrayList<Task> getTaskList(){
		return this.taskList;
	}
	
	/**
	 * Sets the faction of this scheduler to the given faction.
	 * 
	 * @param newFaction
	 * 		The faction that must be assigned to this scheduler.
	 * @post
	 * 		The faction that belongs to this scheduler is set to the given faction.
	 * 		|new.getFaction() == newFaction
	 */
	@Raw
	public void setFaction(Faction newFaction){
		faction = newFaction;
	}
	
	/**
	 * Returns the faction the scheduler manages.
	 * @return
	 */
	@Basic @Raw
	public Faction getFaction(){
		return faction;
	}
	
	
	
	//// ADD/REMOVE TASKS ////
	
	/**
	 * Schedules a given task to the Scheduler.
	 * 
	 * @param task
	 * 		The task that must be scheduled.
	 * @throws SchedulerException 
	 * @effect
	 * 		The given task is added to the tasklist at the correct position.
	 * 		| addTaskToList(task)
	 */
	public void schedule(Task task) throws SchedulerException{
			this.addTaskToList(task);
	}
	
	/**
	 * Add a new task to the scheduler and re-order the taskList.
	 * 
	 * @param newTask
	 * 		The new task that must be added.
	 * @throws SchedulerException 
	 * 		| newTask == null
	 * @post
	 * 		The given task is added to the tasklist at the correct position, meaning that
	 * 		tasks with a higher priority are before the new task and tasks with a lower priority
	 * 		come after the new task if it was not already assigned to this scheduler.
	 * 		| if ( newTask != null && !taskList.contains(newTask))
	 * 		| then new.getTaskList().contains(newTask) == true
	 * @effect
	 * 		The scheduler is added to the list of schedulers of the task.
	 * 		| if ( newTask != null && !taskList.contains(newTask))
	 * 		| then newTask.contains(this) = true
	 */
	public void addTaskToList(Task newTask) throws SchedulerException{
		if ( newTask != null){
			if(taskList.contains(newTask))
				return;
			else{
				this.getTaskList().add(newTask);
				this.sortList();
				newTask.addSchedule(this);
			}	
		}else
			throw new SchedulerException("The task to add is not a valid one");
	}
	
	/**
	 * Removes a task from the taskList and interrupts this task if it was in execution.
	 * 
	 * @param taskToRemove
	 * 		The task that must be removed.
	 * @throws TaskException 
	 * @post
	 * 		If the task was assigned to this scheduler it is removed from this scheduler
	 * 		| if taskToRemove.isAssigned()
	 * 		| then new.getTaskList().contains(taskToRemove) == false
	 * @effect
	 * 		The priority of the task that must be removed is reduced.
	 * 		| taskToRemove.interruptTask()	
	 */
	public void removeTask(Task taskToRemove) throws TaskException{
		if (taskToRemove.isAssigned()){
				taskToRemove.interruptTask();
			}
		taskList.remove(taskToRemove);
	}
	
	/**
	 * Adds multiple tasks to the list of tasks.
	 * 
	 * @param tasks
	 * 		List that contains the different tasks to add.
	 * @throws SchedulerException 
	 * @effect
	 * 		The given tasks are added to the tasklist at the correct position
	 * 		| for(Task taskToAdd: tasks)
	 * 		| 		addTaskToList(taskToAdd)
	 */
	public void addTasksToList(ArrayList<Task> tasks) throws SchedulerException{
		for(int i = 0; i < tasks.size(); i++)
				this.addTaskToList(tasks.get(i));
	}
	
	/**
	 * Removes multiple tasks from the list of tasks.
	 * 
	 * @param tasksToRemove
	 * 		List that contains the different tasks to remove.
	 * @throws TaskException 
	 * @effect
	 * 		The given tasks are removed from the tasklist.
	 * 		| for(Task taskToRemove: tasksToRemove)
	 * 		|		removeTask(taskToRemove)
	 */
	public void removeTasks(ArrayList<Task> tasksToRemove) throws TaskException{
		for(int i = 0; i < tasksToRemove.size(); i++)
			this.removeTask(tasksToRemove.get(i));
	}
	
	/**
	 * Replaces an old task by a new task in the tasklist.
	 * 
	 * @param original
	 * 		The task that must be replaced (removed).
	 * @param newTask
	 * 		The task that replaces the old task (added).
	 * @throws SchedulerException
	 * 		if the tasklist does not contain the original task throw new SchedulerException
	 * 		| !scheduler.contains(original)
	 * @throws TaskException 
	 * @effect
	 * 		The original task is removed from the tasklist.
	 * 		| removeTask(original)
	 * @effect
	 * 		The new task is added to the tasklist.
	 * 		| addTaskToList(newTask)
	 */
	public void replaceTasks(Task original, Task newTask) throws SchedulerException, TaskException{
		if (taskList.contains(original)){
			this.removeTask(original);
			this.addTaskToList(newTask);
		}else
			throw new SchedulerException("Scheduler does not contains the task that must be replaced.");
	}
	
	/**
	 * Sorts the list of tasks from high priority to low priority.
	 * @return
	 * 		Returns the taskList ordered from high priority to low priority.
	 * @post
	 * 		The list of tasks that belong to this scheduler is sorted, starting whit the tasks
	 * 		with the highest priority and ending with the tasks with the lowest priority.
	 * 		| for (int i = 0; i < taskList.size() -1 ; i++)
	 * 		|		taskList.get(i).getPriority() <= taskList.get(i +1).getPriority()
	 * @note
	 * 		In this method a copy of the tasklist is used to sort the original list.
	 */
	private ArrayList<Task> sortList(){
		
		// make a copy
		ArrayList<Task> sortList = new ArrayList<Task>(taskList);
		//sortList = taskList;
		
		// clear the original taskList
		taskList.clear();
		
		Task highestPriorityTask;
		// While the copy contains elements, look for the task with the highest priority, add that
		// task again to the original taskList and remove that task from the copy.
		while (!sortList.isEmpty()) {
			//Reset
			int maxPriority = Integer.MIN_VALUE;
			int sizeList = sortList.size();
			highestPriorityTask = null;
			//Execute
			for (int i = 0; i < sizeList; i++) {
				if (sortList.get(i).getPriority() >= maxPriority) {
					maxPriority = sortList.get(i).getPriority();
					highestPriorityTask = sortList.get(i);
				}
			} 
			
			taskList.add(highestPriorityTask);
			sortList.remove(highestPriorityTask);
		}
		return taskList;
	}
	
	
	//// TASK INFORMATION ////
	
	/**
	 * Returns all the tasks that belong to this scheduler.
	 * @return
	 */
	@Basic @Raw
	public ArrayList<Task> getAllTasks(){
		return this.taskList;
	}
	
	/**
	 * Returns an iterator for all tasks currently managed by this scheduler.
	 * 
	 * @return An iterator that yields all scheduled tasks managed by this
	 *         scheduler, independent of whether they're currently assigned to a
	 *         Unit or not. Completed tasks should not be part of the result.
	 *         The tasks should be delivered by decreasing priority (highest
	 *         priority first).
	 * @throws IteratorException 
	 */
	public Iterator<Task> getAllTasksIterator() throws IteratorException{
		if(this.taskList.isEmpty())
			return null;
		else{
			return this.SchedulerIterator();
			//Iterator<Task> allTaskIterator = new SchedulerIterator(this);
			//return allTaskIterator;
		}
	}
	
	/**
	 * Returns the highest priority task which is not beiing executed at the moment.
	 * @return
	 */ 
	public Task getHighestPriorityTask(){
		for(Task task : this.sortList()){
			if(!task.isAssigned())
				return task;
		}
		return null;	
	}
	
	/**
	 * Returns a boolean reflecting whether all the tasks in a given collection of tasks are 
	 * part of the scheduler or not. 
	 * 
	 * @param tasks
	 * 		Collection of tasks that must be checked.
	 * @return
	 * 		True if and only if all the tasks in the collection are part of the scheduler. 
	 * 		| for(Task taskToCheck: tasks)
	 * 		| 	if (! taskList.contains(taskToCheck))
	 * 		|	then result == false
	 * @throws IteratorException 
	 */
	public boolean areTasksPartOf(Collection<Task> tasks) throws IteratorException{
		Collection<Task> tasksCopy = new ArrayList<Task>(tasks);
		Iterator<Task> taskIterator = this.SchedulerIterator();
		while(taskIterator.hasNext()){
			Task nextTask = taskIterator.next();
			if (tasksCopy.contains(nextTask))
				tasksCopy.remove(nextTask);
			if (tasksCopy.isEmpty())
				return true;
		}
		return false;	
	}
	
	
	/**
	 * An object that implements the possible conditions to check in the satisfiesCondition method.
	 */
	public final Condition myConditionClass = new Condition(this);

	/**
	 * Returns a list of methods that are part of this scheduler and satisfie the given condition.
	 * @param nameCondition
	 * 		The name of the condition that the method evaluates to.
	 * @return
	 * 		A list of tasks that are part of this scheduler and that satisfie the given condition.
	 * 		| for(Task taskToCheck: taskList)
	 * 		| 		if(taskToCheck.Condition == true)
	 * 		|			result.add(taskToCheck)
	 * 		| Return result
	 * @throws SchedulerException 
	 */
	public ArrayList<Task> satisfiesCondition(String nameCondition) throws SchedulerException{
		ArrayList<Task> result = new ArrayList<Task>();
		Iterator<Task> taskIterator = this.SchedulerIterator();
		//Iterator<Task> taskIterator = new SchedulerIterator(this);
		while(taskIterator.hasNext()){
			Task nextTask = taskIterator.next();
				try {
					Method myMethod = myConditionClass.getClass().getDeclaredMethod(nameCondition, Task.class);
					if ((boolean)(myMethod.invoke(myConditionClass, nextTask)))
						result.add(nextTask);
				} catch ( SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
					throw new SchedulerException("The condition named: "+ nameCondition + " does not exist.");	
				} catch (NoSuchMethodException e) {
					throw new SchedulerException("The condition named: "+ nameCondition + " does not exist.");
				}				
		}
		return result;
	}
	
	public Iterator<Task> SchedulerIterator() {
		return new Iterator<Task>() {
			
			/**
			 * The current task that is handled while iterating.
			 */
			private Task current = null;
			
			/**
			 * Index of the iteration.
			 */
			private int counter = -1;
			
			/**
			 * Copy of the list of tasks that are managed by the scheduler over which this iterator runs.
			 */
			private ArrayList<Task> copySortList = new ArrayList<Task>(taskList);
			
			/**
			 * Returns the copy of the Tasklist that belongs to the scheduler.
			 * @return
			 */
			@Basic @Raw
			private ArrayList<Task> getCopySortList(){
				return copySortList;
			}
			
			/**
			 * Returns the value of the counter (index of the iteration).
			 * @return
			 */
			@Basic @Raw
			private int getCounterValue(){
				return counter;
			}
			
			/**
			 * Sets the value of the counter to the given value.
			 * @param newValue
			 * 		The new walue of the counter
			 * @post
			 * 		The value of the counter is equal to the given value.
			 */
			@Raw
			private void setCounterValue(int newValue){
				counter = newValue;
			}
			
			/**
			 * Returns the first task in the copySortList.
			 * @return
			 * @throws IteratorException 
			 */
			@Basic @Raw
			private Task getFirstTask() {
				if(this.getCopySortList().isEmpty())
					return null;
				else {
					this.setCounterValue(0);
					return this.getCopySortList().get(this.getCounterValue());
				}
			}
			
			/**
			 * Returns the next task while iterating over the scheduler.
			 * @effect
			 * 		The value of the counter is increased by one.
			 * @return
			 */
			@Basic @Raw
			private Task getNextTask(){
				this.setCounterValue(this.getCounterValue()+1);
				return this.getCopySortList().get(this.getCounterValue());
			}
			
			/**
			 * Check whether this iterator has a next element or not.
			 * @return
			 * 		True if and only if this iterator has a next element, so if the value of the counter
			 * 		is not equal to the size of the list.
			 */
			@Override
			public boolean hasNext() {
				if (this.getCounterValue() == this.getCopySortList().size()-1)
					return false;
				else
					return true;
			}

			/**
			 * Returns the next task while iterating. 
			 * @return
			 * 		The next element while iterating over the scheduler.
			 */
			@Override
			public Task next() {
				Task result = null;
				if(current != null && this.hasNext()){
					current = getNextTask();
					result = current;
				}else if (current == null){
					current = getFirstTask();
					result = current;
				}
				return result;
			}
			
		};
	}
}
