package hillbillies.model;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import hillbillies.exceptions.IteratorException;
import hillbillies.exceptions.SchedulerException;
import hillbillies.exceptions.TaskException;
import hillbillies.part2.listener.DefaultTerrainChangeListener;
import hillbillies.part3.facade.Facade;
import hillbillies.part3.programs.TaskParser;

/**
 * TestClass for the class Scheduler, tests all the public methods.
 * @author Jasper Bergmans
 * @version 1.0
 *
 */
public class SchedulerTest {
	
	
	//// SETUP FOT TESTS ////
	
	 Scheduler testScheduler;
	 Faction testFaction;
	 Unit testUnit;	
	 Facade testFacade;
	 World testWorld;
	
	//private static final int TYPE_AIR = 0;
	private static final int TYPE_ROCK = 1;
	private static final int TYPE_TREE = 2;
	private static final int TYPE_WORKSHOP = 3;
	
	private Task task10;
	private Task task100;
	private Task task200;
	private Task taskMin100;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		int[][][] types = new int[3][3][3];
		types[1][1][0] = TYPE_ROCK;
		types[1][1][1] = TYPE_ROCK;
		types[1][1][2] = TYPE_TREE;
		types[2][2][2] = TYPE_WORKSHOP;
		
		testFacade = new Facade();
		testWorld = testFacade.createWorld(types, new DefaultTerrainChangeListener());
		testUnit = testFacade.createUnit("Test", new int[] { 0, 0, 0 }, 50, 50, 50, 50, true);
		testFacade.addUnit(testUnit, testWorld);
		testFaction = testFacade.getFaction(testUnit);
		testScheduler = testFacade.getScheduler(testFaction);
		
		//create task with priority 10
		List<Task> tasks10 = TaskParser.parseTasksFromString(
				"name: \"work task\"\npriority: 10\nactivities: work selected;", testFacade.createTaskFactory(),
				Collections.singletonList(new int[] { 1, 1, 1 }));
		task10 = tasks10.get(0); 
		//create task with priority 100
		List<Task> tasks100 = TaskParser.parseTasksFromString(
				"name: \"work task\"\npriority: 100\nactivities: work selected;", testFacade.createTaskFactory(),
				Collections.singletonList(new int[] { 1, 1, 1 }));
		task100 = tasks100.get(0);
		//create task with priority 200
		List<Task> tasks200 = TaskParser.parseTasksFromString(
				"name: \"work task\"\npriority: 200\nactivities: work selected;", testFacade.createTaskFactory(),
				Collections.singletonList(new int[] { 1, 1, 1 }));
		task200 = tasks200.get(0);
		//create task with priority -100
		List<Task> tasksMin100 = TaskParser.parseTasksFromString(
				"name: \"work task\"\npriority: -100\nactivities: work selected;", testFacade.createTaskFactory(),
				Collections.singletonList(new int[] { 1, 1, 1 }));
		taskMin100 = tasksMin100.get(0);
	}

	
	//// TEST CONSTRUCTOR /////
	
	@Test
	public void SchedulerConstructed() {
		this.testScheduler.getTaskList().clear();
		assertEquals(testScheduler.getFaction(), testFaction);
	}
		
	
	//// TEST GETTERS AND SETTERS ////
	
	@Test
	public void GetTasklist() throws SchedulerException {
		this.testScheduler.getTaskList().clear();
		this.testScheduler.addTaskToList(task10);
		this.testScheduler.addTaskToList(task100);
		assertEquals(this.testScheduler.getTaskList().get(1), task10);
		this.testScheduler.getTaskList().clear();
	}
	
	@Test
	public void getEmptyTaskList(){
		this.testScheduler.getTaskList().clear();
		assertTrue(this.testScheduler.getTaskList().isEmpty());
		this.testScheduler.getTaskList().clear();
	}
	
	@Test
	public void setFaction(){
		this.testScheduler.getTaskList().clear();
		final Faction	testFaction2 = new Faction(2);
		testScheduler.setFaction(testFaction2);
		assertTrue(testScheduler.getFaction() == testFaction2);
	}
	
	
	//// TEST ADD/REMOVE TASKS ////
	
	@Test
	public void ScheduleTask() throws SchedulerException{
		this.testScheduler.schedule(task10);
		assertTrue(this.testScheduler.getTaskList().contains(task10));
		this.testScheduler.getTaskList().clear();
	}
	
	@Test (expected = SchedulerException.class)
	public void ScheduleTaskSchedulerException() throws SchedulerException{
		this.testScheduler.getTaskList().clear();
		this.testScheduler.schedule(null);
	}
	
	@Test
	public void addTaskToList() throws SchedulerException{
		this.testScheduler.getTaskList().clear();
		this.testScheduler.addTaskToList(task10);
		assertTrue(this.testScheduler.getTaskList().contains(task10));
		this.testScheduler.addTaskToList(task10);
		assertEquals(this.testScheduler.getTaskList().size(),1);
		this.testScheduler.addTaskToList(task100);
		assertTrue(this.testScheduler.getTaskList().get(0)==task100);
		assertTrue(this.testScheduler.getTaskList().get(1)==task10);
		this.testScheduler.getTaskList().clear();
	}
	
	@Test (expected = SchedulerException.class)
	public void addTaskToListNull() throws SchedulerException{
		this.testScheduler.getTaskList().clear();
		this.testScheduler.addTaskToList(null);
		this.testScheduler.getTaskList().clear();
	}
	
	@Test
	public void removeTaskFromList() throws SchedulerException, TaskException{
		this.testScheduler.getTaskList().clear();
		this.testScheduler.addTaskToList(task10);
		this.testScheduler.addTaskToList(task100);
		this.testScheduler.addTaskToList(task200);
		this.testScheduler.removeTask(task100);
		assertFalse(this.testScheduler.getTaskList().contains(task100));
		this.testScheduler.removeTask(task100);
		assertFalse(this.testScheduler.getTaskList().contains(task100));
		//what about sorting?
		assertEquals(this.testScheduler.getTaskList().get(0),task200);
		assertEquals(this.testScheduler.getTaskList().get(1),task10);
		this.testScheduler.getTaskList().clear();
	}
	
	@Test
	public void addTasksToList() throws SchedulerException{
		ArrayList<Task> taskArray = new ArrayList<Task>();
		taskArray.add(task10);
		taskArray.add(task200);
		taskArray.add(task100);
		taskArray.add(task10);
		this.testScheduler.addTasksToList(taskArray);
		assertEquals(this.testScheduler.getTaskList().get(0), task200);
		assertEquals(this.testScheduler.getTaskList().get(1), task100);
		assertEquals(this.testScheduler.getTaskList().get(2), task10);
		this.testScheduler.getTaskList().clear();
	}
	
	@Test
	public void removeTasksFromList() throws SchedulerException, TaskException{
		ArrayList<Task> taskArray = new ArrayList<Task>();
		taskArray.add(task10);
		taskArray.add(task200);
		taskArray.add(task100);
		this.testScheduler.addTasksToList(taskArray);
		taskArray.remove(task100);
		taskArray.add(task10);
		this.testScheduler.removeTasks(taskArray);
		assertFalse(this.testScheduler.getTaskList().contains(task10));
		assertEquals(this.testScheduler.getTaskList().get(0), task100);
		assertFalse(this.testScheduler.getTaskList().contains(task200));
		this.testScheduler.getTaskList().clear();	
	}
	
	@Test
	public void replaceTasks() throws SchedulerException, TaskException{
		this.testScheduler.addTaskToList(task10);
		this.testScheduler.replaceTasks(task10, task100);
		assertFalse(this.testScheduler.getTaskList().contains(task10));
		assertTrue(this.testScheduler.getTaskList().contains(task100));
		this.testScheduler.getTaskList().clear();
	}
	
	@Test (expected = SchedulerException.class)
	public void replacTasksTaskNotInList() throws SchedulerException, TaskException{
		this.testScheduler.replaceTasks(task10, task100);
	}
	
	
	//// TEST INFORMATION ON TASKS ////
	
	@Test
	public void getAllTasks() throws SchedulerException{
		ArrayList<Task> taskArray = new ArrayList<Task>();
		taskArray.add(task10);
		taskArray.add(task200);
		taskArray.add(task100);
		this.testScheduler.addTasksToList(taskArray);
		assertTrue(this.testScheduler.getAllTasks().size() == 3);
		assertEquals(this.testScheduler.getAllTasks().get(0),task200);
		assertEquals(this.testScheduler.getAllTasks().get(1),task100);
		assertEquals(this.testScheduler.getAllTasks().get(2),task10);
		this.testScheduler.getTaskList().clear();
	}
	
	@Test
	public void getAllTasksEmpty(){
		assertTrue(this.testScheduler.getAllTasks().size() == 0);
	}
	
	@Test
	public void getAllTasksIterator() throws SchedulerException, IteratorException{
		this.testScheduler.addTaskToList(task10);
		this.testScheduler.addTaskToList(task100);
		this.testScheduler.addTaskToList(task200);
		assertEquals(this.testScheduler.getAllTasksIterator().next(),task200);
		assertTrue(this.testScheduler.getAllTasksIterator().hasNext());

		this.testScheduler.getTaskList().clear();
	}
	
	@Test
	public void getAllTasksIteratorEmptyCase() throws SchedulerException, IteratorException{
		this.testScheduler.getTaskList().clear();
		assertEquals(this.testScheduler.getAllTasksIterator(), null);
	}
	
	@Test
	public void getHighestPriorityTaskRegular() throws SchedulerException{
		this.testScheduler.addTaskToList(task10);
		this.testScheduler.addTaskToList(task200);
		this.testScheduler.addTaskToList(task100);
		assertEquals(this.testScheduler.getHighestPriorityTask(),task200);
		this.testScheduler.getTaskList().clear();
	}
	
	@Test
	public void getHighestPriorityTaskAllTasksAssigned() throws SchedulerException, TaskException{
		this.testScheduler.addTaskToList(task200);
		task200.assignUnit(testUnit);
		assertEquals(this.testScheduler.getHighestPriorityTask(),null);
		this.testScheduler.getTaskList().clear();	
	}
	
	@Test
	public void getHighestPriorityTaskOneTaskAssigned() throws SchedulerException, TaskException{
		this.testScheduler.addTaskToList(task200);
		this.testScheduler.addTaskToList(task10);
		this.testScheduler.addTaskToList(task100);
		task200.assignUnit(testUnit);
		assertEquals(this.testScheduler.getHighestPriorityTask(),task100);
		this.testScheduler.getTaskList().clear();
	}
	
	@Test
	public void getHighestPriorityTaskNoTasksInScheduler() throws SchedulerException{
		this.testScheduler.getTaskList().clear();
		assertEquals(this.testScheduler.getHighestPriorityTask(),null);
	}

	@Test
	public void areTasksPartOf() throws SchedulerException, IteratorException{
		this.testScheduler.addTaskToList(task200);
		this.testScheduler.addTaskToList(task10);
		this.testScheduler.addTaskToList(task100);
		
		List<Task> taskList = new ArrayList<Task>();
		taskList.add(task10);
		taskList.add(task100);
	
		assertTrue(this.testScheduler.areTasksPartOf(taskList));
		this.testScheduler.getTaskList().clear();
	}
	
	@Test
	public void areTasksPartOfFalse() throws SchedulerException, IteratorException{
		this.testScheduler.addTaskToList(task200);
		this.testScheduler.addTaskToList(task10);
		
		List<Task> taskList = new ArrayList<Task>();
		taskList.add(task10);
		taskList.add(task100);
		
		assertFalse(this.testScheduler.areTasksPartOf(taskList));
		this.testScheduler.getTaskList().clear();
	}
	
	@Test
	public void areTasksPartOfEmpty() throws SchedulerException, IteratorException{
		List<Task> taskList = new ArrayList<Task>();
		taskList.add(task10);
		taskList.add(task100);
		
		assertFalse(this.testScheduler.areTasksPartOf(taskList));
		this.testScheduler.getTaskList().clear();
	}
	
	@Test
	public void satisfiesCondition() throws ClassNotFoundException, IteratorException, SchedulerException{
		this.testScheduler.getTaskList().clear();
		this.testScheduler.addTaskToList(task200);
		this.testScheduler.addTaskToList(taskMin100);
		assertFalse(this.testScheduler.satisfiesCondition("hasPositivePriority").contains(taskMin100));
		assertEquals(this.testScheduler.satisfiesCondition("hasPositivePriority").get(0),task200);
		this.testScheduler.getTaskList().clear();
	}
	
	@Test
	public void satisfiesConditionOnlyNegative() throws SchedulerException, ClassNotFoundException, IteratorException{
		this.testScheduler.getTaskList().clear();
		this.testScheduler.addTaskToList(taskMin100);
		assertFalse(this.testScheduler.satisfiesCondition("hasPositivePriority").contains(taskMin100));
		assertTrue(this.testScheduler.satisfiesCondition("hasPositivePriority").size()==0);
		this.testScheduler.getTaskList().clear();
	}
	
	@Test
	public void satisfiesConditionIsAssigned() throws SchedulerException, TaskException{
		this.testScheduler.getTaskList().clear();
		this.testScheduler.addTaskToList(taskMin100);
		taskMin100.assignUnit(testUnit);
		this.testScheduler.addTaskToList(task100);
		assertTrue(this.testScheduler.satisfiesCondition("isAssigned").size()==1);
		assertTrue(this.testScheduler.satisfiesCondition("isAssigned").get(0) == taskMin100);
		assertFalse(this.testScheduler.satisfiesCondition("isAssigned").contains(task100));
	}
	
	@Test
	public void satisfiesConditionEmptyList() throws SchedulerException{
		this.testScheduler.getTaskList().clear();
		assertTrue(this.testScheduler.satisfiesCondition("hasPositivePriority").size() == 0);
		this.testScheduler.getTaskList().clear();
	}
	
//	@Test
//	public void taskExecuted() throws ModelException, SchedulerException{
//		//this.testScheduler.addTaskToList(task200);
//		Facade facade = new Facade();
//		
//		int[][][] types = new int[3][3][3];
//		types[1][1][0] = TYPE_ROCK;
//		types[1][1][1] = TYPE_ROCK;
//		types[1][1][2] = TYPE_TREE;
//		types[2][2][2] = TYPE_WORKSHOP;
//
//		World world = facade.createWorld(types, new DefaultTerrainChangeListener());
//		Unit unit = facade.createUnit("Test", new int[] { 0, 0, 0 }, 50, 50, 50, 50, true);
//		facade.addUnit(unit, world);
//		Faction faction = facade.getFaction(unit);
//
//		Scheduler scheduler = facade.getScheduler(faction);
//
//		List<Task> tasks = TaskParser.parseTasksFromString(
//				"name: \"work task\"\npriority: 1\nactivities: work selected;", facade.createTaskFactory(),
//				Collections.singletonList(new int[] { 1, 1, 1 }));
//		Task task = tasks.get(0);
//		facade.schedule(scheduler, task);
//		advanceTimeFor(facade, world, 100, 0.02);
//		
//		assertTrue(scheduler.getTaskList().isEmpty());
//		
//		scheduler.getTaskList().clear();
//	}
//	
//	//// HELPER METHOD ////
//	
//	/**
//	 * Helper method to advance time for the given world by some time.
//	 * 
//	 * @param time
//	 *            The time, in seconds, to advance.
//	 * @param step
//	 *            The step size, in seconds, by which to advance.
//	 */
//	private static void advanceTimeFor(IFacade facade, World world, double time, double step) throws ModelException {
//		int n = (int) (time / step);
//		for (int i = 0; i < n; i++)
//			facade.advanceTime(world, step);
//		facade.advanceTime(world, time - n * step);
//	}
	
		//TODO pdf nalezen op condities scheduler
}
