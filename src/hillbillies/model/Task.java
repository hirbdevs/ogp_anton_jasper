package hillbillies.model;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Raw;
import hillbillies.exceptions.TaskException;
import programmingLanguage.ExpressionException;
import programmingLanguage.StatementException;
import programmingLanguage.Statements.Statement;

/**
 * A class of tasks that can belong to one or more schedulers and can be executed by a unit.
 * Every task has a statement that belongs to it.
 * 
 * @invar
 * 		Each non-terminated task must have a statement attached to it.
 * 		| if(!isTerminated)
 * 		|		statement != null
 * @invar
 * 		Only tasks that are assigned can have an assignee
 * 		| if(!isAssigned)
 * 		|		assignee == null
 * 
 * @version 1.0 
 * @author Jasper Bergmans
 */
public class Task {
	
	/**
	 * Method to construct a task that can be added to a scheduler with the given parameters.
	 * 
	 * @param name
	 * 		The name for this new task.
	 * @param priority
	 * 		The priority for this new task.
	 * @param activity
	 * 		The Statement that indicates what this new taks must do.
	 * @param selectedCube
	 * 		The cube that was selected in the GUI.
	 * @throws StatementException 
	 * @throws ExpressionException 
	 * @post
	 * 		The new name of this task is equal to the given intial name.
	 * 		|new.getname() == name
	 * @post
	 * 		The new priority of this task is set to the given initial priority.
	 * 		|new.getPriority() == priority
	 * @post
	 * 		The new statement of this task is set to the given initial statement.
	 * 		|new.getStatement() == statement
	 * @post
	 * 		The new task is Well formed
	 * 		|new.isWellFormed() == true;
	 */
	public Task(String name, int priority, Statement activity,int[] selectedCube) throws TaskException, ExpressionException, StatementException{
			this.setName(name);
			this.setPriority(priority);
			this.setStatement(activity);
			this.setSelectedCube(selectedCube);
			this.isWellFormed(activity);			
	}
	

	//// VARIABLES ////
	
	/**
	 * The name of this task.
	 */
	private String taskName;  
	
	/**
	 * The priority of this task.
	 */
	private int priority;
	
	/**
	 * The statement that needs to be executed in this task.
	 */
	private Statement statement;
	
	/**
	 * The unit to which this task is assigned.
	 */
	private Unit assignee = null;
	
	/**
	 * Selected cube in the GUI associated with this task.
	 */
	private int[] selectedCube;

	/**
	 * A set containing all the schedulers where this task is part of
	 */
	private Set<Scheduler> schedulers = new HashSet<Scheduler>();	
	
	
	
	//// GETTERS-SETTERS ////
	
	/**
	 * Returns the Schedulers to which this task is part of.
	 * @return
	 */
	@Basic @Raw
	public Set<Scheduler> getSchedulers(){
		return this.schedulers;
	}
	/**
	 * Sets the selected cube to the given array.
	 * 
	 * @param thisSelectedCube
	 * 		Contains an x, y and z parameter.
	 * @post
	 * 		The selectedCube that belongs to this task is set to the given cube.
	 * 		|new.getSelectedCube() == thisSelectedCube
	 */
	@Raw
	public void setSelectedCube(int[] thisSelectedCube){
		this.selectedCube = thisSelectedCube;
	}
	
	/**
	 * Returns the selected cube.
	 * @return
	 */
	@Basic @Raw
	public int[] getSelectedCube(){
		return this.selectedCube;
	}
	
	/**
	 * Sets the name of this task to the given name.
	 * 
	 * @param name
	 * 		The name that must be assigned to this task.
	 * @post
	 * 		The taskname that belongs to this task is set to the given name.
	 * 		|new.getName() == name
	 */
	@Raw
	public void setName(String name){
		taskName = name;
	}
	
	/**
	 * Returns the name of the task.
	 * @return
	 */
	@Basic @Raw
	public String getName(){
		return taskName;
	}
	
	/**
	 * Sets the priority for this task to the given priority
	 * 
	 * @param thisPriority
	 * 		The priority value that must be set.
	 * @Post The priority is equal to the given priority.
	 * 		|new.getPriority() == thisPriority;
	 */
	@Raw
	public void setPriority(int thisPriority){
		priority = thisPriority;
		if(this.priority <= -1000)
			this.priority = -1000;
	}
	
	/**
	 * Returns the priority of the task.
	 * @return
	 */
	@Basic @Raw
	public int getPriority(){
		return priority;
	}
	
	/**
	 * Sets the value of statement that belongs to this task to the given statement.
	 * @param statement
	 * 		The new statement that must be set to this task
	 * @throws CloneNotSupportedException 
	 * @post
	 * 		The statement that belongs to this task is set to the given statement.
	 * 		|new.getStatement() = statement
	 * @effect
	 * 		This task is assigned to the given statement.
	 * 		|statement.setTask(this)
	 */
	@Raw
	public void setStatement(Statement statement) {
		this.statement = statement;
		this.statement.setTask(this);
	}
	
	/**
	 * Returns the statement that belongs to this task.
	 * @return
	 */
	@Basic @Raw
	public Statement getStatement(){
		return this.statement;
	}
			
	/**
	 * Sets the assignee for this task to the given unit.
	 * 
	 * @param newAssignee
	 * 		The new unit that must be assigned to this task.
	 * @post
	 * 		The unit that is assigned to this task is set to the given unit.
	 */
	@Raw
	public void setAssignedUnit(Unit newAssignee){
		assignee = newAssignee;
	}
	
	/**
	 * Returns the the unit that is assigned to this task.
	 * @return
	 */
	@Basic @Raw
	public Unit getAssignedUnit(){
		return assignee;
	}
	
	
	
	//// TASK FUNCTIONALITY ////
	
	/**
	 * Returns a boolean reflecting whether a unit is assigned to this task or not.
	 * @return
	 * 		True if and only if this task is assigned.
	 * 		|result == (assignee != null)
	 */
	public boolean isAssigned(){
		if (assignee == null)
			return false;
		else return true;
	}
	
	/**
	 * Adds a new scheduler to the set of schedulers to which this task belongs.
	 * 
	 * @param scheduler
	 * 		The scheduler that must be added.
	 * @post
	 * 		The schedule is added to the list of schedules that belong to this task.
	 * 		|new.getSchedulers().contains(scheduler)
	 */
	public void addSchedule(Scheduler scheduler){
		if(!this.getSchedulers().contains(scheduler))
			this.getSchedulers().add(scheduler);
		
	}
	
	/**
	 * Removes the given scheduler from the set of schedulers to which this task belongs.
	 * 
	 * @param scheduler
	 * 		The scheduler form which the task must be removed.
	 * @throws TaskException
	 * 		If the task was not assigned to the given scheduler an exception will be thrown.
	 * 		| !schedulers.contains(scheduler)
	 */
	public void removeScheduler(Scheduler scheduler) throws TaskException{
		if(this.schedulers.contains(scheduler))
			this.schedulers.remove(scheduler);
		else
			throw new TaskException("task was not assigned to the given scheduler");
	}
	
	/**
	 * Returns a boolean reflecting whether this task is already scheduled in a scheduler or not.
	 * @return
	 * 	  	True if and only if this task has a scheduler.
	 * 		| result == (schedulers.size()> 0)
	 */
	public boolean isScheduled(){
		if (this.schedulers.size()>0)
			return true;
		else return false;
	}
	
	/**
	 * Method to execute this task. A Unit will be assigned and the statements that belong
	 * to this task will be executed.
	 * 
	 * @param executingUnit
	 * 		The Unit that must execute the task.
	 * @throws TaskException
	 * 	 	If the task was already assigned to another unit, throw new TaskException.
	 * 		| isAssigned()
	 * @throws TaskException
	 * 		If the task was not scheduled yet, throw new TaskException.
	 * 		| isScheduled()
	 * @throws IllegalStateException
	 * 		This task is already terminated
	 * 		| isTerminated()
	 * @effect
	 * 		The task is assigned to the given unit.
	 * 		| assignUnit(executingUnit)
	 * @throws StatementException 
	 * @throws ExpressionException  
	 */
	public void executeTask(Unit executingUnit) throws IllegalStateException, TaskException, StatementException, ExpressionException{
		if(this.isTerminated)
			throw new IllegalStateException("Terminated task!");
		if (this.isScheduled()){
			if (!this.isAssigned()){
				this.assignUnit(executingUnit);
				this.statement.setTask(this);
				this.statement.execute();
			}
			else
				throw new TaskException("Task is already assigned to a Unit.");
		}else
			throw new TaskException("Task is not scheduled yet.");	
	}
	
	/**
	 * Assigns the task to the given unit.
	 * 
	 * @param thisAssignee
	 * 		The Unit that is required to execute the task.
	 * 
	 * @throws TaskException
	 * 		If the given unit equal null, throw new TaskException.
	 * 		| thisAssignee == null
	 * @throws TaskException
	 * 		If the task was already assigned to another unit, throw new TaskException.
	 * 		| isAssigned()
	 * @effect
	 * 		The given unit will have this task assigned to it.
	 * 		| thisAssignee.AssignTask()
	 * @post
	 * 		This task is assigned to the unit.
	 * 		| new.getAssignedUnit() == thisAssignee
	 */
	public void assignUnit(Unit thisAssignee) throws TaskException{	
		if (thisAssignee == null)
			throw new TaskException("The unit to assign the task to cannot be null.");	
		if (this.isAssigned())
			throw new TaskException("Task was already assigned to another Unit.");
		else{
			this.setAssignedUnit(thisAssignee);
			thisAssignee.assignTask(this);
		}
	}
	
	/**
	 * Interrupts the task while in execution.
	 * 
	 * @throws TaskException
	 * 		If the task was not in execution yet, throw an exception.
	 * 		| this.isAssigned()
	 * @post
	 * 		The priority of the task is decreased
	 * 		| new.getPriority() = this.getPriority() - 1
	 * @effect
	 * 		The assignee of the task is replaced by null.
	 * 		| deassignUnit()
	 */
	public void interruptTask() throws TaskException{
		if(this.isAssigned()){
			this.deassignUnit();
			this.setPriority(this.getPriority()-1);
		} else
			throw new TaskException("Task was not in execution yet.");
	}
	
	/**
	 * Deassign the unit that is currently assigned to the task.
	 * 
	 * @throws TaskException
	 * 		Throw an exception when the task was not assigned to a unit yet.
	 * 		|getAssignedUnit() == null
	 * @post
	 * 		The assigned unit is replaced by a null value.
	 * 		| new.getAssignedUnit() == null
	 * @effect
	 * 		The assigned unit losses the task it was assigned to.
	 * 		| getAssignedUnit().deAssignTask()
	 * @effect
	 * 		The assigned unit of this task is set to null.
	 * 		| setAssignedUnit(null)
	 */
	public void deassignUnit(){
			this.getAssignedUnit().deAssignTask();
			this.setAssignedUnit(null);
	}
	
	/**
	 * Removes a task from the schedulers and makes the Unit available for other tasks
	 * when this task is finished.
	 * @throws TaskException 
	 * 
	 * @post
	 * 		The task is removed from all the schedulers' tasklist's.
	 * 		| for(Scheduler nextScheduler: schedulers)
	 * 		|      nextScheduler.contains(this) == false
	 * @effect
	 * 		The assigned unit is replaced by a null value.
	 * 		| deassignUnit()
	 * @effect
	 * 		This task is terminated.
	 * 		| terminate()
	 * @effect
	 * 		The task no longer references to a statement.
	 * 		| terminate()
	 * @effect
	 * 		The task no longer references to a selected cube.
	 * 		| terminate()
	 * @effect
	 * 		The statement attached to this task is terminated.
	 * 		| terminate()
	 */
	public void finishTask(){
		this.deassignUnit();

		Iterator<Scheduler> schedulerIterator = schedulers.iterator();
		while (schedulerIterator.hasNext()){
			try {
				Scheduler nextScheduler = schedulerIterator.next();
				nextScheduler.removeTask(this);
			} catch (TaskException e) {
				e.getMessage();
			}
		}
		//Destructor
		this.terminate();
	}
	
	/**
	 * Contains all the variables with their values declared in the task.
	 */
	public HashMap<String, Object> Variables = new HashMap<String, Object>();
		
	
	
	//// DESTRUCTOR ////
	
	/**
	 * Terminate this task.
	 * 
	 * @post this task is terminated.
	 * 			| new.isTerminated()
	 * @post The task no longer references to a statement
	 * 			| new.getStatement() == null
	 * @effect The task no longer references to a selectedCube
	 * 			| new.getSelectedCube() = null
	 * @effect The statement attached to this task is terminated.
	 * 			| new.getStatement.isTerminated()	
	 */
	public void terminate(){
		if(!this.isTerminated()){
			this.isTerminated = true;
			this.statement = null;
			this.setSelectedCube(null);
		}
	}
		
	/**
	 * Check whether this task is terminated.
	 * @return
	 */
	@Basic @Raw
	public boolean isTerminated(){
		return isTerminated;
	}
	
	/**
	 * Variable registering whether this task is terminated.
	 */
	private boolean isTerminated;
	
	
	//// WELL FORMEDNESS ///
	
	/**
	 * Returns a boolean reflecting whether the given activity is well-formed or not.
	 * 
	 * @param activity
	 * 		The activity that is checked to be well-Formed.
	 * @return
	 * 		True if and only if the activity is Well-Formed.
	 * 		| result == (activity.isWellFormed)	
	 * @throws ExpressionException
	 * @throws StatementException
	 * @throws TaskException
	 * 		Throw an exception when the activity is not well-formed
	 * 		| !(activity.isWellFormed()
	 */
	public boolean isWellFormed(Statement activity) throws ExpressionException, StatementException, TaskException{
		activity.setTask(this);
		boolean result = activity.isWellFormed();
		Variables.clear();
		if (result == false) throw new TaskException("The activity is not well formed.");
		else return result;
	}
}
