package hillbillies.model;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Value;

@Value
public enum TerrainType {
	
	air (0),
	rock(1),
	tree(2),
	workshop(3);
	
	private int number;
	
	TerrainType(int value){
		number = value;
	}
	
	@Basic
	public int getNumber(){
		
		return number;
	}
}
