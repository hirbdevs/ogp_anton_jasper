package hillbillies.model;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
//import java.util.Iterator;
//import java.util.List;
import java.util.Random;
import java.util.Set;
import be.kuleuven.cs.som.annotate.*;
import hillbillies.exceptions.GameObjectException;
import hillbillies.exceptions.IllegalNameException;
import hillbillies.exceptions.IllegalPositionException;
import hillbillies.exceptions.SchedulerException;
import hillbillies.exceptions.TaskException;
import hillbillies.exceptions.UnitException;
import hillbillies.exceptions.WorldException;
import ogp.framework.util.Util;
import programmingLanguage.ExpressionException;
import programmingLanguage.StatementBreakException;
import programmingLanguage.StatementException;
import programmingLanguage.Statements.AttackStatement;
import programmingLanguage.Statements.FollowStatement;
import programmingLanguage.Statements.MoveToStatement;
import programmingLanguage.Statements.Statement;
import programmingLanguage.Statements.WorkStatement;

// Anton Van Gompel, HIRB, Fase 3
// Jasper Bergmans, HIRB, Fase 3
// link to repository: https://bitbucket.org/hirbdevs/ogp_anton_jasper/overview

/**
 * A class that implements a Unit.
 * 
 * @invar	the weight of a unit must at all times be bigger or equal to (strength + agility)/2 and smaller or equal to 200
 * 			|this.getWeight() >= (this.getStrenght+this.getAgility)/2 && this.getWeight() <= 200
 * 
 * @invar	the attributes must at all times be within the legal boundaries.
 * 			| this.getStrength() >= 0 && this.getStrenght() <= 200
 * 			| this.getAgility() >= 0 && this.getAgility() <= 200
 * 			| this.getToughness() >= 0 && this.getToughness() <= 200
 * 			| this.getHitpoints() >= 0 && this.gethitpoints <= 200*(this.getWeight()/100)*(this.getToughness()/200)
 * 			| this.getStaminapoints() >= 0 && this.getstaminapoints <= 200*(this.getWeight()/100)*(this.getToughness()/200)
 * @invar	the orientation of the unit must all times be within the legal boundaries.
 * 			|0 <= this.getOrientation() && this.getOrientation() < 2*Math.Pi 
 * @invar	A unit must always belong to a faction.
 * 			| this.getFaction() != null 
 * 
 * @author Anton Van Gompel
 * @author Jasper Bergmans
 * @version	1.0
 *
 */
public class Unit extends GameObject {
	
	
	/**
	 * A constructor for this Unit. Constructs a unit with a given name, position, weight, strength, agility,
	 * toughness and a boolean indicating whether default behaviour is enabled or not.
	 * 
	 * @param 	name
	 * 			The name we want to set for the unit.
	 * @param 	position
	 * 			The initial position of the unit in the game world, containing an x, y and z coordinate.
	 * @param 	weight
	 * 			The initial weight of the unit.
	 * @param 	strength
	 * 			The initial strength for the unit.
	 * @param 	agility
	 * 			The initial agility for the unit.
	 * @param 	toughness
	 * 			The initial toughness for the unit.
	 * @param 	enableDefaultBehaviour
	 * 			Boolean to indicate whether a unit can execute default behaviour initially.
	 * 
	 * @throws WorldException
	 * 		if a unit cannot be added to the unitSet or if it is not possible to add the unit to a faction, throw new WorldException.
	 * @throws UnitException
	 * 		if an illegal name is given to a unit, throw new IllegalNameException.
	 * @throws IllegalPositionException 
	 * @throws GameObjectException 
	 * 
	 * @effect 	A unit is added to the set of units in the class World.
	 * 			| World.getInstance().addUnitToSet(this)
	 * @effect 	A unit is added to a faction
	 * 			| World.getInstanche().addUnitToFaction(this)
	 * @effect	if enableDefaultBehaviour is true, then the unit starts default behaviour.
	 * 			| if (enableDefaultBehaviour == true)
	 * 			| then setMovementType(MovementType.defaultBehaviour)
	 * 			| else setMovementType(MovementType.nothing)
	 * 
	 * @effect 	A unit's name is set to the given name
	 * 			| this.getName() == name;
	 * 
	 * @effect	A unit's initial position is set to the given coordinates
	 * 			| this.setPosition(position[0]+World.LENGTH_CUBE/2, position[1]+World.LENGTH_CUBE/2,position[2]+World.LENGTH_CUBE/2)
	 * 
	 * @effect  A unit's orientation is set to theta0 which is pi/2
	 * 			| setOrientation((float)(Math.PI/2))
	 * 
	 * @effect  A unit's initial properties are set
	 * 			| setInitialStrength(strength)
	 * 			| setInitialAgility(agility)
	 *			| setInitialToughness(toughness)
	 *			| setInitialWeight(weight)
	 *
	 * @effect	Stamina and hitpoints are initially set to the max values
	 * 			| setHitpoints(this.getMaxHitpoints())
	 * 			| setStaminapoints(this.getMaxStaminapoints())
	 * 
	 */
	@Raw
	public Unit(String name, int[] position, int weight, int strength, int agility, int toughness, boolean enableDefaultBehaviour) throws UnitException, WorldException, GameObjectException, IllegalPositionException{
		super(new Position(position[0],position[1], position[2] ));
		if (World.getInstance().unitSet.size() < World.MAXUNITS){
			try {
				World.getInstance().addUnitToSet(this);
				World.getInstance().addUnitToFaction(this);
			} catch (UnitException e) {
				throw new WorldException(e.getMessage());
			}
		} else return;
		
		try {
			this.setName(name);
		} catch (IllegalNameException e) {
			throw new UnitException("Illegal name given while constructing Unit: " + e.getMessage());
			
		}
		
		//this.setObjectPosition(position[0] + (World.LENGTH_CUBE/2), position[1] + (World.LENGTH_CUBE/2), position[2]+(World.LENGTH_CUBE/2));

	
		this.setOrientation((float)(Math.PI/2));
		
		this.setInitialStrength(strength);
		this.setInitialAgility(agility);
		this.setInitialToughness(toughness);
		this.setInitialWeight(weight);
		
		this.setHitpoints(this.getMaxHitpoints());
		this.setStaminapoints(this.getMaxStaminapoints());
		
		if (enableDefaultBehaviour == true)
			this.setMovementType(MovementType.default_behaviour);

		else this.setMovementType(MovementType.nothing);
	}

	/**
	 * Contains the x, y and z coordinate of the targetpostion. The targetposition is the position to which
	 * this unit wants to move.
	 */
	public Position targetPosition;
	
	/**
	 * The intermediary targetposition of a unit while moving. Contains of x, y and z coordinate to determine the cube.
	 */
	public Position targetCube;
	
	/**
	 * Attribute that make up the name of a Unit.
	 */
	private String unitName;  
	
	/**
	 * Attributes that make up the orientation of a unit.
	 * The orientation of the unit is given by an angle 'theta' in radians.
	 */
	private float theta;
	
	/**
	 * Contains the strength of a Unit.
	 */
	public int strength;
	/**
	 * Contains the agility of a Unit.
	 */
	public int agility;
	/**
	 * Contains the toughness of a Unit.
	 */
	public int toughness;
	
	/**
	 * Contains the current hitpoints of a Unit.
	 */
	public int hitpoints;
	/**
	 * Contains the current stamina points of a Unit.
	 */
	public int staminapoints;
	
	/**
	 * A variable that keeps track of the time a Unit has rested since it started resting.
	 */
	private double time_rested = 0;
	
	/**
	 * Keeps track of the time since the unit has rested for the last time.
	 */
	private double timeSinceRest = 0;
	
	/**
	 * Keeps track of the time since the unit has started to fight.
	 */
	private double fightTime;
	
	/**
	 * Keeps track of the time the Unit has been sprinting since it started sprinting.
	 */
	private double timeSprinting;
	
	/**
	 * Time needed to execute work
	 */
	public float duration_work;
	
	/**
	 * The time the Unit has been working since it started working.
	 */
	public float progress_work;
	
	/**
	 * Boolean that returns true if the unit is working.
	 */
	public boolean isWorking;
	
	/**
	 * Boolean that returns true if the unit is executing default behaviour.
	 */
	public boolean defaultBehaviourEnabled = false;
	
	/**
	 * The current movementtype of the unit which is one of walking, sprinting, falling, working, resting, 
	 * fighting, default_behaviour, nothing or dead.
	 */
	private MovementType currentMovementType;
	
	/**
	 * Remembers the movementType of the new request while a unit is beiing moved to the centre of a cube.
	 */
	public MovementType movementTypeBeforeNewRequest;
	
	/**
	 * Returns true if a unit has gained at least one hitpoint or staminapoint since it started resting.
	 */
	private boolean firstPointAdded;
	
	/**
	 * Contains the current amount of experience points;
	 */
	private int currentExperiencePoints;
	
	/**
	 * Contains the current faction of the unit.
	 */
	public Faction faction;
	
	/**
	 * Contains all the cubes that are to be picked by the Path Finding Algorithm.
	 */
	public ArrayList <QueueElement<Cube, Integer>> Queue;	
	
	
	/**
	 *Sets the targetPosition of the Unit to the given coordinates.
	 * @param x
	 * 		the X coordinate
	 * @param y
	 * 		the Y coordinate
	 * @param z
	 * 		the Z coordinate
	 * @throws UnitException
	 * 		if an illegal coordinate is given, throw new IllegalPositionException.
	 * @post 	the targetPosition of the unit is set.
	 * 			| new.getTargetPositionX() != null
	 * 			| new.getTargetPositionY() != null
	 * 			| new.getTargetPositionZ() != null
	 * @effect 	the coordinates of the targetPosition are set.
	 * 			|this.targetPosition.setPositionX(x)
	 * 			|this.targetPosition.setPositionY(y)
	 * 			|this.targetPosition.setPositionZ(z)
	 */
	@Raw
	public void setTargetPosition(double x, double y, double z) throws UnitException{
		if (targetPosition == null)
			try {
				targetPosition = new Position(x,y,z);
			} catch (IllegalPositionException e) {
				throw new UnitException("Illegal Target Position given :" + e.getMessage());
			}
		else{
			try{
			this.targetPosition.setPositionX(x);
			this.targetPosition.setPositionY(y);
			this.targetPosition.setPositionZ(z);
			}catch (IllegalPositionException e){
				throw new UnitException("Illegal Target Position given :" + e.getMessage());
			}
		}
	}

	/**
	 * Sets the targetCube of the Unit to the given coordinates.
	 * @param x
	 * 		the X coordinate
	 * @param y
	 * 		the Y coordinate
	 * @param z
	 * 		the Z coordinate
	 * @throws UnitException
	 * 		if an illegal position is given, throw new IllegalPositionException.
	 * @post the targetCube of the unit is set.
	 * 			|new.getTargetCube != null;
	 * @effect the targetCubeCoordinates are set.
	 * 			|this.targetCube.setPositionX(x)
	 * 			|this.targetCube.setPositionY(y)
	 * 			|this.targetCube.setPositionZ(z)
	 */
	@Raw
	public void setTargetCube(double x, double y, double z) throws UnitException{
		if (targetCube == null)
			try {
				targetCube = new Position(x,y,z);
			} catch (IllegalPositionException e) {
				throw new UnitException("Illegal Target Position given :" + e.getMessage());
			}
		else {
			try{
			this.targetCube.setPositionX(x);
			this.targetCube.setPositionY(y);
			this.targetCube.setPositionZ(z);
			}catch (IllegalPositionException e){
				throw new UnitException("Illegal Target Cube given: " + e.getMessage());
			}
		}
	}
	
	@Basic @Raw
	public Position getTargetCube(){
		return this.targetCube;
	}
	
	/**
	 * Returns the current x-coordinate of a unit's targetposition.
	 * @return
	 */
	@Basic @Raw
	public double getTargetPositionX(){
		return this.targetPosition.position_x;
		}
	
	/**
	 * Returns the current y-coordinate of a unit's targetposition.
	 * @return
	 */
	@Basic @Raw
	public double getTargetPositionY(){
		return this.targetPosition.position_y;
		}
	
	/**
	 * Returns the current z-coordinate of a unit's targetposition.
	 * @return
	 */
	@Basic @Raw
	public double getTargetPositionZ(){
		return this.targetPosition.position_z;
		}
	
	/**
	 * Returns the current faction of the unit.
	 * @return
	 */
	@Basic @Raw
	public Faction getFaction(){
		return this.faction;
	}
	
	/**
	 * Sets the faction that the unit belongs to.
	 * @param faction
	 * @Post the faction of the Unit is set to the given faction.
	 * 			|new.getFaction() == faction
	 */
	@Raw
	public void setFaction(Faction faction){
		this.faction = faction;
	}
	
	/**
	 * Returns the current strength of the Unit.
	 * @return
	 */
	@Basic @Raw
	public int getStrength(){
		return this.strength;
	}
	
	/**
	 * Returns the current agility-value of the Unit.
	 * @return
	 */
	@Basic @Raw
	public int getAgility(){
		return this.agility;
	}
	
	/**
	 * Returns the current toughness-value of the Unit.
	 * @return
	 */
	@Basic @Raw
	public int getThoughness(){
		return this.toughness;
	}
	
	/**
	 * A boolean that returns whether the given weight is a valid one.
	 * 
	 * @return	true if the weight is an integer between (this.getStrength()+this.getAgility())/2 and 200.
	 * 			|if(weight >= (this.getStrength()+this.getAgility())/2 && weight <= 200)
	 * 			|	return true;
	 * 			|else return false;
	 */
	@Override
	public boolean isValidWeight(int weight){
		return super.isValidWeight(weight) && (weight>= (this.getStrength()+this.getAgility())/2);
	}
	
	/**
	 * Returns true if the value of the integer is bigger or equal to zero and smaller or equal to 200.
	 * 
	 * @param 	value
	 * 			The value that is checked (strength, weight, agility, toughness)
	 * @return	True if value is between 0 and 200.
	 * 			| If (value > 0) && (value <= 200)
	 * 			| then result = true
	 * 			| else result = false
	 */
	public boolean isValidValue(int value){
		if ((value > 0) && (value <= 200))
				return true;
		else return false;
	}
	
	/**
	 * Returns true if the value of the integer is bigger or equal to 25 and smaller or equal to 100.
	 * 
	 * @param 	value
	 * 			An initial value for which we want to check whether it is valid.
	 * @return	True if the value is between 25 and 100.
	 * 			| if (value >= 25) && (value <= 100)
	 * 			| then result = true
	 * 			| else result = false; 
	 * 			
	 */
	public boolean isValidInitialValue(int value){
		if ((value >= 25) && (value <= 100))
			return true;
		else return false;
	}
	
	/**
	 * Sets the value of the attribute weight to the value given as parameter.
	 * 
	 * @param 	weight
	 * 			The weight we want to set for the unit.
	 * @post	Weight has to be at least (strength*agility)/2
	 * 			| new.isValidWeight(this.getWeight, this.getStrength(), this.getAgility())
	 */
	@Override
	public void setWeight(int weight){
		if (! this.isValidWeight(Math.abs(weight)) && weight < (this.getStrength()+this.getAgility())/2)
			this.weight = (this.getStrength() + this.getAgility())/2;
		else if (this.isValidWeight(Math.abs(weight)))
			this.weight = Math.abs(weight);
		else if (weight > 200)
			this.weight = 200;
		this.ValidateStaminaAndHitpoints();
	}
	
	/**
	 * Sets the value of the attribute weight to the value given as parameter, with the initial boundaries 
	 * of 25 and 100.
	 * 
	 * @param 	weight
	 * 			The weight we want to set for the unit as initial value.
	 * @post	Integer number between 25 and 100.
	 * 			| new.isValidInitialValue(this.getweight())
	 * @post	Weight has to be at least (strength*agility)/2.
	 * 			| new.isValidWeight(this.getWeight(), this.getStrenght(), this.getAgility())
	 * @post	If weight is less then 25 we will return 25 as value.
	 * 			| if(this.getweight() < 25)
	 * 			|	new.getweight() == 25
	 * @post	If weight is bigger than 100, the value will become 100.
	 * 			| if (this.getweight() > 100)
	 * 			|	new.getweight() == 100
	 */
	@Raw
	public void setInitialWeight(int weight){
		if (! this.isValidWeight(Math.abs(weight)))
			this.weight = (this.getStrength() + this.getAgility())/2;
		else if (this.isValidWeight(Math.abs(weight)) && (this.isValidInitialValue(Math.abs(weight))))
			this.weight = Math.abs(weight);
		else if (weight > 100)
			this.weight = 100;
	}
	
	/**
	 * Sets the value of the attribute strength to the value given as parameter.
	 * 
	 * @param 	strength
	 * 			The strength we want to set for the unit.
	 * @post	The value of strength must be between 1 and 200, 0 and 200 included.
	 * 			| new.isValidValue(strength)
	 * @post	If the value of strength is negative, we will take the absolute value as result.
	 * 			| if (this.strength < 0)
	 * 			|	new.getStrength() == Math.abs(strength)
	 * @post	If the value of strenght is larger then 200, we will take 200 as value.
	 * 			| if (strength > 200)
	 * 			| 	new.getStrength() == 200
	 * @post	If the value of strength is 0, the value of strength will be 1 (the minimum).
	 * 			| if (strength() == 0)
	 * 			|	new.getStrength == 1
	 */
	@Raw
	public void setStrength(int strength){
		if (this.isValidValue(Math.abs(strength)))
			this.strength = Math.abs(strength);
		else if (strength > 200)
			this.strength = 200;
		else if (strength == 0)
			this.strength = 1; 
		
		this.setWeight(this.getWeight());
	}
	
	/**
	 * Sets the value of the attribute strength to the value given as parameter, with the initial boundaries 
	 * of 25 and 100.
	 * 
	 * @param 	strength
	 * 			The strength value we want to set for the unit as initial value.
	 * @post	Integer number between 25 and 100.
	 * 			|	new.isValidInitialValue(this.getstrength())
	 * @post	If the value is negative we use the absolute value
	 * 			| if (strength <0)
	 * 			|   strength == Math.abs(strength)
	 * @post	If strength is less then 25 we will return 25 as value.
	 * 			| if(this.getstrength() < 25)
	 * 			|	new.getstrength() == 25
	 * @post	If strength is bigger than 100, the value will become 100.
	 * 			| if (this.getstrength() > 100)
	 * 			|	new.getstrength() == 100
	 */
	@Raw
	public void setInitialStrength(int strength){
		if (this.isValidInitialValue(Math.abs(strength)))
			this.strength = Math.abs(strength);
		else if (Math.abs(strength) > 100)
			this.strength = 100;
		else if (Math.abs(strength) < 25)
			this.strength = 25;
	}
	
	/**
	 * Sets the value of the attribute agility to the value given as parameter.
	 * 
	 * @param 	agility
	 * 			The agility-value we want to set for the unit.
	 * @post	The value of agility must be between 0 and 200, 0 and 200 included.
	 * 			| new.isValidValue(agility)
	 * @post	If the value of agility is negative, we will use the absolute value.
	 * 			| if (agility < 0)
	 * 			|	this.agility == Math.abs(agility)
	 * @post	If the value of agility is larger then 200, we will take 200 as value.
	 * 			| if (agility > 200)
	 * 			|	this.agility == 200
	 * @post	If the value of agility is zero, the value of agility will be 1 (the minimum).
	 * 			| if (agility = 0)
	 * 			|	this.agility == 1
	 */
	@Raw
	public void setAgility(int agility){
		if (this.isValidValue(Math.abs(agility)))
			this.agility = Math.abs(agility);
		else if (agility > 200)
			this.agility = 200;
		else if (agility == 0)
			this.agility = 1;
		this.setWeight(this.getWeight());
	}
	
	/**
	 * Sets the value of the attribute agility to the value given as parameter, with the initial boundaries 
	 * of 25 and 100.
	 * 
	 * @param 	agility
	 * 			The agility value we want to set for the unit as initial value.
	 * @post	Integer number between 25 and 100.
	 * 			|	new.isValidInitialValue(this.getagility())
	 * @post	If the value is negative we use the absolute value
	 * 			| if (agility <0)
	 * 			|   new.Agility() == Math.abs(agility)
	 * @post	If agility is less then 25 we will return 25 as value.
	 * 			| if(this.getagility() < 25)
	 * 			|	new.getAgility() == 25
	 * @post	If agility is bigger than 100, the value will become 100.
	 * 			| if (this.getagility() > 100)
	 * 			|	new.getAgility() == 100
	 */
	@Raw
	public void setInitialAgility(int agility){
		if (this.isValidInitialValue(Math.abs(agility)))
			this.agility = Math.abs(agility);
		else if (Math.abs(agility) > 100)
			this.agility = 100;
		else if (Math.abs(agility) < 25)
			this.agility = 25;
	}
	
	/**
	 * Sets the value of the attribute toughness to the value given as parameter.
	 * 
	 * @param 	toughness
	 * 			The toughness-value we want to set for the unit.
	 * @post	The value of toughness must be between 0 and 200, 0 and 200 included.
	 * 			| new.isValidValue(toughness)
	 * @post	If the value of toughness is negative, we will take the absolute value.
	 * 			| if (toughness < 0)
	 * 			|	new.getToughness == Math.abs(toughness)
	 * @post	If the value of toughness is larger then 200, we will us 200 as value.
	 * 			| if (toughness > 200)
	 * 			|	new.getToughness == 200
	 * @post	If the value of toughness is 0, the value of toughness will be 1 (the minimum).
	 * 			| if (toughness == 0)
	 * 			|	new.getToughness == 1
	 */
	@Raw
	public void setToughness(int toughness){
		if (this.isValidValue(Math.abs(toughness)))
			this.toughness = Math.abs(toughness);
		else if (toughness > 200)
			this.toughness = 200;
		else if (toughness == 0)
			this.toughness = 1;
		ValidateStaminaAndHitpoints();
		
	}
	
	/**
	 * Sets the value of the attribute toughness to the value given as parameter, with the initial boundaries 
	 * of 25 and 100.
	 * 
	 * @param 	toughness
	 * 			The toughness value we want to set for the unit as initial value.
	 * @post	Integer number between 25 and 100.
	 * 			|	isValidInitialValue(this.gettoughness())
	 * @post	If the value is negative we use the absolute value
	 * 			| if (toughness <0)
	 * 			|   this.getToughness() == Math.abs(toughness)
	 * @post	If toughness is less then 25 we will return 25 as value.
	 * 			| if(this.getToughness() < 25)
	 * 			|	new.getToughness() == 25
	 * @post	If toughness is bigger than 100, the value will become 100.
	 * 			| if (this.getToughness() > 100)
	 * 			|	new.getToughness() == 100
	 */
	@Raw
	public void setInitialToughness(int toughness){
		if (this.isValidInitialValue(Math.abs(toughness)))
			this.toughness = Math.abs(toughness);
		else if (Math.abs(toughness) > 100)
			this.toughness = 100;
		else if (Math.abs(toughness) < 25)
			this.toughness = 25;
	}
	
	/**
	 * Method to check whether the current stamina and hitpoints are still valid. If not, this method will make them valid.
	 * @effect if the current hitpoints are not smaller or equal to the maximum hitpoints anymore, the hitpoints are set to the maximum hitpoints.
	 * 			|if(!this.getHitpoints() <= new.getMaxHitpoints())
	 * 			|	new.setHitpoints(new.getMaxHitpoints())
	 * @effect if the current staminapoints are not smaller or equal to the maximum hitpoints anymore, the staminapoints are set to the maximum staminapoints.
	 * 			|if(!this.getStaminapoints()<= new.getMaxStaminapoints())
	 * 			|	new.setStaminaPoints(new;getMaxStaminapoints())
	 */
	private void ValidateStaminaAndHitpoints(){
		if (!(this.getHitpoints() <= this.getMaxHitpoints()))
			this.setHitpoints(this.getMaxHitpoints());
		if (!(this.getMaxStaminapoints() <= this.getMaxStaminapoints()))
			this.setStaminapoints(this.getMaxStaminapoints());
	}

	/**
	 * Returns the current hitpoints of the unit.
	 * @return
	 */
	@Basic @Raw
	public int getHitpoints(){
		return this.hitpoints;
	}
	
	/**
	 * returns the current staminapoints of the unit.
	 * @return
	 */
	@Basic @Raw
	public int getStaminapoints(){
		return this.staminapoints;
	}

	/**
	 * Returns the maximal hitpoints the unit can have at the moment.
	 * @return
	 */
	@Basic @Raw
	public int getMaxHitpoints(){
		return (int)Math.ceil(200.0*(this.getWeight()/100.0)*(this.getThoughness()/100.0));
	}
	
	/**
	 * Returns the maximum value that the variable staminapoints is allowed to have.
	 * @return
	 */
	@Basic @Raw
	public int getMaxStaminapoints(){
		return (int)Math.ceil(200.0*(this.getWeight()/100.0)*(this.getThoughness()/100.0));
	}

	/**
	 * Returns true if the hit points are bigger or equal to zero and smaller or equal to 
	 * their max points.
	 * 
	 * @param point
	 * @return	returns true if the hitpoints are bigger or equal to zero and smaller or equal to the max hitpoints
	 * 			| if (point >=0) && (point<= maxhitpoints)
	 * 			| 	then result == true
	 * 			| else result == false
	 */
	public boolean isValidHitpoint(int point){
		if ((point >= 0) && (point <= this.getMaxHitpoints()))
			return true;
		else return false;
	}
	

	/**
	 * Returns true if the stamina points are bigger or equal to zero and smaller or equal to 
	 * their max points.
	 * 
	 * @param point
	 * @return	returns true if the staminapoints are bigger or equal to zero and smaller or equal to the max staminapoints
	 * 			| if (point >=0) && (point<= maxstaminapoints)
	 * 			| 	then result == true
	 * 			| else result == false
	 */
	public boolean isValidStaminapoint(int point){
		if ((point >= 0) && (point <= this.getMaxStaminapoints()))
			return true;
		else return false;
	}
	
	 /**
	 * Sets the value of hitpoints to the value given as parameter.
	 * 
	 * @param 	hitpoints
	 * 			The value of hitpoints we want to set.
	 * @pre		Hitpoints must be larger or equal to 0 and less then or equal to max hitpoints.
	 * 			| isValidPoint(hitpoints)
	 * @post	the hitpoints are set to the value given as parameter.
	 * 			| new.getHitpoints() == hitpoints
	 */
	@Raw
	public void setHitpoints(int hitpoints){
		assert (this.isValidHitpoint(hitpoints));
			this.hitpoints = hitpoints;
	}

	/**
	 * Increases the value of hitpoints with the value given as parameter.
	 * 
	 * @param 	hitpoints
	 * 			The amount of hitpoints we want to add.
	 * @pre		Hitpoints must always be larger or equal to 0 and less or equal to max hitpoints.
	 * 			| isValidHitpoint(this.getHitpoints() + hitpoints)
	 * @post	The additional hitpoints are added to the current hitpoints but cannot surpass the maximal amount for hitpoints.
	 * 			| new.getHitpoints() == this.getHitpoints() + min(hitpoints, maxhitpoints-this.getHitpoints())
	 * @effect	The value of the hitpoints is set to the current value + the given value
	 * 			| this.setHitpoitns(this.getHitpoints() + Math.min(hitpoints, this.getMaxHitpoints() - this.getHitpoints()))
	 */
	public void increaseHitpoints (int hitpoints){
		int difference = this.getMaxHitpoints() - this.getHitpoints();
		assert (this.isValidHitpoint(this.getHitpoints() + this.minimum_points(hitpoints, difference)));
			this.setHitpoints(this.getHitpoints() + Math.min(hitpoints, this.getMaxHitpoints() - this.getHitpoints()));
	}
	
	/**
	 * decreases the value of hitpoints with the value given as parameter.
	 * 
	 * @param 	hitpoints
	 * 			The amount we are going to distract from the current hitpoints.
	 * @pre		Hitpoints must always be larger or equal to 0
	 * 			| isValidHitpoint(this.hitpoints + hitpoints)
	 * @post	The additional hitpoints are distracted from the current hitpoints but cannot become less then minimal
	 * 			amount of hitpoints, which is 0.
	 * 			| new.getHitpoints() == this.getHitpoints() - min(hitpoints, this.hitpoints)
	 * @effect	The value of the hitpoints is set to the current value - the given value
	 * 			| this.setHitpoints(this.getHitpoints() - Math.min(hitpoints, this.getHitpoints()))
	 */
	public void decreaseHitpoints (int hitpoints){
		assert (this.isValidHitpoint(this.getHitpoints() - this.minimum_points(hitpoints, this.getHitpoints())));
			this.setHitpoints(this.getHitpoints() - Math.min(hitpoints, this.getHitpoints()));
	}
		
	/**
	 * Sets the value of staminapoints to the value given as parameter.
	 * 
	 * @param 	staminapoints
	 * 			The value of staminapoints we want to set.
	 * @pre		Staminapoints must be larger or equal to 0 and less then or equal to max staminapoints.
	 * 			| isValidPoint(staminapoints)
	 * @post	the staminapoints are set to the value given as parameter.
	 * 			| new.getStaminapoints() = staminapoints
	 */
	@Raw
	public void setStaminapoints(int staminapoints){
		assert (this.isValidStaminapoint(staminapoints));
			this.staminapoints = staminapoints;
	}
	
	/**
	 * Increases the value of staminapoints with the value given as parameter.
	 * 
	 * @param 	staminapoints
	 * 			The amount of staminapoints we want to add.
	 * @pre		Staminapoints must always be larger or equal to 0 and less or equal to max staminapoints.
	 * 			| isValidHitpoint(this.getStaminapoints() + staminapoints)
	 * @post	The additional staminapoints are added to the current staminapoints but cannot surpass the maximal amount 
	 * 			for staminapoints.
	 * 			| new.getHitpoints() == this.getHitpoints() + min(hitpoints, maxhitpoints-this.getHitpoints())
	 * @effect	The staminapoints are set to the current value + the given value
	 * 			| this.setStaminapoints(this.getStaminapoints() + Math.min(staminapoints, this.getMaxStaminapoints()-this.getStaminapoints()))
	 */
	public void increaseStaminapoints(int staminapoints){
		int difference = this.getMaxStaminapoints() - this.getStaminapoints();
		assert (this.isValidStaminapoint(this.getStaminapoints() + this.minimum_points(staminapoints, difference)));
		this.setStaminapoints(this.getStaminapoints() + Math.min(staminapoints, this.getMaxStaminapoints()-this.getStaminapoints()));	
	}	
	
	/**
	 * Decreases the value of staminapoints with the value given as parameter.
	 * 
	 * @param 	staminapoints
	 * 			The amount we are going to distract from the current staminapoints.
	 * @pre		Staminapoints must always be larger or equal to 0
	 * 			| isValidStaminapoints(this.staminapoints + staminapoints)
	 * @post	The additional staminapoins are distracted from the current staminapoints but cannot become less then minimal
	 * 			amount of staminapoints, which is 0.
	 * 			| new.getStaminapoints() == this.getStaminapoints() - min(staminapoints, this.getStaminapoints())
	 * @effect	The value of the staminapoints is set to the current value - the given value.
	 * 			| this.setStaminapoints(this.getStaminapoints() - Math.min(staminapoints, this.getStaminapoints()))
	 */
	public void decreaseStaminapoints (int staminapoints){
		assert (this.isValidStaminapoint(this.getStaminapoints() - this.minimum_points(staminapoints, this.getStaminapoints())));
			this.setStaminapoints(this.getStaminapoints() - Math.min(staminapoints, this.getStaminapoints()));
	}
	
	/**
	 * Returns the minimum of the two points given as parameter.
	 * @param points
	 * @param difference
	 * @return
	 */
	public int minimum_points(int points, int difference){
		return Math.min(points, difference);
	}
	
	/**
	 * Adds the amount of points given as parameters to the current amount of experiencepoints.
	 * @param value
	 * @post	if the value is negative, than we are going to add the absolute value of the parameter.
	 * 			| new.getExperiencePoints() == this.getExperiencePoints() + Math.abs(value);
	 * @effect 	if the experience points are bigger than 10, Strength, Toughness or Agility are increased by 1
	 * 			|this.SetAgility(this.getAgility()+1) || this.setStrength(this.getStrength()+1) || this.setToughenss(this.getToughness()+1)
	 */
	@Raw
	private void setExperiencePoints(int value){
		int[] IntArray = {1,2,3};
		int choice;
		this.currentExperiencePoints += Math.abs(value);
		
		if ((this.getStrength() == 200) && (this.getAgility()==200) &&(this.getThoughness()==200))
			return;
		
		while (this.currentExperiencePoints >= 10){
			choice = this.getRandom(IntArray);
			
			if (choice == 1)
				if (this.getStrength()<200)
					this.setStrength(this.getStrength()+1);
				else this.setExperiencePoints(value);
			else if (choice == 2)
				if (this.getAgility()<200)
					this.setAgility(this.getAgility()+1);
				else this.setExperiencePoints(value);
			else if (choice == 3)
				if (this.getThoughness()<200)
					this.setToughness(this.getThoughness()+1);
				else this.setExperiencePoints(value);
			this.currentExperiencePoints = this.currentExperiencePoints - 10;
		}
	}
	
	/**
	 * Returns the current amount of experience points of the unit.
	 * @return
	 */
	@Basic @Raw
	public int getExperiencePoints(){
		return this.currentExperiencePoints;
	}
	
	/**
	 * Returns the name a unit has at the moment.
	 * @return
	 */
	@Basic @Raw
	public String getName(){
		return this.unitName;
	}
	
	/**
	 * Sets the name of the unit or changes it.
	 * 
	 * @param	Unitname
	 * 			This is the name we want to set for the unit.
	 * @post	Unitname has at least 2 characters of which the first one is a capital letter. Only a
	 * 			limited set of symbols is allowed.
	 * @throws	IllegalNameException
	 * 			| ! isValidName(unitName)
	 */
	@Raw
	public void setName (String Unitname) throws UnitException{
		if (! this.isValidName(Unitname)) 
			throw new IllegalNameException(Unitname);
		this.unitName = Unitname;
	}
	
	/**
	 * Returns true if the value of the String is a valid userName. A regular expression
	 * is used to give the set of characters that is allowed.
	 * 
	 * @param 	unitName
	 * 			The name we want to set for the unit.
	 * @return	True if the username starts with a capital letter, has at least
	 * 			2 characters and contains only small and capital letters, numbers, space, and
	 * 			quote signs.
	 * 			| if (unitName.matches("^([A-Z]{1})([A-Za-z0-9\\'\\\" ]{1,})$")
	 * 			|	then result == true
	 */
	public boolean isValidName(String unitName){
		// Allowed symbols for unitName in regEx notation.
		String pattern = "^([A-Z]{1})([A-Za-z0-9\\'\\\" ]{1,})$";
		
		boolean valid = false;
		if (unitName.matches(pattern)){
			valid = true;
		}
		return valid;		                                  
	}											

	/**
	 * A method that returns the angle 'theta' the unit is facing to
	 * @return	the angle that the unit is facing to.
	 */
	@Basic @Raw
	public float getOrientation(){
		return this.theta;		
	}
	
	/**
	 * set the angle of the unit to a new angle.
	 * 
	 * @param 	theta
	 * 			The orientation of the unit.
	 * @post	If the given theta is between 0 and 2*pi, then the new
	 * 			orientation of the unit is equal to theta.
	 * 			| if ( (theta >= 0) && (theta < 2*Math.PI))
	 * 			| then new.getOrientation() == theta
	 * @post	If the given theta is larger then 2*pi then the new
	 * 			orientation is equal to the given theta - 2*pi* the number of times 2*pi fits in theta
	 * 			| if ( (theta >= 2*Math.pi))
	 * 			| then new.getOrientation() == theta - 2*Math.PI* (theta/(2*Math.PI))
	 * 
	 * @post	If the given theta is less then 0 then the new orientation
	 * 			is equal to to the given theta + 2*pi* the absolute value of the number of times 2*pi fits in theta	het aantal keer 2*pi in absolute waarden
	 * 			| if ( (theta < 0))
	 * 			| then new.getOrientation() == theta + 2*Math.PI* abs((theta/(2*Math.PI))
	 * 
	 */
	@Raw
	public void setOrientation(float theta){
		if (theta >= 2*Math.PI)
			theta = (float)(theta - (2*Math.PI) * ((int)(theta/(2*Math.PI))));
		else if (theta <0)
			theta = (float)((2*Math.PI) + theta + (2*Math.PI) * (int)Math.abs(theta/(2*Math.PI)));
		this.theta = theta;
	}

	/**
	 * Method to make sure 2 unit's that are fighting each other are also 
	 * facing each other. This method updates their orientation. 
	 * @param	D
	 * 			The defending unit.
	 * @effect the orientation of the unit and of the defender is updated
	 * 			|this.setOrientation(Math.atan2(D.getPositionY()-this.getPositiony(), D.getPositionX()-this.getPositionX())
	 * 			|D.setOrientation(Math.atan2(this.getPositionX()-D.getPositionY(), this.getPositionX()-D.getPositionX())
	 */
	private void orientationDuringFight(Unit D){
		this.setOrientation((float) Math.atan2( (D.getPositionGameObjectY()- this.getPositionGameObjectY()) , (D.getPositionGameObjectX() - this.getPositionGameObjectX()) ) );
		D.setOrientation((float) Math.atan2( (this.getPositionGameObjectY()- D.getPositionGameObjectY()) , (this.getPositionGameObjectX() - D.getPositionGameObjectX()) ) );	
	}
	
	/**
	 * A method to set the orientation of the Unit so that it is facing the cube he wants to work in.
	 * @param cubeToWorkAt
	 * 		the cube the unit wants to work in.
	 * @effect the orientation of the unit is updated.
	 * 			|this.setOrientation(Math.atan2(cubeToWorkAt.getPositionY()+World.LENGTH_CUBE/2-this.getPositionY(), cubeToWorkAt.getPositionX()+World.LENGTH_CUBE/2-this.getPositionX())
	 */
	private void orientationDuringWorking(Cube cubeToWorkAt){
		this.setOrientation((float) Math.atan2((cubeToWorkAt.getPositionY()+ World.LENGTH_CUBE/2-this.getPositionGameObjectY()),
											   (cubeToWorkAt.getPositionX()+ World.LENGTH_CUBE/2-this.getPositionGameObjectX())));
	}
	
	/**
	 * Method to set the orientation during walking.
	 * @param 	v_x
	 * 			the X component of the speed of a unit.
	 * @param 	v_y
	 * 			the Y component of the speed of a unit.
	 * @effect 	the orientation of the unit is updated
	 * 			|this.setOrientation(Math.atan2(v_y,v_x)
	 */
	@Raw
	private void setWalkingOrientation(double v_x, double v_y){
		this.setOrientation((float)Math.atan2(v_y, v_x));	
	}

	/**
	 * Calculates the value of the base speed.
	 * base_speed = 1.5*(strength+agility)/(200*(weight/100))
	 * @param base_speed
	 */
	@Basic @Raw
	public double getBaseSpeed(){
		return ((1.5*(this.getStrength()+this.getAgility()))/(200*(double)this.getTotalWeight()/100));
	}

	/**
	 * Calculates the value of the walking speed of a Unit.
	 * @return the baseSpeed divided by 2 if the current Z - the target Z is -1,
	 * 		   the baseSpeed times 1.2 if the current Z - target Z is 1,
	 * 		   and the baseSpeed else.
	 * 			|if(Util.fuzzyEquals(this.getPositionGameObjectZ() - this.getTargetPositionZ(), -1)
	 * 			|	return this.getBaseSpeed()/2
	 * 			|else if(Util.fuzzyEquals(this.getPositionGameObjectZ()-this.getTargetPosition(), 1)
	 * 			|	return this.getBaseSpeed()*1.2
	 * 			|else return this.getBaseSpeed()
	 */
	@Raw @Basic
	public double getWalkingSpeed(){
		if (Util.fuzzyEquals(this.getPositionGameObjectZ() - this.getTargetPositionZ(), -1.0))
			return this.getBaseSpeed()/2;
		else if (Util.fuzzyEquals(this.getPositionGameObjectZ() - this.getTargetPositionZ(), 1.0))
			return this.getBaseSpeed()*1.2;
		else return this.getBaseSpeed();
	}
	
	/**
	 * Calculates the value of the sprinting speed of a Unit.
	 */
	@Basic @Raw
	public double getSprintingSpeed(){
		return this.getWalkingSpeed()*2;
	}
	
	/**
	 * Returns whether the unit can attack the defending unit D.
	 * @param D
	 * 		the defending unit.
	 * @return
	 */
	public boolean isValidAttack(Unit D){
		if (this.isNeighbouringCube(D.getGameObjectPositionXGameworld()-this.getGameObjectPositionXGameworld(), D.getGameObjectPositionYGameworld()-this.getGameObjectPositionYGameworld(),
				D.getGameObjectPositionZGameworld()-this.getGameObjectPositionZGameworld())
		   )
			return (true);
		else
			return (false);
	}
	
	/**
	 * A method to implement the behaviour when unit A attacks unit D.
	 * @param 	D
	 * 			The defending unit.
	 * @throws TaskException 
	 */
	public void attack(Unit D) {
		if ((this.isValidAttack(D)) && (this.getFaction() != D.getFaction())){
				this.setMovementType(MovementType.fighting);
				this.orientationDuringFight(D);
				try {
					D.defend(this);
				} catch (UnitException e) {
					e.printStackTrace();
				}
				this.fightTime = 0;
		}
	}

	/**
	 * A method that adds the gametime to the time a unit has been fighting.
	 * @param gametime
	 * 		the game time that needs to be added to the time a unit has been fighting.
	 * @post	the gametime is added to the fightTime
	 * 			|new.fightTime == this.fightTime + gametime
	 * @post	if the fightTime becomes bigger or equal to 1, the fightTime becomes zero again.
	 * 			|new.fightTime == 0
	 * @effect	if the currentStatement was not null and is an instance of AttackStatement, the currentStatetement is finished.
	 * 			|if(this.currentStatement != null && this.currentStatement instanceof AttackStatement)
	 * 			|	this.currentStatement.finishStatement()
	 * @effect	if the fightTime becomes bigger or equal to 1, the movementType of the unit is set to nothing.
	 * 			|this.setMovementType(MovementType.nothing)
	 * @throws TaskException 
	 * @throws ExpressionException 
	 * @throws StatementException 
	 * @throws StatementBreakException 
	 * @throws IllegalStateException 
	 */
	public void attack(double gametime) throws StatementException, ExpressionException{
		this.fightTime += gametime;
		
		if (this.fightTime >= 1){
			if (this.currentStatement != null){
				if(this.currentStatement instanceof AttackStatement){
					this.currentStatement.finishStatement();
				}
			}
			this.setMovementType(MovementType.nothing);
			this.fightTime = 0;
		}
	}
	
	/**
	 * A method to implement the behaviour when unit D is being attacked by unit A.
	 * A unit will first try to dodge an attack, if it fails to do so it will try to block the attack.
	 * If it fails also in blocking the attack, the unit will take some damage.
	 * @param A
	 * 		the unit that is doing the attack.
	 * @effect	if the unit was executing a task before he was being attacked, that task is interrupted.
	 * 			|if(this.isExecutingTask()
	 * 			|	this.getAssignedTask().interruptTask()
	 * @effect	the movementType is set to fighting
	 * 			|this.setMovementType(MovementType.fighting)
	 * @effect	if the dodge was succesfull, the unit will dodge the attack.
	 * 			|if(this.isSuccessfullDodge(A)
	 * 			| 	this.dodge()
	 * @effect	if the dodge was successfull, the unit will gain 20 Xp
	 * 			|if(this.isSuccessfullDodge(A)
	 * 			|	this.setExperiencePoitns(20)
	 * @effect	if the block was successfull, the unit will block
	 * 			|if(this.isSuccessFullBlock(A)
	 * 			|	this.block(A)
	 * @effect	if the block was successfull, the unit wil gain 20 Xp
	 * 			|if(this.isSuccessFullBlock(A)
	 * 			|	this.setExperiencePoints(20)
	 * @effect	if the dodge and the block failed, the unit will take damage.
	 * 			|if(!this.isSuccessfullDodge(A) && !this.isSuccessfullBlock(A)
	 * 			|	this.takingDamage(A)
	 * @effect	if the dodge and the block failed, the attacking unit will gain 20 Xp
	 * 			|if(!this.isSuccessfullDodge(A) && !this.isSuccessfullBlock(A)
	 * 			|	A.setExperiencePoitns(20)
	 * @throws  UnitException
	 * 		if a unit dodges to an illegal position, throw new IllegalPositionException.
	 * @throws TaskException 
	 * 
	 * 
	 */
	private void defend(Unit A) throws UnitException{
		if(this.isExecutingTask())
			try {
				this.getAssignedTask().interruptTask();
			} catch (TaskException e) {
				throw new UnitException(e.getMessage());
			}
		this.setMovementType(MovementType.fighting);
		if (this.isSuccesfullDodge(A)){
			try {
				this.dodge();
			} catch (IllegalPositionException e) {
				throw new UnitException(e.getMessage());
			}
			this.setExperiencePoints(20);
		}
		else if (this.isSuccesfullBlock(A)){
			this.block(A);
			this.setExperiencePoints(20);
		}
		else {
			this.takingDamage(A);
			A.setExperiencePoints(20);
		}
		
	}

	/**
	 * Method to dodge an attack. The defending unit will jump away from the attacker.
	 * 
	 * @param 	D
	 * 			The defending unit.
	 *
	 * @post	The unit's currentPosition has changed. (location before dodging is not equal to location after dodging).
	 * 			|	((Xd != 0) || (Yd != 0))
	 * @effect	The unit walks to the new position
	 * 			| this.moveTo(Xd, Yd, Zd)
	 * @throws IllegalPositionException
	 * 			if the random position generated by the dodge is not a valid position, throw a new IllegalPositionException
	 * 			| if (! this.isValidPosition(Xd)
	 * 			|	throw new IllegalPositionException(Xd)
	 * 			| if (! this.isValidPosition(Yd)
	 * 			|	throw new IllegalPositionException(Yd)
	 * 
	 */
	private void dodge() throws UnitException, IllegalPositionException{
		
	double Xd ;
	double Yd ;
	double Zd;
	int[] myIntArray = {-1,0,1};
	boolean validDodge = false;
		
		Xd = 0;
		Yd = 0;
			
		while (validDodge == false) {
			Xd = this.getPositionGameObjectX() + this.getRandom(myIntArray) ;
			Yd = this.getPositionGameObjectY() + this.getRandom(myIntArray) ;
				
			
			if (! World.isValidPosition(Xd, World.MAXPOSITION_X)){
				throw new IllegalPositionException(Xd);
			}
			else if (! World.isValidPosition(Yd, World.MAXPOSITION_Y))
				throw new IllegalPositionException(Yd);
			else if ((World.isValidPosition(Xd,  World.MAXPOSITION_X)) && (World.isValidPosition(Yd, World.MAXPOSITION_Y)) || (Yd != 0)){
				validDodge = true;
			}	
			if (!(World.getInstance().getCubeByPosition(	(int) Math.floor(Xd),
															(int) Math.floor(Yd),
															(int)this.getGameObjectPositionZGameworld()).isPassableCube()))
			validDodge = false;
			
		}
		
		Zd = this.getPositionGameObjectZ();
		this.moveTo(Xd , Yd , Zd );
	}
	
	/**
	 * Method to check a dodge is succesfull or not. A dodge is succesfull with a probability of 0.2*(AgilityDef./AgilityAtt.)
	 * 
	 * @param 	D
	 * 			The defending unit.
	 * @param 	A
	 * 			The attacking unit.
	 * @return true if the dodge was successful.
	 */
	private boolean isSuccesfullDodge(Unit A){
		double P = 0.2*(this.getAgility()/A.getAgility());
		boolean successful = false;
		
		if (P >= 1){
			successful = true;
		}
		else 	// so P less then 1
			if (Math.random() <= P){
				successful = true;
			}
			else if (Math.random() > P){
				successful = false;
			}
		return successful;
	}
	
	/**
	 * A method to block an attack.
	 * @param A
	 * 		the attacking unit.
	 */
	private void block (Unit A){
		
	}

	/**
	 * Method to check whether a block is succesfull or not. A dodge is succesfull with a probability of 0.25*( (getStrengthDef.+ getAgilityDef.) / (getStrengthAtt.+getAgilityAtt.) )
	 * 
	 * @param 	D
	 * 			The defending unit.
	 * @param 	A
	 * 			The attacking unit.
	 * @return true if the block was successful.
	 */
	private boolean isSuccesfullBlock(Unit A){
		double P = 0.25*( (this.getStrength() + this.getAgility()) / (A.getStrength() + A.getAgility()) );
		boolean succesfull = false;
		
		if (P >= 1){
			succesfull = true;
		}
		else
			if (Math.random() <= P){
				succesfull = true;
			}
			else if (Math.random() > P){
				succesfull = false;
			}
		return succesfull;
	}
	
	/**
	 * Method to reduce a unit's hitpoints.
	 * 
	 * @param 	A
	 * 			The attacking unit
	 * @effect 	Hitpoints are decreased
	 * 			|this.decreaseHitpoints(round(A.getStrength()/10)
	 */
	private void takingDamage(Unit A){
		this.decreaseHitpoints(round(A.getStrength()/10));
	}

	/**
	 * A method that sets the MovementType of a Unit to resting and adapts the TimeSinceRest and sets the Time_rested to zero.
	 * @post	if the hitpoints or the staminapoitns are not at their maximum, the TimeSinceRest and the Time_rested are set to zero.
	 * 			|if(this.getHitpoints < this.getMaxHitpoints && this.getStaminapoints < this.getMaxStaminapoints())
	 * 			|	this.timeSinceRest == 0; this.time_rested == 0;
	 * @effect	if the hitpoints or the staminapoints are not at their maximum, the movementType is set to resting.
	 * 			|if(this.getHitpoints < this.getMaxHitpoints && this.getStaminapoints < this.getMaxStaminapoitns())
	 * 			|	this.setMovementType(MovementType.resting)
	 * @effect	if the unit was moving when this method was called, the unit must walk to the middle of it's current cube.
	 * 			|if (this.isMoving() == true)
	 * 			|	this.centreUnit(MovementType.resting);
	 * @effect	if the hitpoints and the staminapoints are at their maximum and defaultBehaviourEnabled is true, the movementType is set to default_behaviour
	 * 			|if(this.getHitpoints() == this.getMaxHitpoints()) && (this.getStaminapoints() == this.getMaxStaminapoints() && this.defaultBehaviourEnabled == true)
	 * 			|	this.setMovementType(MovementType.default_behaviour)
	 * @effect	if the hitponts and the staminapoints are at their maximum and defaultBehaviourEnabled is not true, the movementType is set to nothing.
	 * 			|if(this.getHitpoints() == this.getMaxHitpoints() && this.getStaminapoints() == this.getMaxStaminapoints() && this.defaultBehaviourEnabled == false)
	 * 			|	this.setMovementType(MovementType.nothing 
	 * @throws UnitException 
	 */
	public void rest() throws UnitException{
		if (this.isMoving()== true){
			this.mustStop = true;
			this.centreUnit(MovementType.resting);
		} else {
				if ((this.getHitpoints() == this.getMaxHitpoints()) && (this.getStaminapoints() == this.getMaxStaminapoints())){
					if (this.defaultBehaviourEnabled == true)
						this.setMovementType(MovementType.default_behaviour);
					else
						this.setMovementType(MovementType.nothing);
				}
				else{
					this.setMovementType(MovementType.resting);
					this.timeSinceRest = 0;
					this.time_rested = 0;
					}
			}
		
		}

	/**
	 * A method that adds GameTime to the resttime of a resting Unit and adds hit- or staminapoints if this is necessary.
	 * @param 	gameTime	
	 * 		the gameTime that needs to be added.
	 * @post	the time_rested is increased by the gameTime given as parameter.
	 * 			|new.time_rested == this.time_rested + gameTime
	 * @post	the timeSinceRest is set to zero
	 * 			|new.timeSinceRest = 0
	 * @effect	if the unit is fully recovered and defaultBehaviourEnabled is true, the movementType is set to default_behaviour
	 * 			|if(this.getHitpoints() == this.getMaxHitpoints() && this.getStaminapoints() == this.getMaxHitpoints())
	 * 			|	this.setMovementType(MovementType.default_behaviour)
	 * @effect	if the unit is fully recovered and defaultBehaviourEnabled is false, the movementType is set to nothing.
	 * 			|if(this.getHitpoints() == this.getMaxHitpoints() && this.getStaminapoints() == this.getMaxStaminapoints())
	 * 			|	this.setMovementType(MovementType.nothing)
	 * @effect 	if the hitpoints are smaller then the max hitpoints, and the unit has rested for an amount of time, hitpoints are increased by 1
	 * 			|if(this.getHitpoints() < this.getMaxHitpoints() && factor * this.getThoughness()/200) >= 1)
	 * 			|	this.increaseHitpoints(1)
	 * @effect	if the staminapoints are smaller then the max staminapoints, and the unit has rested for an amount of time,staminapoints are increased by 1
	 * 			|if(this.getStaminapoints() < this.getMaxStaminapoints() && factor * this.getThoughness()/100) >= 1)
	 * 			|	this.increaseStaminapoints(1)
	 */
	public void rest(double gameTime) {
		int factor;
		
		this.time_rested += gameTime;
		
		factor = (int) Math.floor(this.time_rested/0.2);
		
		if (this.getHitpoints() < this.getMaxHitpoints()){
			if ((factor * this.getThoughness()/200) >= 1){
				this.firstPointAdded = true;
				this.increaseHitpoints(1);
				this.time_rested -= 0.2*factor;
				}
		}
		else if ((this.getStaminapoints() < this.getMaxStaminapoints()) && (this.getHitpoints() == this.getMaxHitpoints())){
			if ((factor * this.getThoughness()/100) >= 1){
				this.firstPointAdded = true;
				this.increaseStaminapoints(1);
				this.time_rested -= 0.2*factor;
			}
		}
		
		this.timeSinceRest = 0;
		
		if ((this.getHitpoints() == this.getMaxHitpoints()) && (this.getStaminapoints() == this.getMaxStaminapoints())){
			if (this.defaultBehaviourEnabled == true)
				this.setMovementType(MovementType.default_behaviour);
			else
				this.setMovementType(MovementType.nothing);
		}		
	}
	
	/**
	 * Returns the value of the boolean firstPointAdded.
	 * @return	true  if Unit has gained at least 1 hit- or staminapoint since it started resting.
	 * 			false all other cases.
	 */
	public boolean firstPointAdded(){
		return this.firstPointAdded();
	}
	
	/**
	 * Sets the progress of the work to a given value.
	 * @param value
	 */
	@Raw
	private void setProgress_work(float value){
		this.progress_work = value;
	}
	/**
	 * Calculates the duration needed for a work activity
	 * @return
	 * @throws UnitException 
	 */
	@Basic @Raw
	public float getDurationWork() throws UnitException{
		if (this.getStrength() != 0){
			this.duration_work = (500/this.getStrength());
			return this.duration_work;
		}
		else throw new UnitException("Strength is zero");
	}
	
	/**
	 * Returns the progress of the work needed for an activity.
	 * @return
	 */
	@Basic @Raw
	public float getProgressWork(){
		return this.progress_work;
	}
	
	/**
	 * Selected (neighbouring) cube to work at.
	 */
	public Cube NeighbouringCubeToWorkAt = null;
	

	/**
	 * Method to set the location to work at. This must be the unit's own cube position or a neighbouringCube of this
	 * cube. Sets the movementType to working and the progress of working to zero.
	 * 
	 * @param 	x
	 * 			X coordinate of cube to work at.
	 * @param 	y
	 * 			Y coordinate of cube to work at.
	 * @param 	z
	 * 			Z coordinate of cube to work at.

	 * @throws UnitException
	 * 			if the location given as parameters is not a neighbouring location, throw new UnitException("invalid worklocation").
	 * @effect the orientation is updated
	 * 			|this.orientationDuringWorking(World.getCubeByPosition(x,y,z)
	 * @effect the movementType is updated
	 * 			|this.setMovementType(MovementType.working)
	 * @effect the variable progress_work is set to zero.
	 * 			this.setProgress_work(0)
	 */
	public void workAt(int x, int y, int z) throws UnitException{
		if (World.getInstance().getNeighbouringCubes(getCurrentCube()).contains(World.getInstance().getCubeByPosition(x, y, z))){ 
			this.NeighbouringCubeToWorkAt = World.getInstance().getCubeByPosition(x, y, z);
			this.orientationDuringWorking(NeighbouringCubeToWorkAt);
			this.setMovementType(MovementType.working);
			this.setProgress_work(0);
			}
		else if ((x==this.getGameObjectPositionXGameworld())&&(y==this.getGameObjectPositionYGameworld())&&(z==this.getGameObjectPositionZGameworld())){
			this.NeighbouringCubeToWorkAt = World.getInstance().getCubeByPosition(currentPosition);
			this.setMovementType(MovementType.working);
			this.setProgress_work(0);
		}
		else
			throw new UnitException("Invalid worklocation.");			
	}
	
	/**
	 * Method to make progress while working. If a working activity is finished this method will ask what the effect
	 * of this action will be and reset the progress of work. It also checks whether the unit must perform default
	 * behaviour after finishing working or must do nothing.
	 * 
	 * @param 	gameTime
	 * 			The gametime that the unit will work.
	 * @effect the gameTime is added to the work progress.
	 * 			|new.setProgress_work((float)this.getPorgressWork() + gameTime)
	 * @effect if the progress is bigger or equal to the duration, the conditions after work are checked.
	 * 			|if(this.getProgressWork()>=this.getDurationWork())
	 * 			|	this.checkConditionsAfterWork()
	 * @effect if the progress is bigger or equal to the duration, the work is executed.
	 * 			|if(this.getProgressWork()>=this.getDurationWork())
	 * 			|	this.executeWork()
	 * @effect if the progress is bigger or equal to the duration, tthe progress is set to zero.
	 * 			|if(this.getProgressWork()>=this.getDurationWork())
	 * 			|	this.setProgress_work(0)
	 * @effect if the unit was currently executing an activity of a task, this activity is finished.
	 * 			|if(currentStatement != null && currentStatement instanceof WorkStatement)
	 * 			|	this.currentStatement.finishStatement
	 * @effect if the unit was not executing a task and default behaviour was enabled, the movementType is set to default_behaviour, else to nothing.
	 * 			|if(this.defaultBehaviourEnabled == true && !this.isExecutingTask())
	 * 			|	this.setMovementType(MovementType.default_behaviour)
	 * 			|else this.setMovementType(MovementType.nothing)
	 * @throws WorldException
	 * @throws GameObjectException
	 * @throws UnitException 
	 * @throws SecurityException 
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException 
	 * @throws IllegalPositionException 
	 * @throws TaskException 
	 * @throws ExpressionException 
	 * @throws StatementException 
	 * @throws StatementBreakException 
	 * @throws IllegalStateException 
	 */
	public void work(double gameTime) throws WorldException, GameObjectException, UnitException, IllegalPositionException, TaskException, StatementException, ExpressionException{
		
		this.setProgress_work((float)(this.getProgressWork() + gameTime));
		
		if (this.getProgressWork() >= this.getDurationWork()){
			this.checkConditionsAfterWork();
			this.executeWork();
			this.setProgress_work(0);
		
			if (this.currentStatement != null){
				if(this.currentStatement instanceof WorkStatement){
					this.currentStatement.finishStatement();
				}
			}
			else if (this.defaultBehaviourEnabled == true && !this.isExecutingTask())
				this.setMovementType(MovementType.default_behaviour);
			else
				this.setMovementType(MovementType.nothing);
		}		
	}
	
	/**
	 * Boolean to make sure that only the first activity for which all conditions are met shall be executed.
	 * becomes true if the conditions for an activity are met.
	 */
	private int condition = 0;
	
	/**
	 * Checks all possible conditions that are possible after work.
	 * @effect if the condition is zero, the conditions will be checked.
	 * 			|if (condition == 0)
	 * 			|	checkCondition_x; (with x being the number of the condition that needs to be checked.
	 * @throws WorldException
	 * @throws GameObjectException
	 * @throws UnitException 
	 * @throws SecurityException 
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException 
	 */
	private void checkConditionsAfterWork() throws WorldException, GameObjectException, UnitException{
		if (condition == 0)
			checkCondition_1CarryingBoulder();
		if (condition == 0)
			checkCondition_2CarryingLog();
		if (condition == 0)
			checkCondition_3ExecuteWorkshop();
		if (condition == 0)
			checkCondition_4PickUpBoulder();
		if (condition == 0)
			checkCondition_5PickUpLog();
		if (condition == 0)
			checkCondition_6ChopTree();
		if (condition == 0)
			checkCondition_7SmashRock();
		}
		
	/**
	 * This method executes the work needed for the condition that is true.
	 * @effect execute the condition that is true after the work is done.
	 * 			|switch(condition)
	 * 			| case x: this.executeConditionx()
	 * @post the condition is set to zero again
	 * 			|condition == 0
	 * @throws GameObjectException
	 * @throws WorldException
	 * @throws UnitException
	 * @throws IllegalPositionException 
	 */
	private void executeWork() throws GameObjectException, WorldException, UnitException, IllegalPositionException{
		switch (condition){
		case 1:
			this.executeCondition1();
			break;
		case 2:
			this.executeCondition2();
			break;
		case 3:
			this.executeCondition3();
			break;
		case 4:
			this.executeCondition4();
			break;
		case 5:
			this.executeCondition5();
			break;
		case 6:
			this.executeCondition6();
			break;
		case 7:
			this.executeCondition7();
			break;
		}
		condition = 0;
	}
	
	/**
	 * If a unit is carrying a boulder, condition 1 is true.
	 * @Post	if the unit is carrying a boulder, condition is 1.
	 * 			| if (isCarryingBoulder())
	 * 			|	condition == 1
	 * @throws GameObjectException 
	 * @throws WorldException 
	 * @throws UnitException 
	 */
	private void checkCondition_1CarryingBoulder() throws GameObjectException, UnitException, WorldException{
		if(this.isCarryingMaterial() && this.material instanceof Boulder)
			condition = 1;
	}
	
	/**
	 * Executes the work that is needed if condition 1 is true.
	 * @effect	the boulder is dropped at the cube targeted by the method workAt(x,y,z).
	 * 			|this.getBoulder().dropBoulder(this, this.NeighbouringCubeToWorkAt)
	 * @effect	the experiencepoints are increased by 10.
	 * 			|this.setExperiencePoints(10)
	 * @throws GameObjectException
	 * @throws UnitException
	 * @throws WorldException
	 */
	private void executeCondition1() throws GameObjectException, UnitException, WorldException{
		if(!this.NeighbouringCubeToWorkAt.isSolidCube())
			this.getBoulder().drop(this, this.NeighbouringCubeToWorkAt);
		else
			this.getBoulder().drop(this, this.getCurrentCube());
		this.setExperiencePoints(10);
	}
	
	/**
	 * If a unit is carrying a Log, condition 2 is true.
	 * @Post if the unit is carrying a log, condition 2 is true.
	 * 		|if (isCarryingLog)
	 * 		|	condition == 2;
	 * @throws GameObjectException 
	 * @throws WorldException 
	 * @throws UnitException 
	 */
	private void checkCondition_2CarryingLog() throws GameObjectException, UnitException, WorldException{
		if (this.isCarryingMaterial() && this.material instanceof Log){
			condition = 2;
		}
	}
	
	/**
	 * Executes the work that is needed if condition 2 is true.
	 * @effect	the log is dropped at the cube targeted by the method workAt(x,y,z).
	 * 			|this.getLog().dropLog(this, this.NeighbouringCubeToWorkAt)
	 * @effect	the experiencepoints are increased by 10.
	 * 			|this.setExperiencePoints(10)
	 * @throws GameObjectException
	 * @throws UnitException
	 * @throws WorldException
	 */
	private void executeCondition2() throws GameObjectException, UnitException, WorldException{
		if(!this.NeighbouringCubeToWorkAt.isSolidCube())
			this.getLog().drop(this,NeighbouringCubeToWorkAt);
		else
			this.getLog().drop(this, this.getCurrentCube());
		this.setExperiencePoints(10);
	}
	
	/**
	 * If the unit is located in a Workshop cube and one log and one boulder are available on that cube, condition 3 is true.
	 * @Post if the terraintype is workshop, and there is a boulder and a log present, condition 3 is true.
	 * 		|if((World.getInstance().getTerrainTypeOfCube( 	(int) this.NeighbouringCubeToWorkAt.getPositionX(),
	 *		|											(int) this.NeighbouringCubeToWorkAt.getPositionY(),
	 *		|											(int) this.NeighbouringCubeToWorkAt.getPositionZ()) 
	 *		|== TerrainType.workshop.getNumber()
	 *		|&& (World.getInstance().getCubeByPosition(	(int)this.NeighbouringCubeToWorkAt.getPositionX(),
	 *		|										(int)this.NeighbouringCubeToWorkAt.getPositionY(),
	 *		|										(int)this.NeighbouringCubeToWorkAt.getPositionZ()).isBoulderPresent())
	 *		|&& (World.getInstance().getCubeByPosition(	(int)this.NeighbouringCubeToWorkAt.getPositionX(),
	 *		|										(int)this.NeighbouringCubeToWorkAt.getPositionY(),
	 *		|										(int)this.NeighbouringCubeToWorkAt.getPositionZ()).isLogPresent())
	 *		|	condition == 3
	 * 
	 * @throws GameObjectException
	 * @throws WorldException
	 */	
	private void checkCondition_3ExecuteWorkshop() throws GameObjectException, WorldException{
		if ((World.getInstance().getTerrainTypeOfCube( 	(int) this.NeighbouringCubeToWorkAt.getPositionX(),
														(int) this.NeighbouringCubeToWorkAt.getPositionY(),
														(int) this.NeighbouringCubeToWorkAt.getPositionZ()) 
			== TerrainType.workshop.getNumber())
			&& 
			(World.getInstance().getCubeByPosition(	(int)this.NeighbouringCubeToWorkAt.getPositionX(),
													(int)this.NeighbouringCubeToWorkAt.getPositionY(),
													(int)this.NeighbouringCubeToWorkAt.getPositionZ()).isBoulderPresent()) 
			&& 
			(World.getInstance().getCubeByPosition(	(int)this.NeighbouringCubeToWorkAt.getPositionX(),
													(int)this.NeighbouringCubeToWorkAt.getPositionY(),
													(int)this.NeighbouringCubeToWorkAt.getPositionZ()).isLogPresent())) 
			condition = 3;
	}
	
	/**
	 * Executes the work that is needed after condition 3 is true.
	 * @effect	the boulder and the log that are present are consumed.
	 * 			| World.getInstance().getCubeByPosition(NeighbouringCubeToWorkAt.getPosition()).getBoulder().consumeBoulder()
	 * 			| World.getInstance().getCubeByPosition(NeighbouringCubeToWorkAt.getPosition()).getLog().consumeLog()
	 * @effect	the toughness is increase by 5.
	 * 			|this.setToughness(this.getToughness()+5)
	 * @effect	the weight is increased by 5.
	 * 			|this.setWeight(this.getWeight() + 5)
	 * @effect	the experiencepoints are increased by 10.
	 * 			|this.setExperiencePoints(10)
	 * @throws GameObjectException
	 */
	private void executeCondition3() throws GameObjectException{
		World.getInstance().getCubeByPosition(	(int) this.NeighbouringCubeToWorkAt.getPositionX(),
												(int) this.NeighbouringCubeToWorkAt.getPositionY(),
												(int) this.NeighbouringCubeToWorkAt.getPositionZ()).getBoulder().consume();
		World.getInstance().getCubeByPosition(	(int) this.NeighbouringCubeToWorkAt.getPositionX(),
												(int) this.NeighbouringCubeToWorkAt.getPositionY(),
												(int) this.NeighbouringCubeToWorkAt.getPositionZ()).getLog().consume();

		this.setToughness(this.getThoughness() + 5);
		this.setWeight(this.getWeight() + 5);
		this.setExperiencePoints(10);
	}
	
	/**
	 * If a boulder is present at the cube, condition 4 is true.
	 * @Post	if there is a boulder present, condition 4 is true.
	 * 			|if ( World.getInstance().getCubeByPosition(	(int) this.NeighbouringCubeToWorkAt.getPositionX(),
	 *			|												(int) this.NeighbouringCubeToWorkAt.getPositionY(),
	 *			|												(int) this.NeighbouringCubeToWorkAt.getPositionZ()).isBoulderPresent())
	 *			|	condition == 4
	 * @throws GameObjectException 
	 * @throws UnitException 
	 * @throws WorldException 
	 */
	private void checkCondition_4PickUpBoulder() throws GameObjectException, UnitException, WorldException{
		if (World.getInstance().getCubeByPosition(	(int) this.NeighbouringCubeToWorkAt.getPositionX(),
													(int) this.NeighbouringCubeToWorkAt.getPositionY(),
													(int) this.NeighbouringCubeToWorkAt.getPositionZ()).isBoulderPresent())
			 condition = 4;		
	}
	
	/**
	 * Executes the work that is needed if condition 4 is true.
	 * @effect	the boulder is picked up.
	 * 			|World.getInstance().getCubeByPosition(	(int) this.NeighbouringCubeToWorkAt.getPositionX(),
	 *			|								(int) this.NeighbouringCubeToWorkAt.getPositionY(),
	 *			|								(int) this.NeighbouringCubeToWorkAt.getPositionZ()).getBoulder().pickUpBoulder(this)
	 * @effect	the experiencepoints are increased by 10.
	 *			|this.setExperiencePoints(10) 
	 * @throws GameObjectException
	 * @throws UnitException
	 * @throws WorldException
	 */
	private void executeCondition4() throws GameObjectException, UnitException, WorldException{
		World.getInstance().getCubeByPosition(	(int) this.NeighbouringCubeToWorkAt.getPositionX(),
												(int) this.NeighbouringCubeToWorkAt.getPositionY(),
												(int) this.NeighbouringCubeToWorkAt.getPositionZ()).getBoulder().pickUp(this);
		this.setExperiencePoints(10);
	}
	
	/**
	 * If a log is present at the cube, condition 5 is true.
	 * @Post	if there is a log present, condition 5 is true.
	 * 			|if (World.getInstance().getCubeByPosition(	(int) this.NeighbouringCubeToWorkAt.getPositionX(),
	 *			|								(int) this.NeighbouringCubeToWorkAt.getPositionY(),
	 *			|								(int) this.NeighbouringCubeToWorkAt.getPositionZ()).isLogPresent())
	 *			|	condition == 5;
	 * @throws GameObjectException 
	 * @throws WorldException 
	 * @throws UnitException 
	 */
	private void checkCondition_5PickUpLog() throws GameObjectException, UnitException, WorldException{
		if (World.getInstance().getCubeByPosition(	(int) this.NeighbouringCubeToWorkAt.getPositionX(),
													(int) this.NeighbouringCubeToWorkAt.getPositionY(),
													(int) this.NeighbouringCubeToWorkAt.getPositionZ()).isLogPresent())
			 condition = 5;
	}
	
	/**
	 * Executes the work that is needed if condition 5 is true.
	 * @effect	the log is picked up.
	 * 			|World.getInstance().getCubeByPosition(	(int) this.NeighbouringCubeToWorkAt.getPositionX(),
	 *			|								(int) this.NeighbouringCubeToWorkAt.getPositionY(),
	 *			|								(int) this.NeighbouringCubeToWorkAt.getPositionZ()).getLog().pickUpLog(this)
	 *@effect	the experiencepoints are increased by 10.
	 *			|this.setExperiencePoints(10)
	 * @throws GameObjectException
	 * @throws UnitException
	 * @throws WorldException
	 */
	private void executeCondition5() throws GameObjectException, UnitException, WorldException{
		World.getInstance().getCubeByPosition(	(int) this.NeighbouringCubeToWorkAt.getPositionX(),
												(int) this.NeighbouringCubeToWorkAt.getPositionY(),
												(int) this.NeighbouringCubeToWorkAt.getPositionZ()).getLog().pickUp(this);
		this.setExperiencePoints(10);
	}
	
	/**
	 * If The targetcube was a tree, condition 6 is true.
	 * @Post	if the targetcube was a tree, condition 6 is true.
	 * 			|if (this.neighbouringCubeToWorkAt.getTerrainType == TerrainType.tree)
	 * 			|	condition == 6
	 *  
	 * @throws GameObjectException
	 * @throws WorldException 
	 * @throws IllegalPositionException 
	 */
	private void checkCondition_6ChopTree() throws GameObjectException, WorldException{
		if (!(this.NeighbouringCubeToWorkAt == null))
			if (this.NeighbouringCubeToWorkAt.getTerrainType() == TerrainType.tree)
				condition = 6;					
	}
	
	/**
	 * Executes the work that is needed if condition 6 is true.
	 * @effect	the targetcube caves in.
	 * 			|this.neighbouringCubeToWorkAt.caveIn()
	 * @effect	the experiencePoints are increased by 10
	 * 			|this.setExperiencePoints(10)
	 * @Post	the variable NeighbouringCubeToWorkAt is null.
	 * @throws GameObjectException
	 * @throws WorldException
	 * @throws IllegalPositionException 
	 */
	private void executeCondition6() throws GameObjectException, WorldException, IllegalPositionException{
		this.NeighbouringCubeToWorkAt.caveIn();
		this.NeighbouringCubeToWorkAt = null;
		this.setExperiencePoints(10);
	}
	
	/**
	 * If The targetcube was a rock, condition 7 is true.
	 * @Post	if the targetCube is a rock, condition 7 is true.
	 * 			|if (this.NeighbouringCubeToWorkAt.getTerrainType() == TerrainType.rock)
	 * 			|	condition == 7
	 * @throws GameObjectException
	 * @throws WorldException 
	 * @throws IllegalPositionException 
	 */
	private void checkCondition_7SmashRock() throws GameObjectException, WorldException{
		if (!(this.NeighbouringCubeToWorkAt == null))
			if (this.NeighbouringCubeToWorkAt.getTerrainType() ==  TerrainType.rock)
				condition = 7;
	}
	
	/**
	 * Executes the work that is needed if condition 7 is true.
	 * @effect	the targetCube caves in.
	 * 			|this.NeighbouringCubeToWorkAt.caveIn()
	 * @effect	the experiencePoints are increased by 10
	 * 			|this.setExperiencePoints(10)
	 * @Post	the variable NeighbouringCubeToWorkAt becomes null.
	 * 			|this.NeighbouringCubeToWorkAt == null
	 * @throws GameObjectException
	 * @throws WorldException
	 * @throws IllegalPositionException 
	 */
	private void executeCondition7() throws GameObjectException, WorldException, IllegalPositionException{
		this.NeighbouringCubeToWorkAt.caveIn();
		this.NeighbouringCubeToWorkAt = null;
		this.setExperiencePoints(10);
	}
	
	/**
	 * An object that implements the actions possible in Default Behaviour.
	 */
	public final DefaultBehaviour myDefaultBehaviourClass = new DefaultBehaviour(this);
	
	/**
	 * A method to start the default behaviour of a Unit.
	 * @effect if the unit is not executing a task and the tasklist is not empty and there is a task to be executed, the unit will pick the task with the highest priority, and execute it.
	 * 			|if(!this.isExecutingTask() && !scheduler.getTaskList().isEmpty && scheduler.getHighestPriorityTask() != null)
	 * 			|	scheduler.getHighestPriorityTaks().executeTask(this)
	 * @effect if the unit is not executing a task and the tasklist is empty or there is no task available for execution, the unit will pick a method from MyDefaultBehaviourClass and invoke it.
	 * 			|if(!this.isExecutingTask() && (scheduler.getTaskList.isEmpty() || scheduler.getHightestPriorityTask() == null))
	 * 			|	Method[] methods = myDefaultBehaviourClass.getClass().getDeclaredMethods()
	 * 			|	methods[randomIndex].invoke(myDefaultBehaviourClass) 
	 * @throws UnitException 
	 * @throws IllegalPositionException 
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException 
	 * @throws TaskException 
	 * @throws ExpressionException 
	 * @throws StatementException 
	 * @throws SchedulerException 
	 * @throws StatementBreakException 
	 * @throws IllegalStateException 
	 */
	public void startDefaultBehaviour() throws UnitException, IllegalPositionException, TaskException, ExpressionException, StatementException, SchedulerException{
		this.defaultBehaviourEnabled = true;
		this.setMovementType(MovementType.default_behaviour);
		//checking if there is an enemy in the neighbourhood.
		Scheduler scheduler = this.getFaction().getScheduler();
		if(!this.isExecutingTask()){
		if(scheduler.getTaskList().isEmpty() || scheduler.getHighestPriorityTask() == null){
			Unit enemy = this.getNeighbouringEnemy();
			if(enemy != null)
				attack(enemy);
			else{				
				Method[] methods = myDefaultBehaviourClass.getClass().getDeclaredMethods();
			
				int index = (int)(Math.random()*methods.length);

				try {
					methods[index].invoke(myDefaultBehaviourClass);
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.getCause();
				}

				if (this.getMovementType() == MovementType.walking){
					if (Math.random()>0.4)
						this.sprint();
				}
			}
		}else {
			Task task = scheduler.getHighestPriorityTask(); 
				task.executeTask(this);

		}
		}
	}

	/**
	 * If there is a unit present in one of the neighbouring cubes, and this unit has not the same faction, then this method returns this unit.
	 * @return a unit that is present in a neighbouring cube, if there is one.
	 * @throws IllegalPositionException
	 */
	@Raw
	private Unit getNeighbouringEnemy() throws IllegalPositionException {
		for(Cube neighbour: World.getInstance().getNeighbouringCubes(this.getCurrentCube()))
			if (neighbour.isUnitPresent())
				for(Unit enemy: neighbour.getUnits()){
					if (this.getFaction() != enemy.getFaction()){
						return enemy;
						}}
		return null;
	}
	
	/**
	 * A method to stop the default behaviour of a Unit.
	 * @post default behaviour is not enabled
	 * 			|new.defaultBehaviourEnabled == false;
	 * @effect the movementType is set to nothing.
	 * 			|this.setMovementType(MovementType.nothing)
	 */
	public void stopDefaultBehaviour(){
		this.defaultBehaviourEnabled = false;
		this.setMovementType(MovementType.nothing);
		if(this.currentStatement != null){
			this.currentStatement = null;
			if(this.isExecutingTask())
				this.getAssignedTask().finishTask();
		}
	}
	
	/**
	 * This method generates a random position to work in that is a neighboring cube of the current position.
	 * @return a position to work in that is neighbouring the currentPosition or the currentPosition itself.
	 * @throws IllegalPositionException 
	 */
	public Position getRandomWorkPos() throws IllegalPositionException{
		int[] myIntArray = {0,1,2,3,4};
		int choice;
		
		choice = this.getRandom(myIntArray);
		
		if (choice == 0)
			return this.currentPosition;
		else if (choice == 1){
			return new Position((int)currentPosition.position_x+1,(int)currentPosition.position_y,(int)currentPosition.position_z);
		}else if (choice == 2){
			return new Position((int)currentPosition.position_x-1,(int)currentPosition.position_y,(int)currentPosition.position_z);
		}else if (choice == 3){
			return new Position((int)currentPosition.position_x,(int)currentPosition.position_y+1,(int)currentPosition.position_z);
		}else{
			return new Position((int)currentPosition.position_x,(int)currentPosition.position_y-1,(int)currentPosition.position_z);
		}		
	}
	
	/**
	 * A method to initiate walking. 
	 * @effect Sets the movementType of a unit to walking.
	 * 		|this.setMovementType(MovementType.walking)
	 */
	public void Walk() {
		setMovementType(MovementType.walking);
	}
	
	/**
	 * A method to invoke the calculation of the temporary position while walking.
	 * @effect the temporary position is calculated.
	 * 			|this.calculateTemporaryPosition(gameTime, this.getWalkingSpeed)
	 * @param gameTime
	 * @throws UnitException 
	 * @throws WorldException 
	 * @throws GameObjectException 
	 * @throws TaskException 
	 * @throws ExpressionException 
	 * @throws StatementException 
	 * @throws StatementBreakException 
	 * @throws IllegalStateException 
	 * 
	 */
	private void walk(double gameTime) throws UnitException, WorldException, GameObjectException, TaskException, StatementException, ExpressionException{
		this.calculateTemporaryPosition(gameTime, this.getWalkingSpeed());	
	}
	
	
	/**
	 * Returns true if a unit is allowed to have currentMovementType equal to sprinting.
	 * @return	true if a unit is currently walking or sprinting and is not exhausted (stamina = 0) yet.
	 * 			| (if (currentMovementtype = walking) || (currentMovementType = sprinting) && (getstaminapoints() > 0)
	 * 			| then result = true;
	 * 			| else result = false;
	 */
	private boolean isSprintingAllowed(){
		if ( ((this.currentMovementType == MovementType.walking)|| (this.currentMovementType == MovementType.sprinting)) 
				&& (this.getStaminapoints() > 0))
			return true;
		else
			return false;			
	}
	
	/**
	 * A unit stops sprinting if it has reached it's destination or is exhausted. if at dest then rest,
	 * if exhausted walk.
	 * @effect the movementType of a unit is updated to walking
	 * 			|this.setMovementType(MovementType.walking)
	 */
	public void stopSprinting(){
		setMovementType(MovementType.walking);
		
	}
	
	/**
	 * A method to initiate sprinting.
	 * @effect Sets the movementtype of a unit to sprinting
	 * 		|if(isSprintingAllowed == ture)
	 * 		|	setMovementType(MovementType.sprinting)
	 */
	public void sprint() {
		if(isSprintingAllowed() == true) 
			setMovementType(MovementType.sprinting);
	}
	
	/**
	 * A method that keeps track of the sprinting.
	 * @post the gameTime is added to timeSprinting.
	 * 			|new.timeSprinting == this.timeSprinting + gameTime
	 * @effect if the reduction is bigger or equal to 1, staminapoints is decreased by 1.
	 * 			|if(reduction >= 1)
	 * 			|	this.decreaseStaminapoints(1)
	 * @effect if the staminapoints are smaller or equal to zero, the unit stops sprinting.
	 * 			|if(this.getStaminapoints()<= 0)
	 * 			|	this.stopSprinting()
	 * @effect the temporary position is calculated.
	 * 			|this.calculateTemporaryPosition(gameTime, this.getSprintingSpeed)
	 * @param gameTime
	 * @throws UnitException 
	 * @throws WorldException 
	 * @throws GameObjectException 
	 * @throws TaskException 
	 * @throws ExpressionException 
	 * @throws StatementException 
	 * @throws StatementBreakException 
	 * @throws IllegalStateException 
	 */
	private void sprint(double gameTime) throws UnitException, WorldException, GameObjectException, TaskException, StatementException, ExpressionException{
		double reduction =0;
		if (this.isSprintingAllowed()){
			
			this.timeSprinting += gameTime;
			
			reduction = (this.timeSprinting/0.1);			
			
			if (reduction >= 1){
				this.decreaseStaminapoints(1);
				this.timeSprinting -= 0.1;
				reduction -= 1;
				}
			
			if (this.getStaminapoints()<= 0)
				this.stopSprinting();
			
			this.calculateTemporaryPosition(gameTime, this.getSprintingSpeed());
		}
	}	
	
	/**

	 * This method is called maximum every 0.2 seconds. It keeps track of what's going on with a unit. And 
	 * assigns tasks to the unit.
	 * 
	 * @effect	if the movementType of the unit is working, then the method this.work(gametime) has to be called.
	 * 			| if (this.getMovementType() == MovementType.working)
	 * 			|	this.work(GameTime)
	 * @effect	if the movementType of the unit is fighting, then the method this.attack(gametime) has to be called.
	 * 			| if (this.getMovementType() == MovementType.fighting)
	 * 			|	this.fight(GameTime)
	 * @effect	if the movementType of the unit is resting, then the method this.rest(gametime) has to be called.
	 * 			| if (this.getMovementType() == MovementType.resting)
	 * 			|	this.rest(GameTime)
	 * @effect	if the movementType of the unit is walking, then the method this.walk(gametime) has to be called.
	 * 			| if (this.getMovementType() == MovementType.walking)
	 * @effect	if the movementType of the unit is sprinting, then te method this.sprint(gametime) has to be called.
	 * 			| if (this.getMovementType() == MovementType.sprinting)
	 * 			|	this.sprint(GameTime)
	 * @effect	if the movementType of the unit is default_behaviour, then the method this.startDefaultBehaviour has to be called.
	 * 			| if (this.getMovementType() == MovementType.default_behaviour)
	 * 			|	this.startDefaultBehaviour()
	 * @effect	if the movementType of the unit is not resting, then we must add time to the variable TimeSinceRest to make sure that 
	 * 			a unit rests every 3 minutes of gametime
	 * 			| if (this.getMovementType() != MovementType.resting)
	 * 			|	this.TimeSinceRest += GameTime;
	 * @param GameTime
	 * @throws GameObjectException 
	 * @throws WorldException 
	 * @throws IllegalPositionException 
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException 
	 * @throws SecurityException 
	 * @throws NoSuchMethodException 
	 * @throws TaskException 
	 * @throws ExpressionException 
	 * @throws StatementException 
	 * @throws SchedulerException 
	 * @throws StatementBreakException 
	 * @throws IllegalStateException 
	 */

	public void advanceTime(double GameTime) throws UnitException, IllegalPositionException, WorldException, GameObjectException, TaskException, ExpressionException, StatementException, SchedulerException{
		// check whether the unit must fall or not	
		this.isStable();

		if (this.mustStop == true && this.getMovementType() == MovementType.nothing){
			this.setMovementType(movementTypeBeforeNewRequest);
			mustStop = false;
		}
				
		if (this.getHitpoints() <= 0)
			this.die();
		
		if (this.getMovementType() != MovementType.death){
				if (this.timeSinceRest > 3*60){
					if(this.isExecutingTask())
						this.getAssignedTask().interruptTask();
					this.rest();
				}
				else if(this.getMovementType() == MovementType.falling){
					this.fall(GameTime);
					}

				else if (this.isFollowing == true)
					this.followUnit();
				
				else if (this.getMovementType() == MovementType.working){
					this.work(GameTime);
					}
				
				else if (this.getMovementType() == MovementType.fighting){
					this.attack(GameTime);
					}
				
				else if (this.getMovementType() == MovementType.resting)
					this.rest(GameTime);
				
				else if (this.getMovementType() == MovementType.walking){
					this.walk(GameTime);
				}
					
				else if (this.getMovementType() == MovementType.sprinting)
					this.sprint(GameTime);
				
				else if (this.getMovementType() == MovementType.default_behaviour)
					this.startDefaultBehaviour();
				
				else if ((this.getMovementType() == MovementType.nothing) && (this.defaultBehaviourEnabled == true)){
					this.startDefaultBehaviour();
				}
				
				if ( this.getMovementType() != MovementType.resting){
					this.timeSinceRest += GameTime;
					this.firstPointAdded = false;
					}
				}		
		}
		
	/**
	 * Checks the replacement of a Unit in a timespan of seconds: GameTime.
	 * 
	 * @param GameTime
	 * @param speed
	 * @throws UnitException 
	 * @throws WorldException 
	 * @throws GameObjectException 
	 * @throws TaskException 
	 * @throws ExpressionException 
	 * @throws StatementException 
	 * @throws StatementBreakException 
	 * @throws IllegalStateException 
	 */
	private void calculateTemporaryPosition(double GameTime, double speed) throws UnitException, WorldException, GameObjectException, TaskException, StatementException, ExpressionException{	
		
		reachedTargetCubePosition();
		double v = speed;
		boolean arrived = false;
		
		double v_x = this.getVelocity_X(v, currentPosition, targetCube);
		double v_y = this.getVelocity_y(v, currentPosition, targetCube);
		double v_z = this.getVelocity_Z(v, currentPosition, targetCube);

		double x = this.getPositionGameObjectX() + v_x * GameTime;
		double y = this.getPositionGameObjectY() + v_y * GameTime;
		double z = this.getPositionGameObjectZ() + v_z * GameTime;
		
		if(Util.fuzzyEquals(x, this.targetPosition.position_x) && Util.fuzzyEquals(y, this.targetPosition.position_y) && Util.fuzzyEquals(z,this.targetPosition.position_z)) { 
			   
			   x = this.targetPosition.position_x;
			   y = this.targetPosition.position_y;
			   z = this.targetPosition.position_z;

			   arrived = true;
			   
			   if (this.getMovementType() == MovementType.falling)
				   this.decreaseHitpoints(10);
			   
			   if (this.currentStatement != null){
				   if(this.currentStatement instanceof MoveToStatement){
					   this.currentStatement.finishStatement();
				   }
				   	   
			   }
			   else if (this.defaultBehaviourEnabled == true && !this.isExecutingTask()){
				   this.setMovementType(MovementType.default_behaviour);
			   }
			   else
				   this.setMovementType(MovementType.nothing);
			   }
		
		if (! arrived)
			this.setWalkingOrientation(v_x, v_y);
		
		this.setObjectPosition(x, y, z);
	}
	
	/**
	 * Method to check whether a Unit has reached it's targetcube position. If this is the case and the unit is not centering,
	 * this means the unit has taken one step and can gain an experience point.
	 * 
	 * @throws UnitException
	 * @throws WorldException 
	 * @throws GameObjectException 
	 */
	private void reachedTargetCubePosition() throws UnitException, WorldException, GameObjectException{
		if (currentPosition.isSamePosition(targetCube, 0.05)){
				this.setObjectPosition(targetCube.position_x, targetCube.position_y, targetCube.position_z);
				if (!this.centeringUnit)
					this.setExperiencePoints(1);
				else 
					this.centeringUnit = false;
	
				try {
					this.getNextTargetCube();
				} catch (IllegalPositionException e) {
					throw new UnitException(e.getMessage());
				}
		}
	}
	
	
	/**
	 * Method to initiate a movement to a specific point whitin the game world.
	 * 
	 * @param 	x
	 * 			x coordinate to move to
	 * @param 	y
	 * 			y coordinate to move to
	 * @param 	z
	 * 			z coordinate to move to
	 * @effect 	this.setTargetPositionX(x)
	 * @effect 	this.setTargetPositionY(y)
	 * @effect 	this.setTargetPositionZ(z)
	 * @post 	MovementType is equal to walking
	 * 			| new.setMovementType(MovementType.walking)
	 * @post	If (x,y,z) is a valid position, the new targetposition equals (x,y,z)
	 * 			|if isValidPosition(x)&& isValidPosition(y)&& isValidPosition(z)
	 * 			|then new.getTargetpositionX() == x && new.getTargetpositionY() == y && new.getTargetpositionZ() == z	 * 			
	 * @throws 	IllegalPositionException
	 * 			The coordinates to which the unit wants to move is not a valid position
	 * 			| if (! this.isValidPosition(x, MAXPOSITIOIN_X)
	 * 			|	throw new IllegalPositionException(x);
	 * 			| if (! this.isValidPosition(y, MAXPOSITION_Y)
	 * 			|	throw new IllegalPosition(y)
	 * 			| else
	 * 			|	throw new IllegalPosition(z)
	 */
	public void moveTo(double x, double y, double z) throws UnitException, IllegalPositionException {
		if ( 	(World.isValidPosition(x, World.MAXPOSITION_X)) &&
				(World.isValidPosition(y, World.MAXPOSITION_Y)) && 
				(World.isValidPosition(z, World.MAXPOSITION_Z)) &&
				(World.getInstance().getCubeByPosition((int)x, (int)y, (int)z).isPassableCube())&&
				(World.getInstance().hasSolidNeighbour(World.getInstance().getCubeByPosition((int)x,(int)y,(int)z))))
			{ 	
					this.Walk();
					this.setTargetPosition(x, y, z);
					this.getNextTargetCube();
			}
		
		else{
			if((World.isValidPosition(x, World.MAXPOSITION_X)) == false)
				throw new IllegalPositionException(x);
			else if ((World.isValidPosition(y, World.MAXPOSITION_Y)) == false)
				throw new IllegalPositionException(y);
			else
				throw new IllegalPositionException(z);
		}
	}
	


	/**
	 * This method searches for the next cubes that have to be checked for the Path Finding algorithm.
	 * @param cube
	 * 		the cube whose neighbours need to be checked.
	 * @param n_0
	 * 		the number of steps that are necessary to reach the given cube.
	 * @throws IllegalPositionException
	 */

	private void search(Cube cube, int n_0) throws IllegalPositionException{
	
		for(Cube neighbour : World.getInstance().getNeighbouringCubes(cube)){
			
			if(!this.isAlreadyInQueue(neighbour)){
				if(neighbour.isPassableCube()){
					//check neighbouring solid terrain
					if (World.getInstance().hasSolidNeighbour(neighbour) || neighbour.getPositionZ() == 0){
						Queue.add(new QueueElement<>(neighbour,n_0+1));
						}
					}
				}
			}		
	}
	
	/**
	 * A method that checks whether the given cube is already in the Queue with a number of steps that is less than the given number of steps as parameter.
	 * @param cube
	 * 		the cube that need to be checked
	 * @param n_0
	 * 		the number of steps it takes to reach this cube
	 * @return
	 */
	private boolean isAlreadyInQueue(Cube cube, int n_0){
		boolean result = false;

		for (QueueElement<Cube,Integer> element : Queue)
			if((element.getFirstValue() == cube)&&(element.getSecondValue() <= n_0)){
				result = true;	
			}
		return result;		
	}//we chose to not use this method due to performance reasons.
	
	/**
	 * A method that checks whether the given cube is already in the Queue.
	 * @param cube
	 * @return
	 */
	private boolean isAlreadyInQueue(Cube cube){
		boolean result = false;
		
		for (QueueElement<Cube,Integer> element: Queue)
			if (element.getFirstValue() == cube)
				result = true;
		return result;
	}
	
	/**
		 * Find the next intermediary targetpoint to move towards the targetposition.
		 * @throws IllegalPositionException 
		 * @throws UnitException 
		 * 
		 * @Post 	the targetPosition is added to the cube.
		 * @effect 	as long as the currentPosition is not yet in the Queue, and the Queue still has a next object, the method search is called for the next cube with the given steps.
		 * 			|if(!isAlreadyInQueue(World.getInstance().getCubeByPosition(currentPosition)) && (Queue.hasNext())
		 * 			|	search(nextCube, n)
		 * @effect 	if the currentPosition is already in the Queue, move to the cube that has the least number of steps.
		 * 			|if(isAlreadyInQueue(World.getInstance().getCubeByPosition(currentPosition))
		 * 			|	MoveToAdjacent(next.getPositionX(), next.getPositionY(), next.getPositionZ())
		 */
		@Raw
		private void getNextTargetCube() throws UnitException, IllegalPositionException{
			Cube cube;
			int n;
			int minimum = Integer.MAX_VALUE;
			Cube next;
			QueueElement<Cube,Integer> queueElement;
			Queue  = new ArrayList<QueueElement<Cube,Integer>>();
			int i = 0;
			Queue.add(new QueueElement<>(World.getInstance().getCubeByPosition(targetPosition), 0));
		
			while ((!isAlreadyInQueue(World.getInstance().getCubeByPosition(currentPosition)))&&(i<Queue.size())){
				queueElement = Queue.get(i);
				cube = queueElement.getFirstValue();
				n = queueElement.getSecondValue();
				
				this.search(cube, n);
				
				i ++;
			}		
			//if currentCube is reachable from targetCube
			if(isAlreadyInQueue(World.getInstance().getCubeByPosition(currentPosition))){
				
				Set<Cube> neighbours = World.getInstance().getNeighbouringCubes(this.getCurrentCube());
				
				next = this.getCurrentCube();
				for(QueueElement<Cube,Integer> element : Queue){
					
					if(neighbours.contains(element.getFirstValue())){
						
						if(element.getSecondValue() < minimum){
						minimum = element.getSecondValue();
						next = element.getFirstValue();
						}}}
				moveToAdjacent((int)(next.getPositionX()-this.getGameObjectPositionXGameworld()), (int)(next.getPositionY()-this.getGameObjectPositionYGameworld()),(int)(next.getPositionZ()-this.getGameObjectPositionZGameworld()));
				
			}else{
				if(this.isExecutingTask())
					try {
						this.currentStatement = null;
						this.getAssignedTask().interruptTask();
					} catch (TaskException e) {
						throw new UnitException(e.getMessage());
					}
				this.setTargetPosition(this.getCurrentCube().getPositionX()+World.LENGTH_CUBE/2, 
											this.getCurrentCube().getPositionY()+World.LENGTH_CUBE/2,
											this.getCurrentCube().getPositionZ()+World.LENGTH_CUBE/2);
				   
			}
			}


	
	/**
	 * Returns the current cube that is occupied by the unit.
	 * 
	 * @return
	 */
	@Raw @Basic
	public Cube getCurrentCube(){
		return World.getInstance().getCubeByPosition(currentPosition);
	}

	/**
	 * Method to initiate  movement to neighbouring cube.
	 * 
	 * @param 	xMovement
	 * 			The movement in the x direction.
	 * @param 	yMovement
	 * 			The movement in the y direction.
	 * @param 	zMovement
	 * 			The movement in the z direction.
	 * 
	 * @effect	The unit's position is changed from (x,y,z) to (x',y',z') = 
	 * 			(Xc' + xMovement + (LENGTH_CUBE/2), Yc' + yMovement + (LENGTH_CUBE/2), Zc' +
	 * 			 zMovement + (LENGTH_CUBE/2) )
	 * 			| this.targetCubeX = getPositionXGameworld() + xMovement * (LENGTH_CUBE/2)) 
	 * 			| this.targetCubeY = getPositionXGameworld() + yMovement * (LENGTH_CUBE/2)) 
	 * 			| this.targetCubeZ = getPositionXGameworld() + zMovement * (LENGTH_CUBE/2)) 
	 * 
	 */
	private void moveToAdjacent(int xMovement,int yMovement,int zMovement) throws UnitException{
		if(isNeighbouringCube(xMovement,yMovement,zMovement) == true){
			this.setTargetCube(this.getGameObjectPositionXGameworld() + (World.LENGTH_CUBE/2) + xMovement,
						   this.getGameObjectPositionYGameworld() + (World.LENGTH_CUBE/2) + yMovement, 
						   this.getGameObjectPositionZGameworld() + (World.LENGTH_CUBE/2) + zMovement);
		}
	}
	
	/**
	 * A method that makes a Unit move to a random position.
	 * 
	 * @throws IllegalPositionException
	 * 		if the Xrandom position is not a valid coordinate, throw exception
	 * 		| if (! this.isValidPosition(Xrandom, MAXPOSITIOIN_X)
	 * 		|	throw new IllegalPositionException(Xrandom);
	 * 		| if (! this.isValidPosition(Yrandom, MAXPOSITION_Y)
	 * 		|	throw new IllegalPosition(Yrandom)
	 */
	public void moveToRandomPos() throws UnitException{
		int[] randomPos = this.getRandomPos_limited();
		
		int xRandom = randomPos[0];
		int yRandom = randomPos[1];
		int zRandom = randomPos[2];
		
		try{
			this.moveTo(xRandom + World.LENGTH_CUBE/2, yRandom + World.LENGTH_CUBE/2, zRandom + World.LENGTH_CUBE/2);
		}catch (IllegalPositionException e){
			throw new UnitException (e.getMessage());
		}
	}
	
	/**
	 * A method that generates a random position to move to that is limited in distance from the current position of the Unit.
	 * @note the limited distance is used for better performance of the game.
	 * @return int[] that is a valid position.
	 */
	private int[] getRandomPos_limited(){
		int[] position = new int[3];
		position[0] = (int)(this.getPositionGameObjectX()+ Math.random()*8) - 4;
		position[1] = (int)(this.getPositionGameObjectY()+ Math.random()*8) - 4;
		position[2] = (int)(this.getPositionGameObjectZ()+ Math.random()*8) - 4;
		if ((World.isValidPosition(position[0], World.MAXPOSITION_X))
				&& (World.isValidPosition(position[1], World.MAXPOSITION_Y)) 
				&& (World.isValidPosition(position[2], World.MAXPOSITION_Z))){
			if (World.getInstance().getCubeByPosition(position[0], position[1], position[2]).isPassableCube()){
				if (position[2] == 0) return position;
				else if (World.getInstance().hasSolidNeighbour(World.getInstance().getCubeByPosition(position[0], position[1], position [2]))){
					return position;
				}
			else return getRandomPos_limited();
		}else return getRandomPos_limited();
		}else return getRandomPos_limited();
	}
	
	/**
	 * returns true if the movementtype of a unit is walking or sprinting.	
	 * @return
	 */
	public boolean isMoving(){
		return (this.getMovementType() == MovementType.walking) || (this.getMovementType() == MovementType.sprinting);
	}
	

	/**
	 * Boolean to indicate whether a unit is walking towards the center of a cube (centering). This is the case
	 * when moving is interrupted.
	 */
	public boolean centeringUnit = false;
	
	/**
	 * Method to make the unit walk towards the middle of the current cube.
	 * 
	 * @note if this boolean is true, a unit must not gain an experience point when it reaches it's targetPosition
	 * @param type
	 * 			The movementType the unit must execute after it has centred
	 * @throws UnitException
	 * @effect the targetPosition is set to the current position of the unit.
	 * 			|this.setTargetPosition(Math.floor(this.getPositionGameObjectX()) + World.LENGTH_CUBE/2,
	 *							Math.floor(this.getPositionGameObjectY()) + World.LENGTH_CUBE/2,
	 *							Math.floor(this.getPositionGameObjectZ()) + World.LENGTH_CUBE/2)
	 *@effect the targetCube is set to the current position of the unit.
	 *			|this.setTargetCube(Math.floor(this.getPositionGameObjectX()) + World.LENGTH_CUBE/2,
	 *							Math.floor(this.getPositionGameObjectY()) + World.LENGTH_CUBE/2,
	 *							Math.floor(this.getPositionGameObjectZ()) + World.LENGTH_CUBE/2)
	 *@post centeringUnit is true
	 *			|new.centeringUnit() == true;
	 */
	private void centreUnit(MovementType type) throws UnitException{
		this.setTargetPosition(	Math.floor(this.getPositionGameObjectX()) + World.LENGTH_CUBE/2,
								Math.floor(this.getPositionGameObjectY()) + World.LENGTH_CUBE/2,
								Math.floor(this.getPositionGameObjectZ()) + World.LENGTH_CUBE/2);
		this.setTargetCube(	Math.floor(this.getPositionGameObjectX()) + World.LENGTH_CUBE/2,
							Math.floor(this.getPositionGameObjectY()) + World.LENGTH_CUBE/2,
							Math.floor(this.getPositionGameObjectZ()) + World.LENGTH_CUBE/2);
		this.centeringUnit = true;
		movementTypeBeforeNewRequest = type;
	}
	
	/**
	 * Return the current movementtype of the unit. this can be one of:
	 * walking,sprinting, working, resting, fighting ordefault_behaviour 
	 */
	@Basic @Raw
	public MovementType getMovementType(){
		return this.currentMovementType;
	}
	
	/**
	 * Sets the current movement type to the desired movement type if this is a valid movement type
	 * and changes are allowed.
	 * 
	 * @param 	MovementType
	 * 			The movement type we want to set as current movement type.
	 * @post	The new movement type is a valid movement type.
	 * 			| if new.isValidMovementType() = true
	 * 			| then new.getMovementType() == MovementType
	 */
	public void setMovementType(MovementType MovementType) {
		if (this.isValidMovementType(MovementType) == true){
			this.currentMovementType = MovementType;		
		}	
	}
	
	/**
	 * Method that checks if the movementtype we want to set is a valid movementtype.
	 * Returns true if the movementtype given as parameter is valid given the current movementtype.
	 * 
	 * @param movementType
	 * 				the movementtype we want to set to the unit.
	 * @return
	 * .
	 */
	private boolean isValidMovementType(MovementType movementType){
		if (movementType == MovementType.falling) 
			return true;
		if ((movementType == MovementType.fighting) && (this.getMovementType() == MovementType.falling))
			return false;
		if ((movementType == MovementType.sprinting ) && (this.getMovementType() != MovementType.walking))
			return false;
		else if ((movementType == MovementType.sprinting) && (this.getStaminapoints() <= 0))
			return false;
		else if ((this.firstPointAdded == false)&&(this.getMovementType() == MovementType.resting))
			return false;
		else return true;
	}
	
	/**
	 * Boolean to check the inputvalues of MoveToAdjacent are 0, 1 or -1
	 * @param 	x
	 * 			The x movement
	 * @param 	y
	 * 			The y movement
	 * @param 	z
	 * 			The z movement
	 * @return	true if all 3 parameters are valid
	 */
	private boolean isNeighbouringCube(double x, double y, double z){
		boolean valid = false;
		if (( (x == 1) || (x == -1) || (x == 0) ) &&
			( (y == 1) || (y == -1) || (y == 0) ) &&
			( (z == 1) || (z == -1) || (z == 0) ) )
			valid = true;		
		return valid;
	}	

	/**
	 * Returns the X component velocity
	 * @param v
	 * 		the speed that the unit has at the moment.
	 * @param currentPosition
	 * 		the current position of the unit
	 * @param targetPosition
	 * 		the target position of the unit
	 * @return
	 */
	@Raw @Basic
	private double getVelocity_X(double v, Position currentPosition, Position targetPosition){
		double first;
		first = targetPosition.position_x - currentPosition.position_x;	
		if (Util.fuzzyEquals(currentPosition.getDistance(targetPosition),0))
			return 0;
		else
			return v*first/currentPosition.getDistance(targetPosition);
	}
	
	/**
	 * Returns the Y component velocity
	 * @param v
	 * 		the speed that the unit has at the moment.
	 * @param currentPosition
	 * 		the current position of the unit
	 * @param targetPosition
	 * 		the target position of the unit
	 * @return
	 */
	@Raw @Basic
	private double getVelocity_y(double v, Position currentPosition, Position targetPosition){
		double second;

		second = targetPosition.position_y - currentPosition.position_y;
		
		if (Util.fuzzyEquals(currentPosition.getDistance(targetPosition),0))
			return 0;
		else
			return v*second/currentPosition.getDistance(targetPosition);
	}
	
	/**
	 * Returns the Z component velocity
	 * @param v
	 * 		the speed that the unit has at the moment.
	 * @param currentPosition
	 * 		the current position of the unit
	 * @param targetPosition
	 * 		the target position of the unit
	 * @return
	 */
	@Basic @Raw
	private double getVelocity_Z(double v, Position currentPosition, Position targetPosition){
		double third;
		third = targetPosition.position_z - currentPosition.position_z;
		
		if (Util.fuzzyEquals(currentPosition.getDistance(targetPosition),0))
			return 0;
		else
			return v*third/currentPosition.getDistance(targetPosition);
	}
	
	/**
	 * Method to get a random element out of an array of integers.
	 * 
	 * @param 	array
	 * 			array of integers out of which we want to select a random element.
	 * @return	The selected integer of the array
	 */
	@Raw @Basic
	private int getRandom(int[] array) {
	    int rnd = new Random().nextInt(array.length);
	    return array[rnd];
	}
	
	/**
	 * Function to round a double to the nearest integer value.
	 * 
	 * @param 	d
	 * 			The input double which must be rounded.
	 * @return	The nearest integer value.
	 */
	private int round(double d){
	    double dAbs = Math.abs(d);
	    int i = (int) dAbs;
	    double result = dAbs - (double) i;
	    if(result<0.5){
	        return d<0 ? -i : i;            
	    }else{
	        return d<0 ? -(i+1) : i+1;          
	    }
	}
	
	/**
	 * Boolean to indicate whether a unit must stop walking.	
	 */
	public boolean mustStop = false;
	

	/**
	 * Method to check whether a unit is still alive.
	 * 
	 * @return true if the movementType is not death.
	 * 			|if(this.getMovementType() == MovementType.death)
	 * 			|	return false;
	 * 			|else return true;
	 */
	public boolean isAlive(){
		if (this.getMovementType() == MovementType.death)
			return false;
		else
			return true;
	}
	
	/**
	 * Method to kill a unit. When a unit is dead the movementType is set to dead. The unit must also drop the boulder
	 * and/or log that it is carrying. The unit will also be removed from all the sets where it is a member of.
	 * If the unit was beiing followed by another unit, this method will tell the other unit to stop doiing
	 * this. If this unit was following another unit, this method tells the other unit this unit has stopped
	 * doiing so.
	 * 
	 * @effect	the movementType of the unit is set to death
	 * 			|this.setMovementtype(MovementType.death)
	 * @effect 	if the unit was executing a task, this task needs to be interrupted.
	 * 			|if(this.isExecutingTask())
	 * 			|	this.getAssignedTask().interruptTask()
	 * @effect	if the unit was carrying a material, this is dropped.
	 * 			|if(this.isCarryingMaterial())
	 * 			|	this.dropMaterial()
	 * @effect 	if the unit was being followed, this unit must stop following.
	 * 			|if(this.isBeiingFollowed)
	 * 			|	this.unitFollowingMe.stopFollowingUnit();
	 * @effect	if the unit was following another unit, it must stop.
	 * 			|if(this.isFollowing)
	 * 			|	this.stopFollowingUnit();
	 * @effect	the unit is removed from it's faction.
	 * 			|this.getFaction().removeUnitFromFaction(this);
	 * @effect	the unit is removed from the set in World.
	 * 			|World.getInstance().removeUnitFromSet(this)
	 * @effect 	the unit is removed from the cube
	 * 			|World.getInstance().getCubeByPosition(currentPosition).removeUnitFromCubeSet(this)
	 * 
	 * @throws WorldException
	 * @throws UnitException
	 * @throws GameObjectException 
	 * @throws TaskException 
	 * @throws ExpressionException 
	 * @throws StatementException 
	 * @throws StatementBreakException 
	 * @throws IllegalStateException 
	 */
	public void die() throws WorldException, UnitException, GameObjectException, TaskException, StatementException, ExpressionException{
		this.setMovementType(MovementType.death);
		
		if(this.isExecutingTask())
			this.getAssignedTask().interruptTask();
		if(this.isCarryingMaterial())
			this.dropMaterial();
		if (this.isBeiingFollowed)
			this.unitFollowingMe.stopFollowingUnit();
		if (this.isFollowing)
			this.stopFollowingUnit();
		World.getInstance().removeUnitFromSet(this);
		this.getFaction().removeUnitFromFaction(this);
		World.getInstance().getCubeByPosition(currentPosition).removeUnitFromCubeSet(this);		
		this.isAlive();
	}
	
	// FALLING
	
	/**
	 * A method to initiate falling. 
	 * @effect Sets the movementtype of a unit to falling.
	 * 			|this.setMovementType(MovementType.falling)
	 */
	private void fall(){
		this.setMovementType(MovementType.falling);
	}
	
	/**
	 * Initiates falling if the unit has no solid cube in his direct neighbours.
	 * @effect if nog solid cube is in his direct neighbourhood, the unit will fall.
	 * 			|if ((!solidCubeAround((int)this.getGameObjectPositionXGameworld(),(int)this.getGameObjectPositionYGameworld(), (int) this.getGameObjectPositionZGameworld()) )&&(this.getGameObjectPositionZGameworld()!=0)){
	 *			|	this.fall();
	 * @throws UnitException
	 * @throws IllegalPositionException
	 */
	private void isStable() throws UnitException, IllegalPositionException{
		if ((!solidCubeAround((int)this.getGameObjectPositionXGameworld(),(int)this.getGameObjectPositionYGameworld(), (int) this.getGameObjectPositionZGameworld()) )&&(this.getGameObjectPositionZGameworld()!=0)){
			this.fall();
			}	
	}

	/**
	 * This method checks whether there is a solid cube that is a neighbour of the current Cube.
	 * @param 	Xc
	 * 			X coordinate of the middle cube.
	 * @param 	Yc
	 * 			y coordinate of the middle cube.
	 * @param 	Zc
	 * 			Z coordinate of the middle cube.
	 * @return true if there is a solid cube neighbouring to the current Cube
	 * @throws IllegalPositionException
	 */
	private boolean solidCubeAround(int Xc, int Yc, int Zc) throws IllegalPositionException{
		Cube currentCube = World.getInstance().getCubeByPosition(Xc, Yc, Zc);
		for (Cube neighbour: World.getInstance().getNeighbouringCubes(currentCube))
			if (neighbour.isSolidCube())
				return true;
		return false;
		
	}
	
	/**
	 * A method that checks whether the unit is carrying a boulder or a log.
	 * @return true if the unit is carrying a boulder or a log.
	 * 			|if(this.material != null)
	 * 			|	return true;
	 * 			|else return false;
	 */
	public boolean isCarryingMaterial(){
		if (this.material != null)
			return true;
		else return false;
	}
	
	/**
	 * A method that checks whether the unit is carrying a boulder.
	 * @return true if the unit is carrying a boulder.
	 * 			|if(this.getBoulder() != null)
	 * 			|	return true;
	 * 			|else return false;
	 */
	public boolean isCarryingBoulder(){
		if (this.getBoulder() != null){
			return true;
		}
		else return false;
	}
	
	/**
	 * A method that checks whether the unit is carrying a log.
	 * @return true if the unit is carrying a log.
	 * 			|if(this.getLog() != null)
	 * 			|	return true;
	 * 			|else return false;
	 */
	public boolean isCarryingLog(){
		if (this.getLog() != null)
			return true;
		else return false;
	}
		
	/**
	 * The material that is being carried by the unit, log or boulder.
	 */
	private Material material = null;
	
	/**
	 * The unit shall carry the given material.
	 * @param materialToBeCarried
	 * 		the material that needs to be carried by the unit.
	 * @effect if the unit was carrying a boulder and tries to pick up a log, the boulder is dropped.
	 * 		| if (materialToBeCarried instanceof Log) && (this.isCarryingBoulder())
	 * 		|		this.getBoulder().dropBoulder(this, this.getCurrentCube());
	 * @effect if the unit was carrying a log and tries to pick up a boulder, the log is dropped.
	 * 		| if (materialToBeCarried instanceof Boulder) && (this.isCarryingLog())
	 * 		|		this.getLog().dropLog(this, this.getCurrentCube());
	 * @post the material that the unit is carrying will be the given material.
	 * 		| new.material == materialToBeCarried
	 * @throws UnitException
	 * @throws GameObjectException
	 * @throws WorldException
	 */
	public void carryMaterial(Material materialToBeCarried) throws UnitException, GameObjectException, WorldException{
		if(materialToBeCarried instanceof Log){
			if (this.isCarryingBoulder())
				this.getBoulder().drop(this, this.getCurrentCube());
		}
		else if (materialToBeCarried instanceof Boulder){
			if(this.isCarryingLog())
				this.getLog().drop(this, this.getCurrentCube());
		}
		material = materialToBeCarried;	
		
	}
	
	/**
	 * The unit will drop the material that is was carrying.
	 * @effect if the unit was carrying a boulder, the boulder is dropped.
	 * 		| if (this.material instanceof Boulder)
	 * 		|	((Boulder) this.material).dropBoulder(this, this.getCurrentCube());
	 * @effect if the unit was carrying a log, the log is dropped.
	 * 		| if (this.material instanceof Log)
	 * 		|	((Log)this.material).dropLog(this, this.getCurrentCube());
	 * @throws UnitException
	 * 		if the unit was not carrying any material, throw new UnitException("The unit is not carrying material at the moment.")
	 * 		| if (material == null)
	 * 		|	throw new UnitException("The unit is not carrying material at the moment.")
	 * @throws GameObjectException
	 * @throws WorldException
	 */
	public void dropMaterial() throws UnitException, GameObjectException, WorldException{
		if(material == null)
			throw new UnitException("The unit is not carrying material at the moment.");
		else{
			if(this.material instanceof Boulder)
				((Boulder)this.material).drop(this, this.getCurrentCube());
			else if (this.material instanceof Log)
				((Log)this.material).drop(this, this.getCurrentCube());
		}
	}
	
	/**
	 * Returns the Log that is being carried by the Unit.
	 * @return
	 */
	@Basic @Raw
	public Log getLog(){
		if(this.material == null)
			return null;
		else if(this.material instanceof Log)
			return (Log)this.material;
		else return null;
	}
	
	/**
	 * Returns the boulder that is being carried by the Unit.
	 * @return
	 */
	@Basic @Raw
	public Boulder getBoulder(){
		if(this.material == null)
			return null;
		if(this.material instanceof Boulder)
			return (Boulder)this.material;
		else return null;
	}
	
	/**
	 * the weight of the material that is being carried.
	 */
	public int materialWeight;
	
	/**
	 * Sets the weight of the material that is being carried.
	 * @post the materialWeight is being set to the given weight.
	 * 		| new.getWeightMaterial() == materialWeight
	 * @param materialWeight
	 * 		the weight of the material being carried by the unit.
	 */
	@Raw
	public void setWeightMaterial(int materialWeight){
		this.materialWeight = materialWeight;
	}
	
	@Basic @Raw
	public int getWeightMaterial(){
		return this.materialWeight;
	}
	
	/**
	 * Returns the total weight of the unit + the weight of the object that the unit is carrying.
	 * @return
	 */
	@Basic @Raw
	public int getTotalWeight(){
		return(this.getWeight()+this.materialWeight);
	}

	/**
	 * This method calculates the temporary position while falling.
	 * @param GameTime
	 * 		the gametime that needs to be added.
	 * @param speed
	 * 		the falling speed.
	 * @throws GameObjectException
	 * @throws UnitException
	 * @throws WorldException
	 * @effect decreases the hitpoints with 10 for every z level the unit falls
	 * 			|if(Util.fuzzyEquals(z,this.targetPosition_z,0.05)
	 * 			|	this.decreaseHitpoints(10)
	 * @effect if the unit has reached the targetPosition, the movementType is updated.
	 * 			|if (this.defaultBehaviourEnabled == true)
	 *			|		this.setMovementType(MovementType.default_behaviour);
	 *			|	else
	 *			|		this.setMovementType(MovementType.nothing);
	 * @effect the position of the unit is updated.
	 * 			|this.setPosition(this.getPositionXGameWorld()+World.LENGTH_CUBE/2, this.getPositionYGameworld()+World.LENGTH_CUBE/2, z) 	
	 */
	@Override
	public void calculateTemporaryFallingPosition(double GameTime, double speed) throws GameObjectException, WorldException{
			
			double v_z = speed;			
			double z = this.getPositionGameObjectZ() + v_z * GameTime;			 

			 if(Util.fuzzyEquals(z,this.targetPosition_z,0.05)) {
				   	z = this.targetPosition_z;
				   	falling = false;
				   	this.decreaseHitpoints(10);
					   
					if (this.defaultBehaviourEnabled == true)
						this.setMovementType(MovementType.default_behaviour);
					else
						this.setMovementType(MovementType.nothing);
				}
			 this.setObjectPosition(this.getGameObjectPositionXGameworld()+World.LENGTH_CUBE/2,
					 				this.getGameObjectPositionYGameworld()+World.LENGTH_CUBE/2,
					 				z);
	}
	
	/**
	 * the task that the unit is executing.
	 */
	public Task assignedTask = null;
	
	/**
	 * Assigns a task to the unit.
	 * @param task
	 * 		the task that needs to be assigned.
	 * @post the task is assigned to the unit.
	 * 		|assignedTask = task
	 */
	public void assignTask(Task task){
		assignedTask = task;
	}
	
	/**
	 * Deassigns a task to the unit.
	 * @post the unit has no task to execute.
	 * 		|assignedTask == null
	 */
	@Raw
	public void deAssignTask(){
		assignedTask = null;
	}
	
	/**
	 * Returns the assigned Task.
	 * @return
	 */
	@Basic @Raw
	public Task getAssignedTask(){
		return assignedTask;
	}
	
	/**
	 * A method that checks whether the unit is currently executing a task
	 * @return true if the unit is executing a task.
	 * 		|if(assignedTask == null)
	 * 		|	return false;
	 * 		|else return true;
	 */
	public boolean isExecutingTask(){
		if (assignedTask == null)
			return false;
		else
			return true;
	}

	/**
	 * Boolean to indicate whether a unit is following another unit. 
	 */
	public boolean isFollowing = false;
	
	/**
	 * Boolean to indicate whether a unit is beiing followed by another unit.
	 */
	boolean isBeiingFollowed = false;
	
	/**
	 * The unit that this unit is following, if it is following one. If this unit is not following a unit
	 * the variable equals null.
	 */
	Unit unitToFollow = null;
	
	/**
	 * The unit that is following this unit, if it is beiing followed. If this unit is not beiing followed by 
	 * another unit, the variable equals null.
	 */
	Unit unitFollowingMe = null;

	/**
	 * Method to make a unit follow another unit.�
	 * @param F
	 * 		The unit that must be followed
	 * @post the unit to follow is set
	 * 			|this.unitToFollow == F
	 * @post ...
	 * 			|F.unitFollowingMe == this
	 * @post ...
	 * 			|isFollowing == true
	 * @post ...
	 * 			|F.isBeiingFollowed == true
	 * @effect the unit starts walking to the position of the unit to follow.
	 * 			|this.moveTo(this.unitToFollow.getPositionGameObjectX(), 
	 *				this.unitToFollow.getPositionGameObjectY(),
	 *				this.unitToFollow.getPositionGameObjectZ());
	
	 * @throws IllegalPositionException 
	 * @throws UnitException 
	 * @throws ExpressionException 
	 * @throws StatementException 
	 */
	private void startFollowingUnit(Unit F) throws StatementException, ExpressionException{
		if(F != null){
		isFollowing = true;
		F.isBeiingFollowed = true;
		unitToFollow = F;
		F.unitFollowingMe = this;
		
		try {
			this.moveTo(this.unitToFollow.getPositionGameObjectX(), 
					this.unitToFollow.getPositionGameObjectY(),
					this.unitToFollow.getPositionGameObjectZ());
		} catch (UnitException e) {
			e.printStackTrace();
		} catch (IllegalPositionException e) {
			e.printStackTrace();
		}}else this.currentStatement.finishStatement();
	}

	/**
	 * Method to make a unit stop following another unit.
	 * @post the unit is not following another unit
	 * 			|isFollowing == false;
	 * 			|unitToFollow.unitFollowingMe == null;
	 * 			|unitToFollow.isBeiingFollowed == false;
	 * 			|unitToFollow == null;
	 * @effect if the unit was executing an activity from a task, the activity is finished.
	 * 			|if(this.currentStatement != null && this.currentStatement instanceof FollowStatement)
	 * 			|	this.currentStatement.finishStatement();
	 * @effect the movementType is set to nothing
	 * 			|this.centreUnit(MovementType.nothing)
	 * @throws UnitException
	 * @throws TaskException 
	 * @throws ExpressionException 
	 * @throws StatementException 
	 * @throws StatementBreakException 
	 * @throws IllegalStateException 
	 */
	private void stopFollowingUnit() throws UnitException, TaskException, StatementException, ExpressionException{
		isFollowing = false;
		unitToFollow.isBeiingFollowed= false;
		unitToFollow.unitFollowingMe = null;
		unitToFollow = null;
		
		if (this.currentStatement != null){
			this.centreUnit(MovementType.nothing);
			if(this.currentStatement instanceof FollowStatement){
				this.currentStatement.finishStatement();
				
			}
		}else
			this.centreUnit(MovementType.nothing);
	}
	
	/**
	 * Makes a unit walk to the unit it is following and stop following that unit when it reached that Unit 
	 * or a neighbouring cube of that Unit.
	 * @effect if the unitToFollow is not null and the unit is not neighbouring the unit to follow, the targetPosition is set to the unit's currentPosition.
	 * 			|if(unitToFollow != null && !World.getInstance().getNeighbouringCubes(this.unitToFollow.getCurrentCube()).contains(this.getCurrentCube())
	 * 			|	this.setTargetPosition(this.unitToFollow.getPositionGameObjectX(),
	 *			|					this.unitToFollow.getPositionGameObjectY(), 
     *		 	|						this.unitToFollow.getPositionGameObjectZ())
	 * @effect if the unit is neighbouring the unitToFollow, stop follow this unit
	 * 			|if (World.getInstance().getNeighbouringCubes(this.unitToFollow.getCurrentCube()).contains(this.getCurrentCube()) ||
	 *			|	(this.unitToFollow.getCurrentCube() == this.getCurrentCube()))
	 *			|		this.stopFollowingUnit()
	 * @throws UnitException
	 * 		if(unitToFollow == null)  throw new UnitException("No unit found to follow.")
	 * 
	 * @throws IllegalPositionException
	 * @throws TaskException 
	 * @throws ExpressionException 
	 * @throws StatementException 
	 * @throws StatementBreakException 
	 * @throws IllegalStateException 
	 */
	private void followUnit() throws UnitException, IllegalPositionException, TaskException, StatementException, ExpressionException{
		
		if (this.unitToFollow == null)
			throw new UnitException("No unit found to follow");
		else if (World.getInstance().getNeighbouringCubes(this.unitToFollow.getCurrentCube()).contains(this.getCurrentCube()) ||
				(this.unitToFollow.getCurrentCube() == this.getCurrentCube()))
			this.stopFollowingUnit();
		else
			this.setTargetPosition(	this.unitToFollow.getPositionGameObjectX(),
									this.unitToFollow.getPositionGameObjectY(), 
									this.unitToFollow.getPositionGameObjectZ());
			
	}

	/**
	 * A method to find and return the closest log if there is one.
	 * @return the closestLog available in the gameWorld if there is one.
	 * @throws UnitException
	 * @throws TaskException
	 */
	@Raw
	public Log getClosestLog() throws UnitException, TaskException {
		Log closestLog = null;
		double distance = Integer.MAX_VALUE;
		for (Log log : World.getInstance().logSet){
			if (closestLog == null)
				closestLog = log;
			else if (closestLog.getPositionGameObject().distanceBetweenTwoPoints(log.getPositionGameObject()) < distance){
				closestLog = log;
				distance = closestLog.getPositionGameObject().distanceBetweenTwoPoints(log.getPositionGameObject());
			}
		}
		if(closestLog != null) return closestLog;
		else {
			this.currentStatement = null;
			if(this.getAssignedTask() != null)
				this.getAssignedTask().interruptTask();
			throw new UnitException("No logs available.");	
		}
	}
	
	/**
	 * A method to find and return the closest boulder if there is one.
	 * @return the closest boulder available in the gameWorld if there is one.
	 * @throws UnitException
	 * @throws TaskException
	 */
	@Raw
	public Boulder getClosestBoulder() throws UnitException, TaskException{
		
		Boulder closestBoulder = null;
		double distance = Integer.MAX_VALUE;
		for (Boulder boulder : World.getInstance().boulderSet){
			if (closestBoulder == null)
				closestBoulder = boulder;
			else if (closestBoulder.getPositionGameObject().distanceBetweenTwoPoints(boulder.getPositionGameObject()) < distance){
				closestBoulder = boulder;
				distance = closestBoulder.getPositionGameObject().distanceBetweenTwoPoints(boulder.getPositionGameObject());
			}	
		}
		if(closestBoulder != null) return closestBoulder;
		else {
			this.currentStatement = null;
			if(this.getAssignedTask() != null)
				this.getAssignedTask().interruptTask();
			throw new UnitException("No boulders availble.");
		}
	}
	
	/**
	 * A method to find and return the closest workshop if there is one.
	 * @return the closest workshop available in the gameWorld if there is one.
	 * @throws TaskException
	 * @throws UnitException
	 */
	@Raw
	public Cube getClosestWorkshop() throws TaskException, UnitException{
		Cube closestWorkshop = null;
		double distance = Integer.MAX_VALUE;
		for(Cube workshop : World.getInstance().workshopSet){
			if (closestWorkshop == null)
				closestWorkshop = workshop;
			else if (closestWorkshop.getPosition().distanceBetweenTwoPoints(workshop.getPosition()) < distance){
				closestWorkshop = workshop;
				distance = closestWorkshop.getPosition().distanceBetweenTwoPoints(workshop.getPosition());
			}
		}
		if(closestWorkshop != null) return closestWorkshop;
		else{
			this.currentStatement = null;
			if(this.getAssignedTask() != null)
				this.getAssignedTask().interruptTask();
			throw new UnitException("No workshops available.");
		}
	}

	/**
	 * A method to find and return the closest friendly unit if there is one.
	 * @return the closest friendly unit available in the gameWorld if there is one.
	 * @throws TaskException
	 * @throws UnitException
	 */
	@Raw
	public Unit getFriendlyUnit() throws TaskException, UnitException {
		Unit closestFriend = null;
		double distance = Integer.MAX_VALUE;
		for(Unit friend : World.getInstance().unitSet)
			if (friend.getFaction() == this.getFaction() && !friend.equals(this)){
				if(closestFriend == null)
					closestFriend = friend;
				else if (closestFriend.getPositionGameObject().distanceBetweenTwoPoints(friend.getPositionGameObject()) < distance){
					closestFriend = friend;
					distance = closestFriend.getPositionGameObject().distanceBetweenTwoPoints(friend.getPositionGameObject());
				}
			}
			if(closestFriend != null) return closestFriend;
			else{
				this.currentStatement = null;
				if(this.getAssignedTask() != null)
					this.getAssignedTask().interruptTask();
				throw new UnitException("No friends available.");
			}
	}

	/**
	 * A method to find and return the closest enemy unit if there is one.
	 * @return the closest enemy unit available in the gameWorld if there is one.
	 * @throws TaskException
	 * @throws UnitException
	 */
	@Raw
	public Unit getEnemy() throws TaskException, UnitException {
		Unit closestUnit = null;
		double distance = Integer.MAX_VALUE;
		for (Unit enemy : World.getInstance().unitSet)
			if (enemy.getFaction() != this.getFaction()){
				if(closestUnit == null)
					closestUnit = enemy;
				else if (closestUnit.getPositionGameObject().distanceBetweenTwoPoints(enemy.getPositionGameObject()) < distance){
					closestUnit = enemy;
					distance = closestUnit.getPositionGameObject().distanceBetweenTwoPoints(enemy.getPositionGameObject());
				}
			}
			if(closestUnit != null) return closestUnit;
			else{
				this.currentStatement = null;
				if(this.getAssignedTask() != null)
					this.getAssignedTask().interruptTask();
				throw new UnitException("No enemies available.");
			}
	}

	/**
	 * A method to find and return the closest unit if there is one.
	 * @return the closest unit available in the gameWorld if there is one.
	 * @throws TaskException
	 * @throws UnitException
	 */
	@Raw
	public Unit getAnyUnit() throws TaskException, UnitException {
		Unit closestUnit = null;
		double distance = Integer.MAX_VALUE;
		for(Unit unit : World.getInstance().unitSet){
			if(closestUnit == null)
				closestUnit = unit;
			else if (closestUnit.getPositionGameObject().distanceBetweenTwoPoints(unit.getPositionGameObject()) < distance){
				closestUnit = unit;
				distance = closestUnit.getPositionGameObject().distanceBetweenTwoPoints(unit.getPositionGameObject());
			}
		}
		if(closestUnit!= null) return closestUnit;
		else{
			this.currentStatement = null;
			if(this.getAssignedTask() != null)
				this.getAssignedTask().interruptTask();
			throw new UnitException("No Units available");
		}
			
	}
	
	/**
	 * Contains the statement that is currently being executed by the unit.
	 */
	public Statement currentStatement = null;
	
	/**
	 * A method to initiate moving to the location that comes from a statement.
	 * @param x
	 * 		the x coordinate of the position to walk to.
	 * @param y
	 * 		the y coordinate of the position to walk to.
	 * @param z
	 * 		the z coordinate of the position to walk to.
	 * @param thisStatement
	 * 		the statement that is initiating the movement.
	 * @effect the unit starts walking to the given position
	 * 		|this.moveTo(x + World.LENGTH_CUBE/2, y + World.LENGTH_CUBE/2, z + World.LENGTH_CUBE/2)
	 * @post the currentStatement is set to the given statement
	 * 		|currentStatement == thisStatement
	 * @throws UnitException
	 * @throws IllegalPositionException
	 */
	public void moveTo(double x, double y, double z, Statement thisStatement) throws UnitException, IllegalPositionException{
		
		currentStatement = thisStatement;
		this.moveTo(x + World.LENGTH_CUBE/2, y + World.LENGTH_CUBE/2, z + World.LENGTH_CUBE/2);
	}
	
	/**
	 * A method to initiate working at the location that comes from a statement.
	 * @param x
	 * 		the x coordinate of the position to work at.
	 * @param y
	 * 		the y coordinate of the position to work at.
	 * @param z
	 * 		the z coordinate of the position to work at.
	 * @param thisStatement
	 * 		the statement that is initiating the working.
	 * @effect the unit starts working at the given position.
	 * 		|this.workAt(x,y,z)
	 * @post the currentStatement is set to the given statement
	 * 		|currentStatement == thisStatement

	 * @throws UnitException
	 */
	public void workAt(int x, int y, int z,Statement thisStatement) throws UnitException{


		currentStatement = thisStatement;
		this.workAt(x, y, z);
	}
	
	/**
	 * A method to initiate an attack on the unit given as parameter that comes from a statement.
	 * @param D
	 * 		the unit to attack.
	 * @param thisStatement
	 * 		the statement that is initiating the attack.
	 * @effect the unit given as parameter is being attacked.
	 * 		|this.attack(D)
	 * @post the currentStatement is set to the given statement
	 * 		|currentStatement == thisStatement
	 */
	public void attack(Unit D, Statement thisStatement){
		currentStatement = thisStatement;
		this.attack(D);
	}
	
	/**
	 * A method to initiate following a unit given as parameter that comes from a statement.
	 * @param F
	 * 		the unit to follow
	 * @param thisStatement
	 * 		the statement that intitiates the following.
	 * @throws ExpressionException 
	 * @throws StatementException 
	 * @throws UnitException 
	 * @effect the unit start following the unit given as parameter.
	 * 		|this.startFollowingUnit(F)
	 * @post the currentStatement is set to the given statement
	 * 		|currentStatement == thisStatement
	 */
	public void startFollowingUnit(Unit F, Statement thisStatement) throws StatementException, ExpressionException{
		currentStatement = thisStatement;
		this.startFollowingUnit(F);
	}
	
	/**
	 * A method to interrupt the task that is being executed by the unit.
	 * @effect the task is interrupted.
	 * 		|this.getAssignedTask().interruptTask()
	 * @effect the unit is deassigned from the task.
	 * 		|this.deAssignTask()
	 * @effect the statement currently executed by the unit is interrupted.
	 * 		|this.currentStatement.interruptExecution()
	 * @throws TaskException
	 */
	public void interruptTask() throws TaskException{
		this.getAssignedTask().interruptTask();
		this.deAssignTask();
		this.currentStatement.interruptExecution();
	}	
}
