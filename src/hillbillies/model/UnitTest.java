package hillbillies.model;

import static org.junit.Assert.*;

import java.lang.reflect.InvocationTargetException;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import hillbillies.exceptions.GameObjectException;
import hillbillies.exceptions.IllegalPositionException;
import hillbillies.exceptions.SchedulerException;
import hillbillies.exceptions.TaskException;
import hillbillies.exceptions.UnitException;
import hillbillies.exceptions.WorldException;
import hillbillies.part2.listener.DefaultTerrainChangeListener;
import programmingLanguage.ExpressionException;
import programmingLanguage.StatementException;

public class UnitTest {
	World world;
	static int[][][] cubeType = new int[10][10][10];
	DefaultTerrainChangeListener listener = new DefaultTerrainChangeListener();
	
	Unit testUnit;
	Unit testUnit2;
	Unit testUnit3;
	Unit testUnitCarrier;
	Unit testUnit4;
	Unit testUnit5;
	Unit testUnit8;

	Boulder boulder, boulder2;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		for (int x = 0; x<10; x++)
			for(int y = 0; y<10; y++)
				for(int z = 0; z<10; z++)
					cubeType[x][y][z] = 0;
	}

	@Before
	public void setUp() throws Exception {
		world = World.getInstance();
		World.getInstance().setTerrainChangeListener(listener);
		World.getInstance().setBoundaries(10, 10, 10);
		World.getInstance().setConnectedToBorder(World.MAXPOSITION_X, World.MAXPOSITION_Y, World.MAXPOSITION_Z);
		World.getInstance().setCubes(cubeType);
		World.getInstance().getCubeByPosition(5, 5, 0).setTerrainType(2);
		World.getInstance().unitSet.clear();
		
		int [] PosArray = {1,1,0};
		int [] PosArray2 = {1,2,0};
		int [] PosArray3 = {9,9,0};
		int [] PosArray4 = {5,5,1}; // standing on solid cube
		double [] PosArrayBoulder = {1.5,1.5,0.5};
		
		testUnit = new Unit("James O'Hara", PosArray , 50,50,50,50, false);
		
		testUnit2 = new Unit ("Anton", PosArray2, 50,50,50,50, false);
		testUnit3 = new Unit ("Jasper", PosArray3, 50,50,50,50, false);
		testUnitCarrier = new Unit("James O'Hara",PosArray,50,50,50,50, false);
		boulder = new Boulder(PosArrayBoulder);
		boulder.pickUp(testUnitCarrier);
		boulder2 = new Boulder(PosArrayBoulder);
		testUnit4 = new Unit ("Anton", PosArray4, 50,50,50,50, false);
		testUnit5 = new Unit ("Jasper", PosArray3, 50,50,50,50, false);
		testUnit8 = new Unit ("Anton", PosArray2, 50,50,50,50, false);
		
	}

	//POSITION//
	
	@Test
	public void PositionX_LegalCoordinate(){
		assertTrue(World.isValidPosition(testUnit.getPositionGameObjectX(), testUnit.getMaxPosition_X()));
	}
	
	@Test 
	public void PositionX_NegativeCoordinate() throws IllegalPositionException{
		assertFalse(World.isValidPosition(-2, testUnit.getMaxPosition_X()));
	}
	
	@Test
	public void PositionX_OutOfBounds(){
		assertFalse(World.isValidPosition(51, testUnit.getMaxPosition_X()));
	}
	
	//// NAME ////
	
	@Test
	public void Name_Valid(){
		assertTrue(testUnit.isValidName("James O'Hara"));
	}
	
	@Test
	public void Name_NoUppercase(){
		assertFalse(testUnit.isValidName("james O'Hara"));
	}
	
	@Test
	public void Name_1Char(){
		assertFalse(testUnit.isValidName("X"));
	}
	
	@Test
	public void Name_Null(){
		assertFalse(testUnit.isValidName(""));
	}
	
	@Test
	public void Name_InvalidChar(){
		assertFalse(testUnit.isValidName("Xx?"));
	}
	
	//// POSITION SETTERS ////
	
	@Test
	public void setPositionX_legal() throws WorldException, GameObjectException{
		testUnit.setObjectPosition(9,9,9);
		assertEquals(9, testUnit.getPositionGameObjectX(), 0.01);
	}
	//idem for y and z
	
	@Test(expected=GameObjectException.class)
	public void setPositionX_negative() throws WorldException, GameObjectException{
		testUnit.setObjectPosition(-5,20,20);
	}
	//idem for y and z

	
	@Test(expected=GameObjectException.class)
	public void setPositionX_tooLarge() throws WorldException, GameObjectException{
		testUnit.setObjectPosition(800,20,20);
	}
	//idem for y and z

	@Test
	public void setTargetPositionX_legal() throws UnitException{
		testUnit.setTargetPosition(5,9,8);
		assertEquals(5, testUnit.getTargetPositionX(), 0.01);
	}
	//idem for y and z
	
	@Test(expected=UnitException.class)
	public void setTargetPositionX_negative()throws UnitException{
		testUnit.setTargetPosition(-5,20,20);
	}
	//idem for y and z
	
	@Test(expected=UnitException.class)
	public void setTargetPositionX_tooLarge()throws UnitException{
		testUnit.setTargetPosition(1000,20,20);	
	}
	//idem for y and z
	
	@Test
	public void setTargetCubeX_legal() throws UnitException{
		testUnit.setTargetCube(9,9,9);
		assertEquals(9, testUnit.targetCube.position_x, 0.01);
	}
	//idem for y and z
	
	@Test(expected=UnitException.class)
	public void setTargetCubeX_negative() throws UnitException{
		testUnit.setTargetCube(-5,20,20);
	}
	//idem for y and z
	
	@Test(expected=UnitException.class)
	public void setTargetCubeX_tooLarge() throws UnitException{
		testUnit.setTargetCube(testUnit.getMaxPosition_X() + 10,20,20);
	}
	//idem for y and z
	
	
	//// ATTRIBUTES ////
	
	@Test
	public void StrengthInitial_Legal(){
		assertTrue(testUnit.isValidInitialValue(25));
	}
	
	@Test 
	public void StrengthInitial_TooSmall(){
		assertFalse(testUnit.isValidInitialValue(24));
	}
	
	@Test
	public void StrengthInitial_TooBig(){
		assertFalse(testUnit.isValidInitialValue(101));
	}
	
	@Test
	public void Strength_Legal(){
		assertTrue(testUnit.isValidValue(150));
	}
	
	@Test
	public void Strength_Negative(){
		assertFalse(testUnit.isValidValue(-2));
	}
	
	@Test
	public void Strength_OutOfBounds(){
		assertFalse(testUnit.isValidValue(201));
	}
	
	@Test
	public void Weight_Legal(){
		assertTrue(testUnit.isValidWeight(50));
	}
	
	@Test
	public void Weight_TooSmall(){
		assertFalse(testUnit.isValidWeight(49));
	}
	
	@Test
	public void setWeight_Legal(){
		testUnit.setWeight(51);
		assertEquals(51, testUnit.getWeight());
	}
	
	@Test
	public void setWeight_Negative(){
		testUnit.setWeight(-51);
		assertEquals(51, testUnit.getWeight());
	}
	
	@Test
	public void setWeight_TooBig(){
		testUnit.setWeight(201);
		assertEquals(200, testUnit.getWeight());
	}
	
	//TODO vanwaar deze test?
	@Test
	public void setWeight_TooSmall(){
		testUnit.setWeight(49);
		assertEquals(50, testUnit.getWeight());
	}
	
	@Test
	public void setInitialWeight_Legal(){
		testUnit.setInitialWeight(51);
		assertEquals(51, testUnit.getWeight());
	}
	
	@Test
	public void setInitialWeight_TooBig(){
		testUnit.setInitialWeight(101);
		assertEquals(100, testUnit.getWeight());
	}
	
	@Test
	public void setInitialWeight_TooSmall(){
		testUnit.setInitialWeight(24);
		assertEquals(50, testUnit.getWeight());
	}
	
	@Test
	public void setStrenght_Legal(){
		testUnit.setStrength(51);
		assertEquals(51, testUnit.getStrength());
	}//analoog voor agility en toughness

	@Test
	public void setStrength_Zero(){
		testUnit.setStrength(0);
		assertEquals(1, testUnit.getStrength());
	}//analoog voor agility en toughness
	
	@Test
	public void setStrength_Negative(){
		testUnit.setStrength(-12);
		assertEquals(12, testUnit.getStrength());
	}//analoog voor agility en toughness
	
	@Test
	public void setStrenght_TooBig(){
		testUnit.setStrength(201);
		assertEquals(200, testUnit.getStrength());
	}//analoog voor agility en toughness
	
	@Test
	public void setInitialStrength_Legal(){
		testUnit.setInitialStrength(26);
		assertEquals(26, testUnit.getStrength());
	}//analoog voor agility en toughness
	
	@Test
	public void setInitialStrength_TooSmall(){
		testUnit.setInitialStrength(24);
		assertEquals(25, testUnit.getStrength());
	}//analoog voor agility en toughness
	
	@Test
	public void setInitialStrength_NegativeAndLegal(){
		testUnit.setInitialStrength(-25);
		assertEquals(25, testUnit.getStrength());
	}//analoog voor agility en toughness
	
	@Test
	public void setInitialStrength_NegativeAndTooSmall(){
		testUnit.setInitialStrength(-24);
		assertEquals(25, testUnit.getStrength());
	}//analoog voor agility en toughness
	
	@Test
	public void setInitialStrength_NegativeAndTooBig(){
		testUnit.setInitialStrength(-101);
		assertEquals(100, testUnit.getStrength());
	}//analoog voor agility en toughness
	
	
	//HITPOINTS AND STAMINAPOINTS//
	
	@Test
	public void Hitpoints_Legal(){
		assertTrue(testUnit.isValidHitpoint(30));
	}//analoog voor stamina.
	
	@Test
	public void Hitpoints_Negative(){
		assertFalse(testUnit.isValidHitpoint(-1));
	}//analoog voor stamina.
	
	@Test
	public void Hitpoints_OutOfBounds(){
		assertFalse(testUnit.isValidHitpoint(testUnit.getMaxHitpoints()+1));
	}//analoog voor stamina.
	
	@Test
	public void setHitpoints_Legal(){
		testUnit.setHitpoints(30);
		assertEquals(30, testUnit.getHitpoints());
	}//analoog voor stamina.
	
	@Test
	public void increaseHitpoints_Legal(){
		testUnit.setHitpoints(45);
		testUnit.increaseHitpoints(5);
		assertEquals(50, testUnit.getHitpoints());
	}//analoog voor stamina.
	
	@Test
	public void increaseHitpoints_TooLarge(){
		testUnit.setHitpoints(45);
		testUnit.increaseHitpoints(6);
		assertEquals(50, testUnit.getHitpoints());
	}//analoog voor stamina.
	
	@Test
	public void decreaseHitpoints_Legal(){
		testUnit.setHitpoints(5);
		testUnit.decreaseHitpoints(4);
		assertEquals(1, testUnit.getHitpoints());
	}//analoog voor stamina.
	
	@Test
	public void decreaseHitpoints_TooLarge(){
		testUnit.setHitpoints(5);
		testUnit.decreaseHitpoints(6);
		assertEquals(0, testUnit.getHitpoints());
	}//analoog voor stamina.
	
	
	//// MOVING ////
	
	@Test
	public void moveTo_MovementTypeToWalking() throws UnitException, IllegalPositionException{
		testUnit.moveTo(8,8,0);
		assertTrue(testUnit.getMovementType() == MovementType.walking);
	}
	
	@Test(expected=IllegalPositionException.class)
	public void moveTo_negativePosition() throws IllegalPositionException, UnitException{ 
		testUnit.moveTo(-4, 10, 10);
	}
	
	@Test(expected=IllegalPositionException.class)
	public void moveTo_PositionOutOfBoundaries() throws UnitException, IllegalPositionException{
		testUnit.moveTo(400, 10, 10);
	}

	@Test
	public void moveTo_targetPosition() throws UnitException, IllegalPositionException{
		testUnit.moveTo(5, 4, 0);
		assertEquals(5, testUnit.getTargetPositionX(),0.01);
		assertEquals(4, testUnit.getTargetPositionY(),0.01);
		assertEquals(0, testUnit.getTargetPositionZ(),0.01);
	}
	
	@Test
	public void moveTo_targetPosition000() throws UnitException, IllegalPositionException{
		testUnit.moveTo(0.5, 0.5, 0.5);
		assertEquals(0.5, testUnit.getTargetPositionX(),0.01);
		assertEquals(0.5, testUnit.getTargetPositionY(),0.01);
		assertEquals(0.5, testUnit.getTargetPositionZ(),0.01);
	}
	
	@Test
	public void moveToRandomPos()throws UnitException, IllegalPositionException, WorldException, GameObjectException, TaskException, ExpressionException, StatementException, SchedulerException{
		testUnit.moveToRandomPos();
		testUnit.advanceTime(0.2);
		assertTrue(testUnit.getMovementType() == MovementType.walking);
	}
	
	@Test
	public void Walk_CheckMovementType(){
		testUnit.Walk();
		assertEquals(MovementType.walking, testUnit.getMovementType());
	}
	
	@Test
	public void sprint_checkMovementType(){
		testUnit.Walk();
		testUnit.sprint();
		assertEquals(MovementType.sprinting, testUnit.getMovementType());
	}
	
	//checks that if a unit wants to sprint when it is not walking, the unit doesn't sprint.
	@Test
	public void sprint_mustBeInitialyWalking(){
		testUnit.setMovementType(MovementType.nothing);
		testUnit.sprint();
		assertFalse(MovementType.sprinting == testUnit.getMovementType());
	}

	@Test
	public void sprint_walkWhenExhausted() throws UnitException, WorldException, IllegalPositionException, GameObjectException, TaskException, StatementException, ExpressionException, SchedulerException{
		testUnit.setMovementType(MovementType.walking);
		testUnit.setStaminapoints(1);
		testUnit.moveTo(1, 3, 0);
		testUnit.sprint();
		testUnit.advanceTime(0.2);
		
		assertTrue(testUnit.getMovementType() == MovementType.walking);
	}
	
	@Test 
	public void sprint_decreaseStaminaBy1EveryPoint1second() throws UnitException, WorldException, IllegalPositionException, GameObjectException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, TaskException, ExpressionException, StatementException, SchedulerException{
		int staminaBefore = testUnit.getStaminapoints();
		testUnit.setTargetPosition(1, 2, 0);
		testUnit.setTargetCube(1, 2, 0);
		
		System.out.println(testUnit.getPositionGameObjectX());
		System.out.println(testUnit.getTargetPositionX());
		
		testUnit.Walk();
		testUnit.sprint();
		testUnit.advanceTime(0.1);
		int staminaAfter = testUnit.getStaminapoints();
		assertEquals(staminaBefore - (int)(0.1/0.1), staminaAfter);		
	}

	//// SPEED ////
	
	@Test
	public void getWalkingSpeed_TargetPosIsHigher() throws UnitException{
		testUnit.setTargetPosition(9,9,1.5);
		assertEquals(testUnit.getBaseSpeed()*0.5, testUnit.getWalkingSpeed(), 0.00001);
	}
	
	@Test
	public void getWalkingSpeed_TargetPosIsLower() throws UnitException{
		testUnit4.setTargetPosition(5,6,0.5);
		assertEquals(testUnit4.getBaseSpeed()*1.2, testUnit4.getWalkingSpeed(), 0.00001);
	}
	
	@Test
	public void getWalkingSpeed_Other() throws UnitException{
		testUnit.setTargetPosition(9,8,7);
		assertEquals(testUnit.getBaseSpeed(), testUnit.getWalkingSpeed(), 0.00001);
	}
	
	@Test
	public void GetSprintingSpeed() throws UnitException, WorldException, GameObjectException{
		testUnit.setObjectPosition(9, 8, 7);
		testUnit.setTargetPosition(2, 2, 2);
		assertEquals(testUnit.getWalkingSpeed()*2, testUnit.getSprintingSpeed(), 0.00001);
	}
	
	@Test
	public void centreUnit() throws UnitException, IllegalPositionException, WorldException, GameObjectException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, TaskException, ExpressionException, StatementException, SchedulerException{
		testUnit.moveTo(1, 3, 0);
		testUnit.advanceTime(0.1);
		testUnit.rest();
		testUnit.advanceTime(0.9);
		//assertEquals(testUnit.getMovementType(), MovementType.walking);
		testUnit.advanceTime(0.1);
		//assertEquals(testUnit.getMovementType(), MovementType.resting);
		assertTrue(testUnit.centeringUnit);
		
		
	}
	
	//// ATTACK ////
	
	@Test
	public void attack_beforeAttackIsFinished() throws StatementException, ExpressionException{
		testUnit.attack(testUnit2);
		testUnit.attack(0.5);
		assertEquals(MovementType.fighting, testUnit.getMovementType());
	}
	
	@Test
	public void attack_otherFaction(){
		testUnit.attack(testUnit2);
		assertFalse(testUnit.getFaction() == testUnit2.getFaction());
	}
	
	@Test
	public void attack_sameFaction(){
		Faction compareFaction = testUnit5.getFaction();
		Unit sameFactionUnit = null;
		
		if (testUnit.getFaction() == compareFaction)
			sameFactionUnit = testUnit;
		else if (testUnit2.getFaction() == compareFaction)
			sameFactionUnit = testUnit2;
		else if (testUnit3.getFaction() == compareFaction)
			sameFactionUnit = testUnit3;
		else if (testUnit4.getFaction() == compareFaction)
			sameFactionUnit = testUnit4;
		else if (testUnitCarrier.getFaction() == compareFaction)
			sameFactionUnit = testUnitCarrier;
		
		testUnit5.attack(sameFactionUnit);
		assertEquals(testUnit5.getMovementType(), MovementType.nothing);
		assertEquals(sameFactionUnit.getMovementType(), MovementType.nothing);
	}
	
	@Test
	public void attack_afterAttackIsFinished() throws StatementException, ExpressionException{
		testUnit.attack(testUnit2);
		double epsilon = 0.0000000000001;
		testUnit.attack(1 + epsilon);
		assertEquals(MovementType.nothing, testUnit.getMovementType());
	}
	
	//if the two units do not stand next to eachother, they cannot fight.
	@Test
	public void attack_isNotNeighbouring(){
		MovementType firstType = testUnit3.getMovementType();
		testUnit.attack(testUnit3);
		assertEquals(testUnit3.getMovementType(), firstType);
	}
	
	//if two units stand next to eachother, they can fight.
	// Walk when dodging.
	@Test
	public void attack_isNeighbouring(){
		testUnit.attack(testUnit8);
		assertTrue(	(testUnit8.getMovementType() == MovementType.fighting ) || 
					(testUnit8.getMovementType() == MovementType.walking) );
	}
	
	
	//// ORIENTATION ////
	
	@Test
	public void setOrientation_Regular(){
		double angle = Math.PI;
		testUnit.setOrientation((float)angle);
		assertEquals(Math.PI, testUnit.getOrientation(), 0.01);
	}
	
	@Test
	public void setOrientation_TooHigh(){
		double angle = Math.PI*3.5;
		testUnit.setOrientation((float)angle);
		assertEquals(1.5*Math.PI, testUnit.getOrientation(), 0.01);
	}
	
	@Test
	public void setOrientation_TooLow(){
		double angle = -5.5*Math.PI;
		testUnit.setOrientation((float)angle);
		assertEquals(0.5*Math.PI, testUnit.getOrientation(), 0.01);
	}
	
	@Test
	public void setOrientation_2PI(){
		double angle = 2* Math.PI;
		testUnit.setOrientation((float)angle);
		assertEquals(0, testUnit.getOrientation(), 0.01);
	}
	
	@Test
	public void setOrientation_zero(){
		double angle = 0;
		testUnit.setOrientation((float)angle);
		assertEquals(0, testUnit.getOrientation(), 0.01);
	}
	
	
	//// WORK ////
	
	@Test
	public void workAt_MovementType_OwnPosition() throws IllegalPositionException, UnitException{
		testUnit.workAt(1, 1, 0);
		assertEquals(MovementType.working, testUnit.getMovementType());
	}
	
	@Test
	public void workAt_MovementType_NeighbouringPosition() throws UnitException{
		testUnit.workAt(1, 2, 0);
		assertEquals(MovementType.working, testUnit.getMovementType());
	}
	
	@Test (expected = UnitException.class)
	public void workAt_MovementType_InvalidWorkLocation() throws UnitException{
		testUnit.workAt(9, 9, 0);
	}
	
	@Test
	public void work_duration() throws WorldException, GameObjectException, UnitException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IllegalPositionException, TaskException, StatementException, ExpressionException{
		testUnit.work(0.2);
		assertEquals(10 ,testUnit.duration_work,0.001);
	}
	
	@Test
	public void work_whenFinishedDoNothing() throws WorldException, GameObjectException, UnitException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IllegalPositionException, TaskException, StatementException, ExpressionException{
		int [] PosArray = {1,1,0};
		Unit testUnit6 = new Unit ("Anton", PosArray, 50,50,50,50, false);
		testUnit6.workAt(1, 1, 0);
		testUnit6.work(testUnit6.duration_work + 20);
		assertTrue(testUnit6.getMovementType() == MovementType.nothing);
	}

	//// RESTING ////
	@Test
	public void resting_NotFullPoints() throws UnitException{
		testUnit.decreaseHitpoints(5);
		testUnit.rest();
		assertEquals(testUnit.getMovementType(), MovementType.resting);
	}
	
	// it takes 0.8 seconds to restore a hitpoint with the given values.
	@Test
	public void resting_TimeHitpoints() throws UnitException{
		testUnit.decreaseHitpoints(5);
		double points = testUnit.getHitpoints();
		testUnit.rest();
		testUnit.rest(0.8);
		assertEquals(points + 1, testUnit.getHitpoints(), 0.00001);
	}
	
	//it taks 0.4 seconds to recover 1 staminapoints with the given values.
	@Test
	public void resting_TimeStaminapoints() throws UnitException{
		testUnit.decreaseStaminapoints(5);
		double points = testUnit.getStaminapoints();
		testUnit.rest();
		testUnit.rest(0.4);
		assertEquals(points + 1, testUnit.getStaminapoints(), 0.00001);
	}
	
	//if stamina and hitpoints are not full, resting will always restore the hitpoints first.
	@Test
	public void resting_HitpointsBeforeStamina() throws UnitException{
		testUnit.decreaseHitpoints(5);
		testUnit.decreaseStaminapoints(5);
		double points = testUnit.getHitpoints();
		testUnit.rest();
		testUnit.rest(0.8);
		assertEquals(points+1, testUnit.getHitpoints(), 0.00001);
	}
	
	//a Unit must stop resting if the hit- and staminapoints are full.
	@Test
	public void resting_MovementTypeWhenFull() throws UnitException{
		testUnit.decreaseHitpoints(1);
		testUnit.rest();
		testUnit.rest(1);
		assertEquals(testUnit.getMovementType(), MovementType.nothing);
	}
	
	//must at least rest untill 1 point is recovered.
	@Test
	public void resting_UntillFirstPointAdded() throws IllegalPositionException, UnitException{
		testUnit.decreaseHitpoints(5);
		testUnit.rest();
		testUnit.rest(0.2);
		testUnit.workAt(1,1,0);
		assertEquals(testUnit.getMovementType(), MovementType.resting);
	}
	
	//// DEFAULT BEHAVIOUR ////
	
	@Test
	public void defaultBehaviour_Enabled() throws UnitException, IllegalPositionException, TaskException, ExpressionException, StatementException, SchedulerException{
		testUnit.startDefaultBehaviour();
		assertEquals(testUnit.defaultBehaviourEnabled, true);
	}
	
	@Test
	public void defaultBehaviour_notEnabled() throws UnitException, IllegalPositionException,TaskException, ExpressionException, StatementException, SchedulerException{
		testUnit.startDefaultBehaviour();
		testUnit.stopDefaultBehaviour();
		assertEquals(testUnit.defaultBehaviourEnabled, false);
	}
	
	//after stopDefaultBehaviour(), the MovementType should be MovementType.nothing.
	@Test
	public void stopDefaultBehaviour_MovementType() throws UnitException, IllegalPositionException, TaskException, ExpressionException, StatementException, SchedulerException{
		testUnit.startDefaultBehaviour();
		testUnit.stopDefaultBehaviour();
		assertEquals(testUnit.getMovementType(), MovementType.nothing);
	}
	
	@Test
	public void AfterWorkingAndDefBehNotTrue() throws IllegalPositionException, UnitException, WorldException, GameObjectException, TaskException, ExpressionException, StatementException, SchedulerException{
		int [] PosArray = {1,1,0};
		Unit testUnit6 = new Unit ("Anton", PosArray, 50,50,50,50, false);
		
		testUnit6.stopDefaultBehaviour();
		testUnit6.workAt(1,1,0);
		testUnit6.advanceTime(20);
		assertEquals(testUnit6.getMovementType(), MovementType.nothing);		
	}
	
	@Test
	public void AfterRestingAndDefBehNotTrue() throws UnitException{
		testUnit.stopDefaultBehaviour();
		testUnit.decreaseHitpoints(1);
		testUnit.rest();
		testUnit.rest(1);
		assertEquals(testUnit.getMovementType(), MovementType.nothing);
	}//analoog voor walking.
	
	
	
	//// DYING ////
	@Test
	public void unit_dying() throws WorldException, UnitException, GameObjectException, TaskException, StatementException, ExpressionException{
		testUnitCarrier.die();
		assertEquals(testUnitCarrier.getMovementType(),MovementType.death);
		assertFalse(testUnitCarrier.isCarryingBoulder());
		assertTrue(World.getInstance().getCubeByPosition(	(int)testUnitCarrier.getPositionGameObjectX(), 
															(int)testUnitCarrier.getPositionGameObjectY(),
															(int)testUnitCarrier.getPositionGameObjectZ()).boulderSet.contains(boulder));
		assertFalse(World.getInstance().unitSet.contains(testUnitCarrier));
	}
	
	@Test
	public void unit_killed() throws UnitException, IllegalPositionException, WorldException, GameObjectException, TaskException, ExpressionException, StatementException, SchedulerException{
		testUnit2.setHitpoints(0);
		testUnit2.advanceTime(0.2);
		assertEquals(testUnit2.getMovementType(),MovementType.death);
	}
	
	//// CARRY, DROP, PICK-UP BOULDER & LOG ////
	
	// test are made for boulder, but log is completely similar to boulder
	@Test
	public void pick_upBoulder() throws GameObjectException{
		assertEquals(testUnitCarrier.getBoulder(),boulder);
		assertTrue(testUnitCarrier.isCarryingBoulder());
		assertTrue(testUnitCarrier.isCarryingMaterial());
		assertEquals(testUnitCarrier.getTotalWeight(), 50 + boulder.getWeight());
		assertEquals(boulder.getCarrier(),testUnitCarrier);
		assertFalse(testUnitCarrier.getCurrentCube().getBoulders().contains(boulder));
	}
	
	@Test
	public void dropBoulder_atValiPosition() throws GameObjectException, UnitException, WorldException{
		boulder.drop(testUnitCarrier, testUnitCarrier.getCurrentCube());
		assertEquals(testUnitCarrier.getBoulder(),null);
		assertFalse(testUnitCarrier.isCarryingBoulder());
		assertFalse(testUnitCarrier.isCarryingMaterial());
		assertEquals(testUnitCarrier.getTotalWeight(), 50);
		assertTrue(testUnitCarrier.getCurrentCube().getBoulders().contains(boulder));
	}
	
	@Test (expected=UnitException.class)
	public void dropBoulder_NotCarryingBoulder() throws GameObjectException, UnitException, WorldException{
		boulder.drop(testUnit4, testUnit4.getCurrentCube());
	}
	
	@Test(expected = UnitException.class)
	public void getClosestLog_noAvailable() throws UnitException, TaskException{
		World.getInstance().logSet.clear();
		testUnit.getClosestLog();	
	}//idem for getClosestBoulder, getClosestWorkshop, getFriendlyUnit, getEnemy and getAnyUnit
	
	@Test
	public void getClosestLog_legal() throws GameObjectException, WorldException, IllegalPositionException, UnitException, TaskException{
		double[] logPos = new double[3];
		logPos[0] = 1;
		logPos[1] = 2;
		logPos[2] = 0;
		World.getInstance().logSet.clear();
		Log log = new Log(logPos);
		assertEquals(testUnit.getClosestLog(), log);
	}//idem for getClosestBoulder, getClosestWorkshop, getFriendlyUnit, getEnemy, getAnyUnit
	
	@Test (expected = UnitException.class)
	public void getClosestWorkShop_noAvailable() throws TaskException, UnitException{
		double[] workshopPos = new double[3];
		workshopPos[0] = 1;
		workshopPos[1] = 2;
		workshopPos[2] = 0;
		World.getInstance().workshopSet.clear();
		testUnit.getClosestWorkshop();
	}
	
	@Test
	public void getClosestWorkshop_legal() throws TaskException, UnitException, WorldException{
		double[] workshopPos = new double[3];
		workshopPos[0] = 1;
		workshopPos[1] = 2;
		workshopPos[2] = 0;
		World.getInstance().getCubeByPosition(5, 5, 0).setTerrainType(3);
		World.getInstance().workshopSet.add(World.getInstance().getCubeByPosition(5,5,0));
		assertTrue(testUnit.getClosestWorkshop() != null);
	}
	
	@Test
	public void getFriendlyUnit() throws TaskException, UnitException{
		assertTrue(testUnit.getFriendlyUnit().getFaction() == testUnit.getFaction());
	}
	
	@Test
	public void getEnemy() throws TaskException, UnitException{
		assertFalse(testUnit.getEnemy().getFaction() == testUnit.getFaction());
	}
	
	@Test
	public void getAnyUnit() throws TaskException, UnitException{
		assertFalse(testUnit.getAnyUnit() == null);
	}
}
	
