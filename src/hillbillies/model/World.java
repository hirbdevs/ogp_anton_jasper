package hillbillies.model;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

import be.kuleuven.cs.som.annotate.*;
import hillbillies.exceptions.FactionException;
import hillbillies.exceptions.GameObjectException;
import hillbillies.exceptions.IllegalPositionException;
import hillbillies.exceptions.SchedulerException;
import hillbillies.exceptions.TaskException;
import hillbillies.exceptions.TooManyFactionsException;
import hillbillies.exceptions.UnitException;
import hillbillies.exceptions.WorldException;
import hillbillies.part2.listener.TerrainChangeListener;
import hillbillies.util.ConnectedToBorder;
import ogp.framework.util.Util;
import programmingLanguage.ExpressionException;
import programmingLanguage.StatementBreakException;
import programmingLanguage.StatementException;

/**
 * A class that implements a gameworld 
 * @author Anton
 *
 */
public class World {
	
	/**
	 * Makes sure that there is only one instance of the class World.
	 */
	private static World instance = null;
	/**
	 * The maximal boundaries of the gameworld.
	 */
	public static int MAXPOSITION_X;
	public static int MAXPOSITION_Y;
	public static int MAXPOSITION_Z;
	
	/**
	 * Length_cube gives the length, width and height of 1 cube in the game world, expressed in meters.
	 */
	public static final double LENGTH_CUBE = 1;
	
	public static final int MAXUNITS = 100;
	
	public static final int MAXFACTIONS = 5;
	
	/**
	 * contains all the units active in the gameWorld.
	 */
	public Set<Unit> unitSet;
	
	/**
	 * contains all the active factions in the gameWorld.
	 */
	public Set<Faction> factionSet;
	
	/**
	 * contains all the active boulders in the gameWorld.
	 */
	public Set<Boulder> boulderSet;
	
	/**
	 * contains all the active logs in the gameWorld.
	 */
	public Set<Log>	logSet;
	
	/**
	 * contains all the workshops in the gameWorld.
	 */
	public Set<Cube> workshopSet;
	
	/**
	 * contains the cubes in the gameworld with their position as a key.
	 */
	public HashMap<int[], Cube> cubeMap;
	
	/**
	 * contains an instance of the connectedToBorder algorithm provided in hillbillies.util.
	 */
	public static ConnectedToBorder connected;
	
	/**
	 * contains an instance of the TerrainChangeListener class provided in hillbillies.part2.listener.
	 */
	private TerrainChangeListener terrainChangeListener;
	
	/**
	 * sets the boundaries of the gameWordl to the values given as parameter.
	 * @param x
	 * 		the X boundary
	 * @param y
	 * 		the Y boundary
	 * @param z
	 * 		the Z boundary
	 */
	public void setBoundaries(int x, int y, int z){
		World.MAXPOSITION_X = x;
		World.MAXPOSITION_Y = y;
		World.MAXPOSITION_Z = z;
	}
	
	/**
	 * creates an instance of the world, if there is not yet one. If there is already an instance of a game world, this method returns
	 * this instance.
	 * @return
	 */
	public static World getInstance(){
		if (instance == null)
			instance = new World();
		return instance;
	}
	
	/**
	 * creator of the world. Sets the different sets that have to contain the gameObjects present in the game world.
	 */
	private World(){
		
		unitSet = new HashSet<Unit>();
		factionSet = new HashSet<Faction>();
		boulderSet = new HashSet<Boulder>();
		logSet = new HashSet<Log>();
		cubeMap = new HashMap<int[], Cube>();
		workshopSet = new HashSet<Cube>();
	
	}
	
	/**
	 * This method creates an instance of the connectedToBorder algorithm. 
	 * @param maxX
	 * 		the maximum X boundary of the game world.
	 * @param maxY
	 * 		the maximum Y boundary of the game world.
	 * @param maxZ
	 * 		the maximum Z boundary of the game world.
	 */
	public void setConnectedToBorder(int maxX, int maxY, int maxZ){
		connected = new ConnectedToBorder(maxX, maxY, maxZ);
		
	}
	
	/**
	 * This method initializes the instance of the connectedToBorder algorithm, so that the state of the algorithm is up to date with the game world.
	 * 
	 */
	public void initializeConnectedToBorder(){
		for (int x=0; x < MAXPOSITION_X; x++)
			for (int y = 0; y < MAXPOSITION_Y; y ++)
				for (int z = 0; z < MAXPOSITION_Z; z ++){
					if (World.getInstance().getCubeByPosition(x, y, z).isPassableCube()){
						connected.changeSolidToPassable(x, y, z);
					
					}
				}
	}
	
	/**
	 * Sets the TerrainChangeListener of the gameWorld.
	 * @param terrainListener
	 */
	public void setTerrainChangeListener(TerrainChangeListener terrainListener){
		if(this.terrainChangeListener == null)
			this.terrainChangeListener = terrainListener;
	}
	
	/**
	 * Sets the cubes to a given terrainType.
	 * @throws IllegalPositionException
	 * @throws WorldException 
	 * @effect for every position in the gameworld (x,y,z), there is a new cube created with the given terraintype.
	 * @effect for every created cube in the gameworld, this cube is added to the HashMap cubeMap in this class. The position of the position is given as key of the HashMap.
	 */
	public void setCubes(int [][][] cubeType) throws IllegalPositionException, WorldException{
		
		for (int x=0; x < MAXPOSITION_X; x++)
			for (int y = 0; y < MAXPOSITION_Y; y ++)
				for (int z = 0; z < MAXPOSITION_Z; z ++){
					int currentType = cubeType[x][y][z];
					
					Cube cube = new Cube(currentType, x, y, z);
					
					if (cube.getTerrainType() == TerrainType.workshop)
						workshopSet.add(cube);
					
					//populate HashMap
					int[] position = new int[3];
					position[0] = x;
					position[1] = y;
					position[2] = z;
					cubeMap.put(position, cube);
				}
	}

	
	/**
	 * Returns the set of Units active in the gameWorld.
	 * @return the set of Units active in the game world.
	 */
	@Basic
	public Set<Unit> getUnits(){	
		return unitSet;	
	}
	
	private static boolean freeToAccessUnitSet = true;
	private CopyOnWriteArrayList<Unit> AddQueue = new CopyOnWriteArrayList<Unit>();
	
	/**
	 * Adds a given Unit to the set unitSet.
	 * @param unit
	 * 		the unit we want to add to the set.
	 * @throws WorldException
	 * 		if the parameter unit is null, throw new exception with message: Cannot add Unit to the Set.
	 * @Post if the given unit is not null, it is present in the unitSet.
	 */
	public void addUnitToSet(Unit unit) throws WorldException{
		if(freeToAccessUnitSet == true){
			if (unit != null)
				unitSet.add(unit);
			else throw new WorldException("Cannot add Unit to the Set.");
		} else{
			AddQueue.add(unit);
		}
		
	}	
	
	
	/**
	 * A method that spawns a new unit with random initial attributes at a random position of the game world.
	 * @param enableDefaultBehaviour
	 * 		true if the Unit has to spawn in default behaviour.
	 * @return a Unit with random initial attributes at a random position of the game world.
	 * @throws WorldException
	 * @throws IllegalPositionException 
	 * @throws GameObjectException 
	 * @effect a new Unit is created with random initial attributes at a random position of the game world.
	 */
	public Unit spawnUnit(boolean enableDefaultBehaviour) throws WorldException, GameObjectException, IllegalPositionException{
		//set random intial attributes
		int strength = (int)Math.random()*100;
		int agility = (int)Math.random()*100;
		int toughness = (int)Math.random()*100;
		int weight = (int)Math.random()*100; //we do not need to check whether these values are passable, because it is programmed total.
		
		String defaultName = "Hillbilly";
		
		//set random initial position
		int[] position = getRandomPos();
		
		Unit unit;
		
		try{
			unit = new Unit(defaultName, position, weight, strength, agility, toughness, enableDefaultBehaviour);
			
		}catch (UnitException e){
			throw new WorldException(e.getMessage());
		}
		
		return unit;
	}
	
	/**
	 * This method generates a random position to spawn a unit.
	 * @return a position that consists of an array of 3 integers.
	 * 
	 * @note Works recursive until a valid position to spawn a unit is generated.
	 */
	public int[] getRandomPos(){
		int[] position = new int[3];
		position[0] = (int)(Math.random()*(World.MAXPOSITION_X-1));
		position[1] = (int)(Math.random()*(World.MAXPOSITION_Y-1));
		position[2] = (int)(Math.random()*(World.MAXPOSITION_Z-1));
		if (this.getCubeByPosition(position[0], position[1], position[2]).isPassableCube()){
			if (position[2] == 0) return position;
//			else if (this.getCubeByPosition(position[0], position[1], position [2] - 1).isSolidCube()){
			else if (this.hasSolidNeighbour(this.getCubeByPosition(position[0], position[1], position [2]))){
				return position;
			}
			else return getRandomPos();
		}else return getRandomPos();
		
	}
	
	private CopyOnWriteArrayList<Unit> RemoveQueue = new CopyOnWriteArrayList<Unit>();
	
	/**
	 * A method that removes a given Unit from the set of Units.
	 * @param unit
	 * 		the unit we want to remove from the set.
	 * @throws WorldException
	 * 		if unitSet does not contain the unit given as parameter, throw new WorldException.
	 * @Post the given unit is not in unitSet.
	 */
	public void removeUnitFromSet(Unit unit) throws WorldException{
		if (freeToAccessUnitSet == true){
			if (unitSet.contains(unit))
				unitSet.remove(unit);
			else throw new WorldException("No such Unit in the Set.");	
		}else{
			RemoveQueue.add(unit);
			}
	}
	
	/**
	 * Returns the set of Factions active in the gameWorld.
	 * @return factionSet
	 */
	@Basic
	public Set<Faction> getFactions(){
		return factionSet;
	}
	
	/**
	 * A method that adds a given faction to the set of factions.
	 * @param faction
	 * 		the faction we want to add to the set.
	 * @throws FactionException
	 * 		if the faction given as parameter is null, throw new FactionException.
	 * @Post if the given faction was not null, it is present in factionSet.
	 */
	public void addFactionToSet(Faction faction) throws FactionException{
		
		if (faction != null)
			factionSet.add(faction);
		else throw new FactionException(faction);
	}
	
	/**
	 *A method that removes a given faction from the set of factions.
	 * @param faction
	 * 		the faction we want to remove from the set.
	 * @throws FactionException
	 * 		if the faction given as parameter is not in the set, throw new FactionException.
	 * @Post the given faction is not in factionSet.
	 */
	public void removeFactionFromSet(Faction faction) throws FactionException{
		
		if (factionSet.contains(faction))
			factionSet.remove(faction);
		else throw new FactionException(faction);
	}
	
	/**
	 * A method to add a given Unit to the correct faction.
	 * @param unit
	 * 		the unit we want to add to the correct faction.
	 * @throws WorldException
	 * @throws UnitException
	 * 		if the unit given as parameter is null, throw new UnitException.
	 * @effect The unit is added to the set of units of the faction that is retrieved by this.getFactionToAdd().
	 * @effect The faction is added to the unit's properties.
	 */
	public void addUnitToFaction(Unit unit) throws WorldException, UnitException{
		
		if (unit != null){
			Faction faction = this.getFactionToAdd();
			faction.addUnitToFaction(unit);
			unit.setFaction(faction);
		}else throw new UnitException("");	
	}
	
	/**
	 * This method returns the faction where the next unit should be added to.
	 * @return the faction where the next unit should be added to.
	 * @throws WorldException
	 * 		if there are already the maximum number of factions and the factions have already reached their limited number of units, 
	 * 		throw new TooManyFactionsException.
	 * @effect if there are not yet the maximum number of factions, a new faction is created.
	 * @effect if a new faction is created, it is added to the set of factions.
	 */
	private Faction getFactionToAdd() throws WorldException{
		Faction minFaction;
		if (factionSet.size() < MAXFACTIONS){

			Faction faction = new Faction(factionSet.size() + 1);
			this.addFactionToSet(faction);
			return faction;
		}
		else if (factionSet.size()== MAXFACTIONS){

			minFaction = this.factionSet.iterator().next(); //minFaction becomes the first element of the factionSet.
			for (Faction faction : factionSet){
				if (faction.units.size() < minFaction.units.size())
						minFaction = faction;
			}
			return minFaction;	
		}
		else{
			throw new TooManyFactionsException();
		}
	}
	
	/**
	 * Returns all Units that belong to the given faction.
	 * @param faction
	 * 		the faction of which we want to get all units.
	 * @return the set of units belonging to the given faction.
	 */
	public Set<Unit> getUnitsOfFaction(Faction faction){
		return faction.getUnitsInFaction();
	}
	
	/**
	 * Returns all the boulders that are active in the game world.
	 * @return all the boulders that are active in the game world.
	 */
	@Basic
	public Set<Boulder> getBoulders(){
		return boulderSet;
	}
	
	/**
	 * A method to add a given boulder to the set of boulders.
	 * @param boulder
	 * 		the boulder we want to add to the set.
	 * @throws GameObjectException
	 * 		if the boulder given as parameter is null, throw new GameObjectException.	
	 * @Post if the given boulder is not null, it is present in boulderSet.
	 */
	public void addBoulderToSet(Boulder boulder) throws GameObjectException{
		if (boulder != null)
			boulderSet.add(boulder);
		else throw new GameObjectException(boulder);
	}
	
	/**
	 * A method to remove a given boulder from the set of boulders.
	 * @param boulder
	 * 		the boulder we want to remove from the set.
	 * @throws GameObjectException
	 * 		if the boulder given as parameter is not in the boulderSet, throw new GameObjectException.
	 * @Post the given boulder is not in boulderSet.
	 */
	public void removeBoulderFromSet(IMaterial boulder) throws GameObjectException{
		if (boulderSet.contains(boulder))
			boulderSet.remove(boulder);
		else throw new GameObjectException(boulder);
	}
	
	/**
	 * Returns all the logs that are active in the game world.
	 * @return the set of logs that are active in the game world.
	 */
	@Basic
	public Set<Log> getLogs(){
		return logSet;
	}
	
	/**
	 * A method that adds a log to the set of logs.
	 * @param log
	 * 		the log we want to add to the set.
	 * @throws GameObjectException
	 * 		if the log given as parameter is null, throw new GameObjectException.
	 * @Post if the given log is not null, it is present in logSet.
	 */
	public void addLogToSet(Log log) throws GameObjectException{
		if (log != null)
			logSet.add(log);
		else throw new GameObjectException(log);
	}
	
	/**
	 * A method that removes a log from the set of logs.
	 * @param log
	 * 		the log we want to remove from the set of logs.
	 * @throws GameObjectException 
	 *		if the log given as parameter is not in the set, throw new GameObjectException.
	 *@Post the given log is not in logSet.
	 */
	public void removeLogFromSet(Log log) throws GameObjectException{
		if (logSet.contains(log))
			logSet.remove(log);
		else throw new GameObjectException(log);
	}
	
	/**
	 * Returns true if the value of the paramater x which is a position coordinate is bigger or equal to zero and
	 * smaller or equal to parameter max.
	 * 
	 * @param 	x
	 * 			The position coordinate for which we want to check whether it is a valid one	 * 
	 * @param 	max
	 * 			The max value for x.			
	 * @return	true if x between 0 and max
	 * 			| if (x>0) && (x <= max)
	 * 			| then result = true
	 * 			| else result = false
	 */
	public static boolean isValidPosition(double x, double max){
		if ((x >= 0) && (x < max))
			return true;
		else return false;
	}
	
	/**
	 * Returns the cube that matches the given coordinates.
	 * @param x
	 * 		the x coordinate
	 * @param y
	 * 		the y coordinate
	 * @param z
	 * 		the z coordinate
	 * @return the cube that matches the given coordinates.
	 */
	@Basic
	public Cube getCubeByPosition(int x, int y, int z){
		
		for(int[] cubePosition: cubeMap.keySet())
			if ((cubePosition[0] == x) && (cubePosition[1] == y) && (cubePosition[2] == z))
				if (cubeMap.containsKey(cubePosition))
					return cubeMap.get(cubePosition);
		return null;
	}
 
	/**
	 * Returns the cube that matches the given position.
	 * @param position
	 * 		the position of which we want to get the cube.
	 * @return the cube that matches the given coordinates.
	 */
	public Cube getCubeByPosition(Position position){
		
		int x = (int)position.position_x;
		int y = (int)position.position_y;
		int z = (int)position.position_z;
		
		return this.getCubeByPosition(x, y, z);
	}
	
	/**
	 * Returns the TerrainType of the cube specified with the given position.
	 * @param x
	 * 		the x coordinate of the cube.
	 * @param y
	 * 		the y coordinate of the cube.
	 * @param z
	 * 		the z coordinate of the cube
	 * @return the integer that corresponds to the Terrain Type of the cube with the given coordinates.
	 * @throws WorldException 
	 */
	public int getTerrainTypeOfCube(int x, int y, int z){	
		return this.getCubeByPosition(x,y,z).getTerrainTypeNumber();
	}
	
	/**
	 * Returns the set of units that are currently present in the cube with the given position, if there is one.
	 * @param x
	 * 		the x coordinate of the cube.
	 * @param y
	 * 		the y coordinate of the cube.
	 * @param z
	 * 		the z coordinate of the cube
	 * @return the set of units that are currently present in the cube.
	 * @throws UnitException
	 */
	public Set<Unit> getUnitsInCube(int x, int y, int z){
	
		return this.getCubeByPosition(x,y,z).getUnits();
	}
	
	/**
	 * Returns the boulders that are currently present in the cube with the given position, if there is one.
	 * @param x
	 * 		the x coordinate of the cube.
	 * @param y
	 * 		the y coordinate of the cube.
	 * @param z
	 * 		the z coordinate of the cube.
	 * @return the boulders that are currently present in the cube.
	 * @throws GameObjectException 
	 */
	public Set<Boulder> getBouldersInCube(int x, int y, int z) {

		return this.getCubeByPosition(x, y, z).getBoulders();
	}
	
	/**
	 * Returns the logs that are currently present in the cube with the given position, if there is one.
	 * @param x
	 * 		the x coordinate of the cube.
	 * @param y
	 * 		the y coordinate of the cube.
	 * @param z
	 * 		the z coordinate of the cube
	 * @return the set of logs currently present in the cube.
	 */
	public Set<Log> getLogsInCube(int x, int y, int z){
		return this.getCubeByPosition(x, y, z).getLogs();
	}
	
	/**
	 * Returns true if the given cube is connected to the border;
	 * @param cube
	 * @return true if the given cube is connected to the border
	 * @throws WorldException
	 * 		if the cube given as parameter is null, throw new WorldException.
	 */
	public boolean isCubeConnectedToBorder(Cube cube) throws WorldException{
		
		if (cube != null){
		int x = (int)cube.getPositionX();
		int y = (int)cube.getPositionY();
		int z = (int)cube.getPositionZ();
		
		boolean result = connected.isSolidConnectedToBorder(x, y, z);
		
		return result;
		}else
			throw new WorldException("WorldException caused by null value of the parameter cube.");
		
	}
	
	/**
	 * Sets the state of the cube given as parameter from solid to passable if the terraintype of the cube is passable and notifies the terrainChangeListener.
	 * @param cube
	 * 		the cube of which we want to set the state.
	 * @param isPassable
	 * 		true if the cube given as parameter is passable.
	 * @throws WorldException 
	 * 		if the cube given as parameter is null, throw new WorldException.
	 */
	public void notifyTerrainTypeChange(Cube cube, boolean isPassable) throws WorldException {
		
		if (cube != null) {
			int x = (int) cube.getPositionX();
			int y = (int) cube.getPositionY();
			int z = (int) cube.getPositionZ();
			if (isPassable)
				connected.changeSolidToPassable(x, y, z);
			this.terrainChangeListener.notifyTerrainChanged(x, y, z);
		}else throw new WorldException("Given cube does not exist.");
	}
	
	/**
	 * Returns the set of cubes that are directly adjacent to the cube given as parameter.
	 * @param x
	 * 		the x coordinate
	 * @param y
	 * 		the y coordinate
	 * @param z
	 * 		the z coordinate
	 * @return Set of cubes that are directly adjacent to the cube given as parameter.
	 */
	public Set<Cube> getNeighbouringCubes(Cube cube) {
		
		Set<Cube> neighbours = new HashSet<Cube>();
		int x = (int)cube.getPositionX();
		int y = (int)cube.getPositionY();
		int z = (int)cube.getPositionZ();
		int i,j,k;
		
		for(i = -1; i <= 1; i ++)
			for (j = -1; j<= 1; j ++)
				for (k = -1; k <= 1; k ++){
					if (!World.isValidPosition(x + i, World.MAXPOSITION_X))
//						throw new IllegalPositionException(x+i);
						continue;
					else if (!World.isValidPosition(y + j, World.MAXPOSITION_Y))
						continue;
//						throw new IllegalPositionException(y+j);
					else if (!World.isValidPosition(z+k, World.MAXPOSITION_Z))
						continue;
//						throw new IllegalPositionException(z+k);
					else neighbours.add(this.getCubeByPosition(x + i, y + j, z + k));
					}
		neighbours.remove(this.getCubeByPosition(x, y, z));
		
		return neighbours;
	}

	// TODO deze methode moet er voor gaan zorgen dat er niet op alle 26 plaatsen gewerkt kan worden.
//	public Set<Cube> getSurroundingCubes(Cube cube){
//		Set<Cube> surroundingCubes = new HashSet<Cube>();
//		int x = (int)cube.getPositionX();
//		int y = (int)cube.getPositionY();
//		int z = (int)cube.getPositionZ();
//		int i,j,k;
//		for (i = -1; i <=1; i ++){
//			for (j = -1; j<= 1; j ++){
//				for (k = -1; k <= 1; k ++){
//						
//				}	
//			}
//		}
//	}
	
	/**
	 * A method that checks whether a cube has a solid cube directly adjacent to it.
	 * @param cube
	 * 		the cube for which we want to check it.
	 * @return true if the cube given as parameter has a solid cube directly adjacent to it.
	 */
	public boolean hasSolidNeighbour(Cube cube){
		
		Set<Cube> neighbours = this.getNeighbouringCubes(cube);
		for(Cube neighbourCube : neighbours)
			if(neighbourCube.isSolidCube())
				return true;
		if(Util.fuzzyEquals(cube.getPositionZ(), 0))
			return true;
		return false;
	}
	
	/**
	 * this method controls timed behaviour of the World and invoke the advanceTime methods of all game objects that are part of the World.
	 * @param GameTime
	 * @throws GameObjectException 
	 * @throws UnitException 
	 * @throws WorldException 
	 * @throws IllegalPositionException 
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException 
	 * @throws SecurityException 
	 * @throws NoSuchMethodException 
	 * @throws TaskException 
	 * @throws ExpressionException 
	 * @throws StatementException 
	 * @throws SchedulerException 
	 * @throws StatementBreakException 
	 * @throws IllegalStateException 
	 */
	public void advanceTime(double GameTime) throws GameObjectException, UnitException, WorldException, IllegalPositionException, TaskException, ExpressionException, StatementException, SchedulerException{

		
		freeToAccessUnitSet = false;
		
		if ((AddQueue.isEmpty()) && (RemoveQueue.isEmpty())){
		//this method must advance all objects that are currently present in the gameWorld.
		//advance all Units
		for (Unit unit: unitSet)
			unit.advanceTime(GameTime);
		//advance all boulders
		for (Boulder boulder: boulderSet)
			boulder.advanceTime(GameTime);
		//advance all logs
		for (Log log: logSet)
			log.advanceTime(GameTime);
		//advance all cubes
		for (int[]position: cubeMap.keySet()){
			cubeMap.get(position).advanceTime(GameTime);
		}
		}
		//TODO dit moet ook nog voor boulder en logset gebeuren!
		freeToAccessUnitSet = true;
		
		while(!AddQueue.isEmpty()){
			if (freeToAccessUnitSet) {
				Unit unitToAdd = AddQueue.iterator().next();
				addUnitToSet(unitToAdd);
				AddQueue.remove(unitToAdd);
			}
		}
		
		while(!RemoveQueue.isEmpty()){
			if (freeToAccessUnitSet) {
				Unit unitToRemove = RemoveQueue.iterator().next();
				removeUnitFromSet(unitToRemove);
				RemoveQueue.remove(unitToRemove);
			}
		}
	}
	
}
