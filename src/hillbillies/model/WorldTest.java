package hillbillies.model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import hillbillies.exceptions.FactionException;
import hillbillies.exceptions.GameObjectException;
import hillbillies.exceptions.IllegalPositionException;
import hillbillies.exceptions.UnitException;
import hillbillies.exceptions.WorldException;
import hillbillies.part2.listener.DefaultTerrainChangeListener;

public class WorldTest {
	World world;
	Cube testCube, testCube2;
	DefaultTerrainChangeListener listener = new DefaultTerrainChangeListener();
	int[] position;
	static int[][][] cubeType = new int[3][3][3];
	Unit testUnit;
	Unit testUnit2;
	Faction testFaction;
	Boulder testBoulder;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		for (int x = 0; x<3; x++)
			for(int y = 0; y<3; y++)
				for(int z = 0; z<3; z++)
					cubeType[x][y][z] = 0;
	}

	@Before
	public void setUp() throws Exception {
		world = World.getInstance();
		World.getInstance().setTerrainChangeListener(listener);
		World.getInstance().setBoundaries(2, 2, 2);
		World.getInstance().setConnectedToBorder(World.MAXPOSITION_X, World.MAXPOSITION_Y, World.MAXPOSITION_Z);
		World.getInstance().setCubes(cubeType);
	}

	/**
	 * tests the constructor of the world.
	 */
	@Test
	public void worldConstructor(){
		assertEquals(World.getInstance(), world);
	}
	
	@Test
	public void setBoundaries(){
		World.getInstance().setBoundaries(2,2,2);
		assertEquals(World.MAXPOSITION_X, 2);
	}
	
	@Test 
	public void connectedToBorderTest(){
		World.getInstance().setConnectedToBorder(50, 50, 50);
		assertTrue(World.connected != null);
	}
	
	@Test
	public void getCubeByPosition_legal() throws IllegalPositionException, WorldException{
		assertFalse(World.getInstance().getCubeByPosition(1, 1, 1) == null);
	}
	
	@Test
	public void getCubeByPosition_negativePos() {
		assertEquals(World.getInstance().getCubeByPosition(-1,1,1), null);
	}
	
	@Test
	public void getCubeByPosition_tooLargePos(){
		assertEquals(World.getInstance().getCubeByPosition(60, 60, 60), null);
	}
	
	@Test
	public void getCubeByPosition_notInKeySet(){
		assertEquals(World.getInstance().getCubeByPosition(3, 3, 3), null);
	}
	
	@Test 
	public void getCubePosition(){
		assertEquals(World.getInstance().getCubeByPosition(1, 1, 1).getPositionX(), 1, 0.00001);
	}
	
	@Test
	public void getUnits_noUnit(){
		World.getInstance().unitSet.clear();
		assertTrue(World.getInstance().unitSet.isEmpty());
	}

	@Test
	public void getUnits_legal() throws UnitException, WorldException, GameObjectException, IllegalPositionException{
		World.getInstance().unitSet.clear();
		int[] position = new int[3]; position[0]=1; position[1]=1; position[2]=1;
		testUnit = new Unit("Hillbilly", position, 20,20,20,20,false);
		assertTrue(World.getInstance().getUnits().contains(testUnit));
	}
	
	@Test
	public void addUnitToSet_legal() throws UnitException, WorldException, GameObjectException, IllegalPositionException{
		int[] position = new int[3]; position[0]=1; position[1]=1; position[2]=1;
		testUnit = new Unit("Hillbilly", position, 20,20,20,20,false);
		World.getInstance().unitSet.clear();
		World.getInstance().addUnitToSet(testUnit);
		assertTrue(World.getInstance().unitSet.contains(testUnit));
	}
	
	@Test(expected = WorldException.class)
	public void addUnitToSet_null() throws UnitException, WorldException{
		World.getInstance().addUnitToSet(null);
	}
	
	@Test
	public void RemoveUnitFromSet_legal() throws UnitException, WorldException, GameObjectException, IllegalPositionException{
		World.getInstance().unitSet.clear();
		int[] position = new int[3]; position[0]=1; position[1]=1; position[2]=1;
		testUnit = new Unit("Hillbilly", position, 20,20,20,20,false);
		World.getInstance().removeUnitFromSet(testUnit);
		assertFalse(World.getInstance().unitSet.contains(testUnit));	
	}
	
	@Test(expected = WorldException.class)
	public void RemoveUnitFromSet_notInSet() throws UnitException, WorldException{
		World.getInstance().removeUnitFromSet(testUnit);
	}
	
	@Test
	public void spawnUnit_legalWeight() throws WorldException, GameObjectException, IllegalPositionException{
		testUnit2 = World.getInstance().spawnUnit(false);
		assertTrue(testUnit2.isValidWeight(testUnit2.getWeight()));
	}
	
	@Test
	public void spawnUnit_legalAgility() throws WorldException, GameObjectException, IllegalPositionException{
		testUnit2 = World.getInstance().spawnUnit(false);
		assertTrue(testUnit2.isValidInitialValue(testUnit2.getAgility()));
	}//analog for weight and toughness.
	
	@Test
	public void spawnUnit_legalName() throws WorldException, GameObjectException, IllegalPositionException{
		testUnit2 = World.getInstance().spawnUnit(false);
		assertTrue(testUnit2.isValidName(testUnit2.getName()));
	}
	
	@Test
	public void spawnUnit_getRandomPos() throws WorldException, GameObjectException, IllegalPositionException{
		testUnit2 = World.getInstance().spawnUnit(false);
		assertTrue((World.isValidPosition(testUnit2.getPositionGameObjectX(), World.MAXPOSITION_X))
					&&(World.isValidPosition(testUnit2.getPositionGameObjectY(),  World.MAXPOSITION_Y))
					&&(World.isValidPosition(testUnit2.getPositionGameObjectZ(), World.MAXPOSITION_Z)));	
	}
	
	@Test
	public void addFactionToSet_legal() throws FactionException{
		testFaction = new Faction(1);
		World.getInstance().addFactionToSet(testFaction);
		assertTrue(World.getInstance().factionSet.contains(testFaction));
	}
	
	@Test(expected = FactionException.class)
	public void addFactionToSet_nullValue() throws FactionException{
		World.getInstance().addFactionToSet(null);
	}
	
	@Test
	public void removeFactionFromSet_legal() throws FactionException{
		World.getInstance().factionSet.clear();
		testFaction = new Faction(1);
		World.getInstance().addFactionToSet(testFaction);
		World.getInstance().removeFactionFromSet(testFaction);
		assertFalse(World.getInstance().factionSet.contains(testFaction));
	}
	
	@Test(expected = FactionException.class)
	public void removeFactionFromSet_noFaction() throws FactionException{
		testFaction = new Faction(1);
		World.getInstance().factionSet.clear();
		World.getInstance().removeFactionFromSet(testFaction);
	}
	
	@Test
	public void addUnitToFaction_legal() throws UnitException, WorldException, GameObjectException, IllegalPositionException{
		int[] position = new int[3]; position[0]=1; position[1]=1; position[2]=1;
		testUnit = new Unit("Hillbilly", position, 20,20,20,20,false);
		assertTrue(testUnit.getFaction() != null);
	}
	
	@Test(expected = UnitException.class)
	public void addUnitToFaction_nullForUnit()throws UnitException, WorldException{
		World.getInstance().addUnitToFaction(null);
	}
	
	@Test
	public void addBoulderToSet_legal() throws GameObjectException, WorldException, IllegalPositionException{
		double[]pos = new double[3]; pos[0]=1; pos[1]=1; pos[2]=1;
		testBoulder = new Boulder(pos);
		World.getInstance().addBoulderToSet(testBoulder);
		assertTrue(World.getInstance().boulderSet.contains(testBoulder));
	}//same for Log
	
	@Test(expected = GameObjectException.class)
	public void addBoulderToSet_nullValue() throws GameObjectException{
		World.getInstance().addBoulderToSet(null);
	}//same for Log
	
	@Test
	public void removeBoulderFromSet_legal() throws GameObjectException, WorldException, IllegalPositionException{
		double[]pos = new double[3]; pos[0]=1; pos[1]=1; pos[2]=1;
		testBoulder = new Boulder(pos);
		World.getInstance().boulderSet.clear();
		World.getInstance().addBoulderToSet(testBoulder);
		World.getInstance().removeBoulderFromSet(testBoulder);
		assertFalse(World.getInstance().boulderSet.contains(testBoulder));
	}//same for Log
	
	@Test(expected = GameObjectException.class)
	public void removeBoulderFromSet_notInSet() throws GameObjectException, WorldException, IllegalPositionException{
		double[]pos = new double[3]; pos[0]=1; pos[1]=1; pos[2]=1;
		testBoulder = new Boulder(pos);
		World.getInstance().boulderSet.clear();
		World.getInstance().removeBoulderFromSet(testBoulder);
	}//same for Log
	
	@Test
	public void getTerrainTypeOfCube_legal(){
		assertEquals(World.getInstance().getTerrainTypeOfCube(1, 1, 1),0);
	}
	
	@Test
	public void getUnitsInCube_legal() throws UnitException, WorldException, GameObjectException, IllegalPositionException{
		int[] position = new int[3]; position[0]=1; position[1]=1; position[2]=1;
		testUnit = new Unit("Hillbilly", position, 20,20,20,20,false);
		assertTrue(World.getInstance().getUnitsInCube((int)testUnit.getPositionGameObjectX(), 
				(int)testUnit.getPositionGameObjectY(), (int)testUnit.getPositionGameObjectZ()).contains(testUnit));	
	}
	
	@Test
	public void getBouldersInCube_legal() throws GameObjectException, WorldException, IllegalPositionException{
		double[]pos = new double[3]; pos[0]=1; pos[1]=1; pos[2]=1;
		testBoulder = new Boulder(pos);
		assertTrue(World.getInstance().getBouldersInCube((int)testBoulder.getPositionGameObjectX(),
				(int)testBoulder.getPositionGameObjectY(), (int)testBoulder.getPositionGameObjectZ()).contains(testBoulder));
	}//same for Log
}
