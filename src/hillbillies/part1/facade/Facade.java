package hillbillies.part1.facade;

import hillbillies.exceptions.GameObjectException;
import hillbillies.exceptions.IllegalPositionException;
import hillbillies.exceptions.SchedulerException;
import hillbillies.exceptions.TaskException;
import hillbillies.exceptions.UnitException;
import hillbillies.exceptions.WorldException;
import hillbillies.model.MovementType;
import hillbillies.model.Unit;
import hillbillies.model.World;
import ogp.framework.util.ModelException;
import programmingLanguage.ExpressionException;
import programmingLanguage.StatementException;

public class Facade implements IFacade{

	@Override
	public Unit createUnit(String name, int[] initialPosition, int weight, int agility, int strength, int toughness,
			boolean enableDefaultBehaviour) throws ModelException {
		
		try{	
			Unit unit = new Unit(name, initialPosition, weight, agility, strength, toughness, enableDefaultBehaviour);
			return unit;
		}catch (UnitException e){
			throw new ModelException(e.getMessage());
		}catch (WorldException e){
			throw new ModelException(e.getMessage());
		} catch (GameObjectException e) {
			throw new ModelException(e.getMessage());
		} catch (IllegalPositionException e) {
			throw new ModelException(e.getMessage());
		}
	}

	@Override
	public double[] getPosition(Unit unit) throws ModelException {
		
		double[] position = new double[3];
		
		position[0] = unit.getPositionGameObjectX();
		position[1] = unit.getPositionGameObjectY();
		position[2] = unit.getPositionGameObjectZ();
	
		return position;
	}

	@Override
	public int[] getCubeCoordinate(Unit unit) throws ModelException {
		
		int[] cube_coordinate = new int[3];
		
		cube_coordinate[0] = (int)unit.getGameObjectPositionXGameworld();
		cube_coordinate[1] = (int)unit.getGameObjectPositionYGameworld();
		cube_coordinate[2] = (int)unit.getGameObjectPositionZGameworld();
	
		return cube_coordinate;
	}

	@Override
	public String getName(Unit unit) throws ModelException {
		
		return unit.getName();
	}

	@Override
	public void setName(Unit unit, String newName) throws ModelException {
		
		try{
			unit.setName(newName);
		} catch (UnitException e){
			throw new ModelException(e.getMessage(), e);
		}
	}

	@Override
	public int getWeight(Unit unit) throws ModelException {
		
		return unit.getWeight();
	}

	@Override
	public void setWeight(Unit unit, int newValue) throws ModelException {
		
		unit.setWeight(newValue);
	}

	@Override
	public int getStrength(Unit unit) throws ModelException {
		
		return unit.getStrength();
	}

	@Override
	public void setStrength(Unit unit, int newValue) throws ModelException {
		
		unit.setStrength(newValue);
	}

	@Override
	public int getAgility(Unit unit) throws ModelException {
	
		return unit.getAgility();
	}

	@Override
	public void setAgility(Unit unit, int newValue) throws ModelException {

		unit.setAgility(newValue);
	}

	@Override
	public int getToughness(Unit unit) throws ModelException {
	
		return unit.getThoughness();
	}

	@Override
	public void setToughness(Unit unit, int newValue) throws ModelException {
	
		unit.setToughness(newValue);
	}

	@Override
	public int getMaxHitPoints(Unit unit) throws ModelException {
	
		return (int)unit.getMaxHitpoints();
	}

	@Override
	public int getCurrentHitPoints(Unit unit) throws ModelException {
	
		return unit.getHitpoints();
	}

	@Override
	public int getMaxStaminaPoints(Unit unit) throws ModelException {
	
		return (int)unit.getMaxStaminapoints();
	}

	@Override
	public int getCurrentStaminaPoints(Unit unit) throws ModelException {
	
		return unit.getStaminapoints();
	}

	@Override
	public void moveToAdjacent(Unit unit, int dx, int dy, int dz) throws ModelException {
	
		try{
			//unit.moveTo(unit.getPositionX() + dx, unit.getPositionY() + dy, unit.getPositionZ() + dz);
			unit.moveTo(unit.getGameObjectPositionXGameworld()+World.LENGTH_CUBE/2+dx,
						unit.getGameObjectPositionYGameworld()+World.LENGTH_CUBE/2+dy, 
						unit.getGameObjectPositionZGameworld()+World.LENGTH_CUBE/2+dz);
		} catch (UnitException e){
			throw new ModelException(e.getMessage(), e);
		} catch (IllegalPositionException e){
			throw new ModelException(e.getMessage());
		}
	}

	@Override
	public double getCurrentSpeed(Unit unit) throws ModelException {
	
		if (unit.getMovementType() == MovementType.walking)
			return (double)unit.getWalkingSpeed();
		if (unit.getMovementType() == MovementType.sprinting)
			return (double)unit.getSprintingSpeed();
		else return 0;
	}

	@Override
	public boolean isMoving(Unit unit) throws ModelException {
	
		return unit.isMoving();
	}

	@Override
	public void startSprinting(Unit unit) throws ModelException {
	
		unit.sprint();
	}

	@Override
	public void stopSprinting(Unit unit) throws ModelException {

		unit.stopSprinting();
	}

	@Override
	public boolean isSprinting(Unit unit) throws ModelException {
	
		return (unit.getMovementType() == MovementType.sprinting);
	}

	@Override
	public double getOrientation(Unit unit) throws ModelException {

		return unit.getOrientation();
	}

	@Override
	public void moveTo(Unit unit, int[] cube) throws ModelException {
	
		try{
			this.setDefaultBehaviorEnabled(unit, false);
			unit.moveTo(cube[0] + World.LENGTH_CUBE/2, cube[1] + World.LENGTH_CUBE/2, cube[2]+World.LENGTH_CUBE/2);
		} catch (IllegalPositionException e){
			throw new ModelException(e.getMessage(), e);
		} catch (UnitException e){
			throw new ModelException(e.getMessage());
		}
	}

	
	@Override
	public boolean isWorking(Unit unit) throws ModelException {
		
		return (unit.getMovementType() == MovementType.working);
	}

	@Override
	public void fight(Unit attacker, Unit defender) throws ModelException {

		this.setDefaultBehaviorEnabled(attacker, false);
		attacker.attack(defender);
	}

	@Override
	public boolean isAttacking(Unit unit) throws ModelException {
		
		return (unit.getMovementType() == MovementType.fighting);
	}

	@Override
	public void rest(Unit unit) throws ModelException {
		try{
			unit.rest();
		} catch (UnitException e){
			throw new ModelException(e.getMessage());
		}
	}

	@Override
	public boolean isResting(Unit unit) throws ModelException {
	
		return (unit.getMovementType() == MovementType.resting);
	}

	@Override
	public void setDefaultBehaviorEnabled(Unit unit, boolean value) throws ModelException {
	
		if (value == true){
			try {
				unit.startDefaultBehaviour();
			} catch (UnitException e) {
				throw new ModelException(e.getMessage());
			} catch (IllegalPositionException e){
				throw new ModelException(e.getMessage());
			} catch (TaskException e) {
				throw new ModelException(e.getMessage());
			} catch (ExpressionException e) {
				throw new ModelException(e.getMessage());
			} catch (StatementException e) {
				throw new ModelException(e.getMessage());
			} catch (SchedulerException e) {
				throw new ModelException(e.getMessage());
			}
			}
		else if (value == false)
			unit.stopDefaultBehaviour();	
	}

	@Override
	public boolean isDefaultBehaviorEnabled(Unit unit) throws ModelException {
	
		return unit.defaultBehaviourEnabled;
	}

	@Override
	public void advanceTime(Unit unit, double dt) throws ModelException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void work(Unit unit) throws ModelException {
		// TODO Auto-generated method stub
		
	}
}
