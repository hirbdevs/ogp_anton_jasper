package hillbillies.part3.facade;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import hillbillies.exceptions.GameObjectException;
import hillbillies.exceptions.IllegalPositionException;
import hillbillies.exceptions.IteratorException;
import hillbillies.exceptions.SchedulerException;
import hillbillies.exceptions.TaskException;
import hillbillies.exceptions.UnitException;
import hillbillies.exceptions.WorldException;
import hillbillies.model.Boulder;
import hillbillies.model.Faction;
import hillbillies.model.Log;
import hillbillies.model.MovementType;
import hillbillies.model.Scheduler;
import hillbillies.model.Task;
import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.part2.listener.TerrainChangeListener;
import hillbillies.part3.programs.ITaskFactory;
import ogp.framework.util.ModelException;
import programmingLanguage.ExpressionException;
import programmingLanguage.StatementException;
import programmingLanguage.TaskFactory;
import programmingLanguage.Statements.Statement;
import programmingLanguage.expressions.Expression;

public class Facade implements IFacade{

	@Override
	public World createWorld(int[][][] terrainTypes, TerrainChangeListener modelListener) throws ModelException {
		World world = World.getInstance();
		
		World.getInstance().setBoundaries(terrainTypes.length, terrainTypes[0].length, terrainTypes[0][0].length);
		
		world.setTerrainChangeListener(modelListener);
		
		world.setConnectedToBorder(World.MAXPOSITION_X, World.MAXPOSITION_Y, World.MAXPOSITION_Z);
		
		try {
			world.setCubes(terrainTypes);
		} catch (IllegalPositionException e) {
			throw new ModelException(e.getMessage());
		} catch (WorldException e) {
			throw new ModelException(e.getMessage());
		}
		
//		world.initializeConnectedToBorder();
		
		return world;
	}

	@Override
	public int getNbCubesX(World world) throws ModelException {
		return World.MAXPOSITION_X;
	}

	@Override
	public int getNbCubesY(World world) throws ModelException {
		return World.MAXPOSITION_Y;
	}

	@Override
	public int getNbCubesZ(World world) throws ModelException {
		return World.MAXPOSITION_Z;
	}

	@Override
	public void advanceTime(World world, double dt) throws ModelException {
		try {
			World.getInstance().advanceTime(dt);
		} catch (GameObjectException e) {
			throw new ModelException(e.getMessage());
		} catch (UnitException e) {
			throw new ModelException(e.getMessage());
		} catch (WorldException e){
			throw new ModelException(e.getMessage());
		} catch (IllegalPositionException e){
			throw new ModelException(e.getMessage());
		} catch (TaskException e) {
			throw new ModelException(e.getMessage());
		} catch (ExpressionException e) {
			throw new ModelException(e.getMessage());
		} catch (StatementException e) {
			throw new ModelException(e.getMessage());
		} catch (SchedulerException e) {
			throw new ModelException(e.getMessage());
		} 
		
	}

	@Override
	public int getCubeType(World world, int x, int y, int z) throws ModelException {
		return world.getCubeByPosition(x,y,z).getTerrainType().getNumber();
//		return world.getTerrainTypeOfCube(x, y, z);
	}

	@Override
	public void setCubeType(World world, int x, int y, int z, int value) throws ModelException {
		try {
			world.getCubeByPosition(x, y, z).setTerrainType(value);
		} catch (WorldException e) {
			throw new ModelException(e.getMessage());
		}
		
	}

	@Override
	public boolean isSolidConnectedToBorder(World world, int x, int y, int z) throws ModelException {
		try {
			
			return world.isCubeConnectedToBorder(world.getCubeByPosition(x, y, z));
			
		} catch (WorldException e) {
			throw new ModelException(e.getMessage());
		}
	}

	@Override
	public Unit spawnUnit(World world, boolean enableDefaultBehavior) throws ModelException {
		try {
			return world.spawnUnit(enableDefaultBehavior);
		} catch (WorldException e) {
			throw new ModelException(e.getMessage());
		} catch (GameObjectException e) {
			throw new ModelException(e.getMessage());	
		} catch (IllegalPositionException e) {
			throw new ModelException(e.getMessage());		
		}
	}

	@Override
	public void addUnit(Unit unit, World world) throws ModelException {
		try {
			world.addUnitToSet(unit);
		} catch (WorldException e) {
			throw new ModelException(e.getMessage());
		}
		
	}

	@Override
	public Set<Unit> getUnits(World world) throws ModelException {
		 
		return world.getUnits();
	}

	@Override
	public boolean isCarryingLog(Unit unit) throws ModelException {
		return unit.isCarryingLog();
	}

	@Override
	public boolean isCarryingBoulder(Unit unit) throws ModelException {
		return unit.isCarryingBoulder();
	}

	@Override
	public boolean isAlive(Unit unit) throws ModelException {
		return unit.isAlive();
	}

	@Override
	public int getExperiencePoints(Unit unit) throws ModelException {
		return unit.getExperiencePoints();
	}

	@Override
	public void workAt(Unit unit, int x, int y, int z) throws ModelException {
		try {
			unit.workAt(x, y, z);
		} catch (UnitException e) {
			throw new ModelException(e.getMessage());
		}
		
	}

	@Override
	public Faction getFaction(Unit unit) throws ModelException {
		return unit.getFaction();
	}

	@Override
	public Set<Unit> getUnitsOfFaction(Faction faction) throws ModelException {
		return faction.getUnitsInFaction();
	}

	@Override
	public Set<Faction> getActiveFactions(World world) throws ModelException {
		return world.factionSet;
	}

	@Override
	public double[] getPosition(Boulder boulder) throws ModelException {
		 
		double[] position = new double[3];
		position[0] = boulder.getPositionGameObjectX();
		position[1] = boulder.getPositionGameObjectY();
		position[2] = boulder.getPositionGameObjectZ();
		
		return position;
	}
	
	@Override
	public Set<Boulder> getBoulders(World world) throws ModelException {
		 
		return world.getBoulders();
	}

	@Override
	public double[] getPosition(Log log) throws ModelException {
		 
		double[] position = new double[3];
		position[0] = log.getPositionGameObjectX();
		position[1] = log.getPositionGameObjectY();
		position[2] = log.getPositionGameObjectZ();
		
		return position;
	}

	@Override
	public Set<Log> getLogs(World world) throws ModelException {
		 
		return world.getLogs();
	}

	@Override
	public Unit createUnit(String name, int[] initialPosition, int weight, int agility, int strength, int toughness,
			boolean enableDefaultBehavior) throws ModelException {
		
		try{	
			Unit unit = new Unit(name, initialPosition, weight, agility, strength, toughness, enableDefaultBehavior);
			return unit;
		}catch (UnitException e){
			throw new ModelException(e.getMessage());
		}catch (WorldException e){
			throw new ModelException(e.getMessage());
		} catch (GameObjectException e) {
			throw new ModelException(e.getMessage());
		} catch (IllegalPositionException e) {
			throw new ModelException(e.getMessage());			
		}
	}

	@Override
	public double[] getPosition(Unit unit) throws ModelException {
		
		double[] position = new double[3];
		
		position[0] = unit.getPositionGameObjectX();
		position[1] = unit.getPositionGameObjectY();
		position[2] = unit.getPositionGameObjectZ();
	
		return position;
	}

	@Override
	public int[] getCubeCoordinate(Unit unit) throws ModelException {
		
		int[] cube_coordinate = new int[3];
		
		cube_coordinate[0] = (int)unit.getGameObjectPositionXGameworld();
		cube_coordinate[1] = (int)unit.getGameObjectPositionYGameworld();
		cube_coordinate[2] = (int)unit.getGameObjectPositionZGameworld();
	
		return cube_coordinate;
	}

	@Override
	public String getName(Unit unit) throws ModelException {
		
		return unit.getName();
	}

	@Override
	public void setName(Unit unit, String newName) throws ModelException {
		
		try{
			unit.setName(newName);
		} catch (UnitException e){
			throw new ModelException(e.getMessage(), e);
		}
	}

	@Override
	public int getWeight(Unit unit) throws ModelException {
		
		return unit.getWeight();
	}

	@Override
	public void setWeight(Unit unit, int newValue) throws ModelException {
		
		unit.setWeight(newValue);
	}
	
	@Override
	public int getStrength(Unit unit) throws ModelException {
		
		return unit.getStrength();
	}

	@Override
	public void setStrength(Unit unit, int newValue) throws ModelException {
		
		unit.setStrength(newValue);
	}

	@Override
	public int getAgility(Unit unit) throws ModelException {
	
		return unit.getAgility();
	}

	@Override
	public void setAgility(Unit unit, int newValue) throws ModelException {

		unit.setAgility(newValue);
	}

	@Override
	public int getToughness(Unit unit) throws ModelException {
	
		return unit.getThoughness();
	}

	@Override
	public void setToughness(Unit unit, int newValue) throws ModelException {
	
		unit.setToughness(newValue);
	}

	@Override
	public int getMaxHitPoints(Unit unit) throws ModelException {
	
		return (int)unit.getMaxHitpoints();
	}

	@Override
	public int getCurrentHitPoints(Unit unit) throws ModelException {
	
		return unit.getHitpoints();
	}

	@Override
	public int getMaxStaminaPoints(Unit unit) throws ModelException {
	
		return (int)unit.getMaxStaminapoints();
	}

	@Override
	public int getCurrentStaminaPoints(Unit unit) throws ModelException {
	
		return unit.getStaminapoints();
	}


	@Override
	public void moveToAdjacent(Unit unit, int dx, int dy, int dz) throws ModelException {
	
		try{
			//unit.moveTo(unit.getPositionX() + dx, unit.getPositionY() + dy, unit.getPositionZ() + dz);
			unit.moveTo(unit.getGameObjectPositionXGameworld()+World.LENGTH_CUBE/2+dx,
						unit.getGameObjectPositionYGameworld()+World.LENGTH_CUBE/2+dy, 
						unit.getGameObjectPositionZGameworld()+World.LENGTH_CUBE/2+dz);
		} catch (UnitException e){
			throw new ModelException(e.getMessage(), e);
		} catch (IllegalPositionException e){
			throw new ModelException(e.getMessage());
		}
	}

	@Override
	public double getCurrentSpeed(Unit unit) throws ModelException {
	
		if (unit.getMovementType() == MovementType.walking)
			return (double)unit.getWalkingSpeed();
		if (unit.getMovementType() == MovementType.sprinting)
			return (double)unit.getSprintingSpeed();
		else return 0;
	}

	@Override
	public boolean isMoving(Unit unit) throws ModelException {
	
		return unit.isMoving();
	}

	@Override
	public void startSprinting(Unit unit) throws ModelException {
	
		unit.sprint();
	}

	@Override
	public void stopSprinting(Unit unit) throws ModelException {

		unit.stopSprinting();
	}

	@Override
	public boolean isSprinting(Unit unit) throws ModelException {
	
		return (unit.getMovementType() == MovementType.sprinting);
	}

	@Override
	public double getOrientation(Unit unit) throws ModelException {

		return unit.getOrientation();
	}

	@Override
	public void moveTo(Unit unit, int[] cube) throws ModelException {
	
		try{
			this.setDefaultBehaviorEnabled(unit, false);
			unit.moveTo(cube[0] + World.LENGTH_CUBE/2, cube[1] + World.LENGTH_CUBE/2, cube[2]+World.LENGTH_CUBE/2);
		} catch (IllegalPositionException e){
			throw new ModelException(e.getMessage(), e);
		} catch (UnitException e){
			throw new ModelException(e.getMessage());
		}
	}


	@Override
	public boolean isWorking(Unit unit) throws ModelException {
		
		return (unit.getMovementType() == MovementType.working);
	}

	@Override
	public void fight(Unit attacker, Unit defender) throws ModelException {

		this.setDefaultBehaviorEnabled(attacker, false);
		attacker.attack(defender);
	}

	@Override
	public boolean isAttacking(Unit unit) throws ModelException {
		
		return (unit.getMovementType() == MovementType.fighting);
	}

	@Override
	public void rest(Unit unit) throws ModelException {
		try{
			unit.rest();
		} catch (UnitException e){
			throw new ModelException(e.getMessage());
		}
	}

	@Override
	public boolean isResting(Unit unit) throws ModelException {
	
		return (unit.getMovementType() == MovementType.resting);
	}

	@Override
	public void setDefaultBehaviorEnabled(Unit unit, boolean value) throws ModelException {
	
		if (value == true){
			try {
				try {
					unit.startDefaultBehaviour();
				} catch (TaskException e) {
					throw new ModelException(e.getMessage());
				} catch (ExpressionException e) {
					throw new ModelException(e.getMessage());
				} catch (StatementException e) {
					throw new ModelException(e.getMessage());
				} catch (SchedulerException e) {
					throw new ModelException(e.getMessage());
				} 
			} catch (UnitException e) {
				throw new ModelException(e.getMessage());
			} catch (IllegalPositionException e){
				throw new ModelException(e.getMessage());
			} catch (IllegalArgumentException e) {
				throw new ModelException(e.getMessage());
			}
			}
		else if (value == false)
			unit.stopDefaultBehaviour();	
	}

	@Override
	public boolean isDefaultBehaviorEnabled(Unit unit) throws ModelException {
	
		return unit.defaultBehaviourEnabled;
	}

	@Override
	public ITaskFactory<?, ?, Task> createTaskFactory() {
		ITaskFactory<Expression, Statement, Task> factory = new TaskFactory();
		return factory;
	}

	@Override
	public boolean isWellFormed(Task task) throws ModelException {
//		try {
//			return task.isWellFormed(task.getStatement());
//		} catch (ExpressionException | StatementException | TaskException e) {
//			throw new ModelException(e.getMessage());
//		}
		return true;
	}

	@Override
	public Scheduler getScheduler(Faction faction) throws ModelException {
		return faction.getScheduler();
	}

	@Override
	public void schedule(Scheduler scheduler, Task task) throws ModelException {
		try {
			scheduler.schedule(task);
		} catch (SchedulerException e) {
			throw new ModelException(e.getMessage());
		}
	}

	@Override
	public void replace(Scheduler scheduler, Task original, Task replacement) throws ModelException {
		try {
			scheduler.replaceTasks(original, replacement);
		} catch (SchedulerException e) {
			throw new ModelException(e.getMessage());
		} catch (TaskException e) {
			throw new ModelException(e.getMessage());
		}
	}

	@Override
	public boolean areTasksPartOf(Scheduler scheduler, Collection<Task> tasks) throws ModelException {
		try {
			return scheduler.areTasksPartOf(tasks);
		} catch (IteratorException e) {
			throw new ModelException(e.getMessage());
		}
	}

	@Override
	public Iterator<Task> getAllTasksIterator(Scheduler scheduler) throws ModelException {
		try {
			return scheduler.getAllTasksIterator();
			
		} catch (IteratorException e) {
			throw new ModelException(e.getMessage());
		}
	}

	@Override
	public Set<Scheduler> getSchedulersForTask(Task task) throws ModelException {
		return task.getSchedulers();
	}

	@Override
	public Unit getAssignedUnit(Task task) throws ModelException {
		return task.getAssignedUnit();
	}

	@Override
	public Task getAssignedTask(Unit unit) throws ModelException {
		return unit.getAssignedTask();
	}

	@Override
	public String getName(Task task) throws ModelException {
		return task.getName();
	}

	@Override
	public int getPriority(Task task) throws ModelException {
		return task.getPriority();
	}

}
