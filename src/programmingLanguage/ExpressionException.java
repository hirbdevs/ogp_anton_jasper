package programmingLanguage;

public class ExpressionException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 9L;
	
	public ExpressionException(String message){
		super(message);
	}

}
