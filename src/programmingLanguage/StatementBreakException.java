package programmingLanguage;

public class StatementBreakException extends Exception{

	/**
	 * A class that represents the Exception thrown by a break statement.
	 */
	private static final long serialVersionUID = 8L;
	
	public StatementBreakException(String message){
		super(message);
	}

}
