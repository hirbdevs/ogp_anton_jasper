package programmingLanguage;

public class StatementException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7L;
	
	public StatementException(String message){
		super(message);
	}

}
