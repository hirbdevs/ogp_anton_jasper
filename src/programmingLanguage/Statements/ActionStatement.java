package programmingLanguage.Statements;

import hillbillies.model.Task;
import hillbillies.part3.programs.SourceLocation;
import programmingLanguage.ExpressionException;
import programmingLanguage.StatementException;
import programmingLanguage.expressions.Expression;

public abstract class ActionStatement extends Statement{

	public ActionStatement(String name, Expression expression, SourceLocation source) {
		super(name, source);
		this.expression = expression;
		this.expression.setStatement(this);
	}

	Expression expression;
	
	@Override
	public abstract void execute() throws StatementException, ExpressionException;

	@Override
	public void execute(Statement statementAbove)
			throws IllegalStateException, ExpressionException, StatementException {
		this.statementAbove = statementAbove;
		this.execute();
		
	}

	@Override
	public void interruptExecution() {
		this.finished = false;	
	}
	
	@Override
	public void setTask(Task task){
		this.task = task;
		this.expression.setTask(task);
	}
	
	@Override
	public abstract boolean isWellFormed() throws ExpressionException;
	
	@Override
	public void terminate(){
		this.setTerminated(true);
		this.sourcelocation = null;
		this.setTask(null);
		
		this.expression.terminate();
		this.expression = null;
	}

}
