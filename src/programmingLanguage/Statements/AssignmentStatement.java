package programmingLanguage.Statements;

import hillbillies.model.Position;
import hillbillies.model.Task;
import hillbillies.model.Unit;
import hillbillies.part3.programs.SourceLocation;
import programmingLanguage.ExpressionException;
import programmingLanguage.StatementException;
import programmingLanguage.expressions.Expression;

public class AssignmentStatement extends Statement{

	public AssignmentStatement(String name, String variableName, Expression value, SourceLocation sourcelocation){
		super(name, sourcelocation);
		this.variableName = variableName;
		this.value = value;
	}
	
	private Expression value;
	private String variableName;
	

	public void execute() throws StatementException, IllegalStateException, ExpressionException{
		if(this.isTerminated())
			throw new IllegalStateException("Terminated statement!");
		try {
			if(value.execute() instanceof Unit){
				this.task.Variables.put(variableName, value.execute());
			}
			else if (value.execute() instanceof Position){
				this.task.Variables.put(variableName, value.execute());
				}
			else if (value.execute() instanceof Boolean){
				this.task.Variables.put(variableName, value.execute());
				}
			else throw new StatementException("not a valid value for the assignment.");
		} catch (ExpressionException e) {
			e.getCause();
		}
		this.finishStatement();
	
	}
	
	@Override
	public void setTask(Task task){
		this.task = task;
		this.value.setTask(task);
	}
	
	public void execute(Statement statementAbove) throws IllegalStateException, ExpressionException, StatementException{
		this.statementAbove = statementAbove;
		this.execute();
	}

	@Override
	public void interruptExecution() {
		this.finished = false;	
	}
	
	@Override
	public boolean isWellFormed() throws ExpressionException{
		this.task.Variables.put(variableName, this.value);
		if (value.isWellFormed())
			return true;
		else return false;
	}

	@Override	
	public void terminate() {
		this.setTerminated(true);
		this.sourcelocation = null;
		this.setTask(null);
		
		this.value.terminate();
		this.value = null;

	}
}
