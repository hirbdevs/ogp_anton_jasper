package programmingLanguage.Statements;

import hillbillies.part3.programs.SourceLocation;
import programmingLanguage.ExpressionException;
import programmingLanguage.expressions.Expression;
import programmingLanguage.expressions.ReadVariableExpression;
import programmingLanguage.expressions.UnitExpression;

public class AttackStatement extends ActionStatement{
	public AttackStatement(String name, Expression expression, SourceLocation source) {
		super(name, expression, source);
		
	}
	
	@Override
	public void execute() throws ExpressionException{
		if(this.isTerminated())
			throw new IllegalStateException("Terminated statement!");
		this.getExecutingUnit().attack(((UnitExpression)expression).execute(),this);
	}
	
	@Override
	public boolean isWellFormed() throws ExpressionException{
		if(expression instanceof ReadVariableExpression){
			if(expression.isWellFormed() && ((ReadVariableExpression)expression).isInstanceOf(this))
				return true;
			else return false;
		}
		else if(expression instanceof UnitExpression && expression.isWellFormed())
			return true;
		else return false;
	}

}
