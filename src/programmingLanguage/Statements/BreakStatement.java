package programmingLanguage.Statements;

import hillbillies.part3.programs.SourceLocation;
import programmingLanguage.ExpressionException;
import programmingLanguage.StatementException;

public class BreakStatement extends Statement{
	
	public BreakStatement(String name, SourceLocation source){
		super(name, source);	
	}
	
	@Override
	public void execute() throws IllegalStateException, StatementException, ExpressionException{
		if(this.isTerminated())
			throw new IllegalStateException("Terminated statement!");
		this.statementAbove.breakCalled = true;
		this.statementAbove.finishStatement();
	}
	
	public void execute(Statement statementAbove) throws IllegalStateException, ExpressionException, StatementException{
		this.statementAbove = statementAbove;
		this.execute();
	}

	@Override
	public void interruptExecution() {
		this.finished = false;
	}

	@Override
	public boolean isWellFormed(){
		if (this.inWhile == true)
			return true;
		else return false;
	}
}
