package programmingLanguage.Statements;

import hillbillies.part3.programs.SourceLocation;
import programmingLanguage.ExpressionException;
import programmingLanguage.StatementException;
import programmingLanguage.expressions.Expression;
import programmingLanguage.expressions.ReadVariableExpression;
import programmingLanguage.expressions.UnitExpression;

public class FollowStatement extends ActionStatement{
    public FollowStatement(String name, Expression expression, SourceLocation source) {
        super(name, expression, source);   
    }
    
    @Override
    public void execute() throws IllegalStateException, ExpressionException, StatementException{
        if(this.isTerminated())
            throw new IllegalStateException("Terminated statement!");
        this.getExecutingUnit().startFollowingUnit(((UnitExpression)expression).execute(),this);
    }

    @Override
    public boolean isWellFormed() throws ExpressionException{
        if(expression instanceof ReadVariableExpression){
            if(expression.isWellFormed() && ((ReadVariableExpression)expression).isInstanceOf(this))
                return true;
            else return false;
        }
        else if (expression instanceof UnitExpression && expression.isWellFormed())
            return true;
        else return false;
    }

}