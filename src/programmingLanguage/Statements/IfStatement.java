package programmingLanguage.Statements;

import hillbillies.model.Task;
import hillbillies.part3.programs.SourceLocation;
import programmingLanguage.ExpressionException;
import programmingLanguage.StatementException;
import programmingLanguage.expressions.Expression;

public class IfStatement extends Statement{


	public IfStatement(String name, Expression expression, Statement ifBody, Statement elseBody, SourceLocation source) {
		super(name, source);
		this.expression = expression;
		this.expression.setStatement(this);
		this.ifBody = ifBody;
		this.elseBody = elseBody;
	}
	
	Expression expression;
	Statement ifBody;
	Statement elseBody;
	

	@Override
	public void execute() throws StatementException, ExpressionException, IllegalStateException{
		if(this.isTerminated())
			throw new IllegalStateException("Terminated statement!");
		
		if ((Boolean)this.expression.execute()){
			this.ifBody.execute(this);
		}
		else{
			if(elseBody != null)
				this.elseBody.execute(this);
			else
				this.finishStatement();
		}

	}
	
	public void execute(Statement statementAbove) throws IllegalStateException, ExpressionException, StatementException{
		this.statementAbove = statementAbove;
		this.execute();
	}


	@Override
	public void interruptExecution() {
		this.finished = false;		
	}
	
	@Override
	public void setTask(Task task){
		this.task = task;
		this.expression.setTask(task);
		this.ifBody.setTask(task);
		if(this.elseBody != null)
			this.elseBody.setTask(task);
	}
	
	@Override
	public boolean isWellFormed() throws ExpressionException, StatementException{		
		if(this.inWhile){
			ifBody.inWhile = true;
			if(elseBody != null)
				elseBody.inWhile = true;
		}
		if (expression.isWellFormed() && ifBody.isWellFormed()){
			if(elseBody != null)
				if(elseBody.isWellFormed())
					return true;
				else return false;
			return true;
		}
		else return false;
	}
	
	@Override
	public void terminate() {
		this.setTerminated(true);
		this.sourcelocation = null;
		this.setTask(null);
		
		this.expression.terminate();
		this.expression = null;
		this.ifBody.terminate();
		this.ifBody = null;
		if(this.elseBody != null)
			this.elseBody.terminate();
		this.elseBody = null;
	}
}
