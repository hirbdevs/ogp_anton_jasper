package programmingLanguage.Statements;


import hillbillies.exceptions.IllegalPositionException;
import hillbillies.exceptions.UnitException;
import hillbillies.model.Position;
import hillbillies.part3.programs.SourceLocation;
import programmingLanguage.ExpressionException;
import programmingLanguage.expressions.Expression;
import programmingLanguage.expressions.PositionExpression;
import programmingLanguage.expressions.ReadVariableExpression;

public class MoveToStatement extends ActionStatement{
	public MoveToStatement(String name,Expression expression, SourceLocation source){
		super(name, expression, source);		
	}
	
	@Override
	public void execute() throws ExpressionException, IllegalStateException{
		if(this.isTerminated())
			throw new IllegalStateException("Terminated statement!");

		double x = ((Position)this.expression.execute()).getPositionX();
		double y = ((Position)this.expression.execute()).getPositionY();
		double z = ((Position)this.expression.execute()).getPositionZ();
		
		try {
			this.task.getAssignedUnit().moveTo((int)x, (int)y, (int)z, this);
		} catch (UnitException e) {
			e.printStackTrace();
		} catch (IllegalPositionException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public boolean isWellFormed() throws ExpressionException{
		if(expression instanceof ReadVariableExpression){
			if(expression.isWellFormed() && ((ReadVariableExpression)expression).isInstanceOf(this))
				return true;
			else return false;
		}
		else if(expression instanceof PositionExpression && expression.isWellFormed())
			return true;
		else return false;
	}

}
