package programmingLanguage.Statements;

import hillbillies.model.Task;
import hillbillies.part3.programs.SourceLocation;
import programmingLanguage.ExpressionException;
import programmingLanguage.StatementException;
import programmingLanguage.expressions.Expression;

public class PrintStatement extends Statement{
	public PrintStatement(String name,Expression expression, SourceLocation source){
		super(name, source);
		this.expression = expression;

	}
	
	Expression expression;
	
	@Override
	public void execute() throws ExpressionException, IllegalStateException, StatementException{
		if(this.isTerminated())
			throw new IllegalStateException("Terminated statement!");
		this.finishStatement();
		}
	
	public void execute(Statement statementAbove) throws IllegalStateException, ExpressionException, StatementException{
		this.statementAbove = statementAbove;
		this.execute();
	}

	@Override
	public void interruptExecution() {
		this.finished = false;		
	}
	
	@Override
	public void setTask(Task task){
		this.task = task;
		this.expression.setTask(task);
	}
	
	@Override
	public boolean isWellFormed() throws ExpressionException{
		if (expression.isWellFormed())
			return true;
		else return false;
	}
	
	@Override
	public void terminate() {
		this.setTerminated(true);
		this.sourcelocation = null;
		this.setTask(null);
		
		this.expression.terminate();
		this.expression = null;
	}

}
