package programmingLanguage.Statements;

import java.util.List;

import hillbillies.model.Task;
import hillbillies.part3.programs.SourceLocation;
import programmingLanguage.ExpressionException;
import programmingLanguage.StatementException;

public class SequenceStatement extends Statement{


	public SequenceStatement(String name,List<Statement> statements, SourceLocation source){
		super(name, source);
		listOfStatements = statements;
	}
	
	List<Statement> listOfStatements;
	
	public Statement currentExecuted;
	private int index;
	
	@Override
	public void execute() throws StatementException, ExpressionException, IllegalStateException{
		if(this.isTerminated())
			throw new IllegalStateException("Terminated statement!");
		if(!listOfStatements.isEmpty()){
			this.index = -1;	
			this.executeNextStatement();
		}else this.finishStatement();
	}
	
	public void execute(Statement statementAbove) throws IllegalStateException, ExpressionException, StatementException{
		this.statementAbove = statementAbove;
		this.execute();
	}
	
	public void executeNextStatement() throws StatementException, ExpressionException, IllegalStateException{
		index = index +1;
		if (index == listOfStatements.size())
            this.finishStatement();
		else{
			this.listOfStatements.get(index).execute(this);
		}    
    }

	@Override
	public void interruptExecution() {
		for (Statement current : listOfStatements){
			current.interruptExecution();
		}
		this.finished = false;
	}
	
	@Override
	public void setTask(Task task){
		this.task = task;
		for(Statement current : listOfStatements){
			current.setTask(task);
		}
	}
	
	@Override
	public boolean isWellFormed() throws ExpressionException, StatementException{
		for (Statement current : listOfStatements){
			if(this.inWhile == true)
				current.inWhile = true;
			
			if (!current.isWellFormed()){
				return false;
			}
		}
		return true;
	}
	
	@Override
	public void terminate() {
		this.setTerminated(true);
		this.sourcelocation = null;
		this.setTask(null);
		
		// terminate all statements
		for (Statement current : listOfStatements){
			current.terminate();
		}
		this.listOfStatements = null;

	} 
}
