package programmingLanguage.Statements;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Raw;
import hillbillies.model.MovementType;
import hillbillies.model.Task;
import hillbillies.model.Unit;
import hillbillies.part3.programs.SourceLocation;
import programmingLanguage.ExpressionException;
import programmingLanguage.StatementException;

public abstract class Statement {
	public Statement(String name, SourceLocation source){
		this.name = name;
		this.sourcelocation = source;
	}
	
	public SourceLocation sourcelocation;
	
	public String name;
	public Task task = null;
	public boolean inWhile = false;
	public Statement statementAbove = null;
	public boolean breakCalled = false;
	
	@Basic
	public Unit getExecutingUnit(){
		return this.task.getAssignedUnit();
	}
	
	/**
	 * Sets the task that belongs to this statement to the given statement.
	 * @param task
	 * 		The task that must belong to this statement 		
	 * @post
	 * 		The task that belongs to this statement is set to the given task.
	 */
	public void setTask(Task task){
		this.task = task;
	}
	
	public boolean finished = false;
	
	public abstract void execute() throws StatementException, ExpressionException;
	
	public abstract void execute(Statement statementAbove) throws IllegalStateException, ExpressionException, StatementException;
	

	public abstract void interruptExecution();
	
	public void finishStatement() throws StatementException, ExpressionException{
		this.finished = true;
		if (breakCalled && this.statementAbove != null) {
			if(this.statementAbove instanceof WhileStatement){
				breakCalled = false;
				this.statementAbove.breakCalled = false;
				this.statementAbove.finishStatement();
			}else{
				this.statementAbove.breakCalled = true;
				this.statementAbove.finishStatement();
			}
		}else{
		if (this.statementAbove != null) {
			if (this.statementAbove instanceof SequenceStatement) {
				((SequenceStatement) this.statementAbove).executeNextStatement();
			} else if (this.statementAbove instanceof IfStatement)
				((IfStatement) this.statementAbove).finishStatement();
			else if (this.statementAbove instanceof WhileStatement)
				((WhileStatement) this.statementAbove).execute();
			}
		else if (this.statementAbove == null) {
				this.task.getAssignedUnit().currentStatement = null;
				this.task.getAssignedUnit().setMovementType(MovementType.nothing);
				this.task.finishTask();

		} }
			
	}
	
	
	/**
	 * Boolean that indicates that a statement is a statement on it's own or if that statement is
	 * part of a sequenceStatement
	 */
	public boolean partOfSequence;

	public void terminate() {
		if(!this.isTerminated()){
			this.isTerminated = true;
			this.sourcelocation = null;
			this.setTask(null);
		}
	}
	
	/**
	 * Check whether this Statement is terminated.
	 * @return
	 */
	@Basic @Raw
	public boolean isTerminated(){
		return isTerminated;
	}
	
	/**
	 * Sets the boolean isTerminated to the given boolean.
	 * @param isTerminated
	 * @post
	 * 		isTerminated has the given boolean as its value.
	 */
	public void setTerminated(boolean isTerminated){
		this.isTerminated = isTerminated;
	}
	
	/**
	 * Variable registering whether this statement is terminated.
	 */
	private boolean isTerminated;


	public boolean isWellFormed() throws ExpressionException, StatementException {
		return true;
	}



}
