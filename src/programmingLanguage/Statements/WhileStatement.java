package programmingLanguage.Statements;

import hillbillies.model.Task;
import hillbillies.part3.programs.SourceLocation;
import programmingLanguage.ExpressionException;
import programmingLanguage.StatementException;
import programmingLanguage.expressions.Expression;

public class WhileStatement extends Statement{

	public WhileStatement(String name, Expression expression, Statement statement, SourceLocation source) {
		super(name, source);
		this.expression = expression;
		this.expression.setStatement(this);
		this.statement = statement;
	}
		
	Expression expression;
	Statement statement;
	
	@Override
	public void execute() throws StatementException, ExpressionException, IllegalStateException{
		if(this.isTerminated())
			throw new IllegalStateException("Terminated statement!");
		boolean condition = (Boolean)this.expression.execute();
		if(condition){
				this.statement.execute(this);	
			}
		else {
			this.statement.statementAbove = null;
			this.finishStatement();
			}
	}
	
	public void execute(Statement statementAbove) throws IllegalStateException, ExpressionException, StatementException{
		this.statementAbove = statementAbove;
		this.execute();
	}

	@Override
	public void interruptExecution() {
		this.finished = false;		
	}
	
	@Override
	public void setTask(Task task){
		this.task = task;
		this.expression.setTask(task);
		this.statement.setTask(task);
	}
	
	@Override
	public boolean isWellFormed() throws ExpressionException, StatementException{
		statement.inWhile = true;
		if (expression.isWellFormed() && statement.isWellFormed())
			return true;
		else return false;
	}
	
	@Override
	public void terminate() {
		this.setTerminated(true);
		this.sourcelocation = null;
		this.setTask(null);
		
		this.expression.terminate();
		this.expression = null;
		this.statement.terminate();
		this.statement = null;
	}
}
