package programmingLanguage.Statements;

import hillbillies.exceptions.UnitException;
import hillbillies.model.Position;
import hillbillies.part3.programs.SourceLocation;
import programmingLanguage.ExpressionException;
import programmingLanguage.expressions.Expression;
import programmingLanguage.expressions.PositionExpression;
import programmingLanguage.expressions.ReadVariableExpression;

public class WorkStatement extends ActionStatement{
	public WorkStatement(String name, Expression expression, SourceLocation source) {
		super(name, expression, source);
	}
	
	@Override
	public void execute() throws ExpressionException, IllegalStateException{
		if(this.isTerminated())
			throw new IllegalStateException("Terminated statement!");
		
		int x = ((int)((Position)(this.expression.execute())).getPositionX());
		int y = ((int)((Position)(this.expression.execute())).getPositionY());
		int z = ((int)((Position)(this.expression.execute())).getPositionZ());
		
		try {
			this.getExecutingUnit().workAt(x, y, z, this);
		} catch (UnitException e) {
			e.printStackTrace();
		}		
	}
	
	@Override
	public boolean isWellFormed() throws ExpressionException{
		if (expression instanceof ReadVariableExpression){
			if(expression.isWellFormed() && ((ReadVariableExpression)expression).isInstanceOf(this))
				return true;
			else return false;
		}
		else if (expression instanceof PositionExpression && expression.isWellFormed())
			return true;
		else return false;
	}
	
}
