package programmingLanguage;

import java.util.ArrayList;
import java.util.List;

import hillbillies.exceptions.TaskException;
import hillbillies.model.Task;
import hillbillies.part3.programs.ITaskFactory;
import hillbillies.part3.programs.SourceLocation;
import programmingLanguage.Statements.AssignmentStatement;
import programmingLanguage.Statements.AttackStatement;
import programmingLanguage.Statements.BreakStatement;
import programmingLanguage.Statements.FollowStatement;
import programmingLanguage.Statements.IfStatement;
import programmingLanguage.Statements.MoveToStatement;
import programmingLanguage.Statements.PrintStatement;
import programmingLanguage.Statements.SequenceStatement;
import programmingLanguage.Statements.Statement;
import programmingLanguage.Statements.WhileStatement;
import programmingLanguage.Statements.WorkStatement;
import programmingLanguage.expressions.AndExpression;
import programmingLanguage.expressions.AnyUnitExpression;
import programmingLanguage.expressions.BoulderPositionExpression;
import programmingLanguage.expressions.Carries_Item;
import programmingLanguage.expressions.EnemyUnitExpression;
import programmingLanguage.expressions.Expression;
import programmingLanguage.expressions.FalseExpression;
import programmingLanguage.expressions.FriendUnitExpression;
import programmingLanguage.expressions.HerePositionExpression;
import programmingLanguage.expressions.Is_Alive;
import programmingLanguage.expressions.Is_Enemy;
import programmingLanguage.expressions.Is_Friend;
import programmingLanguage.expressions.Is_Passable;
import programmingLanguage.expressions.Is_Solid;
import programmingLanguage.expressions.LiteralPositionExpression;
import programmingLanguage.expressions.LogPositionExpression;
import programmingLanguage.expressions.Next_ToExpression;
import programmingLanguage.expressions.NotExpression;
import programmingLanguage.expressions.OrExpression;
import programmingLanguage.expressions.PositionOfExpression;
import programmingLanguage.expressions.ReadVariableExpression;
import programmingLanguage.expressions.SelectedPositionExpression;
import programmingLanguage.expressions.ThisUnitExpression;
import programmingLanguage.expressions.TrueExpression;
import programmingLanguage.expressions.WorkshopPositionExpression;


public class TaskFactory implements ITaskFactory<Expression, Statement, Task>{

	@Override
	public List<Task> createTasks(String name, int priority, Statement activity, List<int[]> selectedCubes) {
		List<Task> result;
		if (!selectedCubes.isEmpty()) {
			result = new ArrayList<Task>();
			for (int i = 0; i < selectedCubes.size(); i++) {
				try {
					Task task = new Task(name, priority, activity, selectedCubes.get(i));
					result.add(task);
				} catch (TaskException | ExpressionException | StatementException e) {
					e.getMessage();
				}
			} 
		}
		else {
			result = new ArrayList<Task>();
			Task task;
			try {
				task = new Task(name, priority, activity, null);
				result.add(task);
			} catch (TaskException | ExpressionException | StatementException e) {				
				e.getMessage();
			}
			
		}
		return result;
	}

	@Override
	public Statement createAssignment(String variableName, Expression value, SourceLocation sourceLocation) {
		return new AssignmentStatement("Assignment", variableName, value, sourceLocation);
	}

	@Override
	public Statement createWhile(Expression condition, Statement body, SourceLocation sourceLocation) {
		return new WhileStatement("while", condition, body, sourceLocation);
	}

	@Override
	public Statement createIf(Expression condition, Statement ifBody, Statement elseBody,
			SourceLocation sourceLocation) {
		
		return new IfStatement("if", condition, ifBody, elseBody, sourceLocation);
	}

	@Override
	public Statement createBreak(SourceLocation sourceLocation) {
		return new BreakStatement("Break", sourceLocation);
	}

	@Override
	public Statement createPrint(Expression value, SourceLocation sourceLocation) {
		return new PrintStatement("print", value, sourceLocation);
	}

	@Override
	public Statement createSequence(List<Statement> statements, SourceLocation sourceLocation) {
		return new SequenceStatement("sequence", statements, sourceLocation);
	}

	@Override
	public Statement createMoveTo(Expression position, SourceLocation sourceLocation) {
		return new MoveToStatement("moveTo", position, sourceLocation);
	}

	@Override
	public Statement createWork(Expression position, SourceLocation sourceLocation) {
		return new WorkStatement("workAT", position, sourceLocation);
	}

	@Override
	public Statement createFollow(Expression unit, SourceLocation sourceLocation) {
		return new FollowStatement("follow", unit, sourceLocation);
	}

	@Override
	public Statement createAttack(Expression unit, SourceLocation sourceLocation) {
		return new AttackStatement("attack", unit, sourceLocation);
	}

	@Override
	public Expression createReadVariable(String variableName, SourceLocation sourceLocation) {
		return new ReadVariableExpression(variableName, "ReadVariable", sourceLocation);
	}

	@Override
	public Expression createIsSolid(Expression position, SourceLocation sourceLocation) {
		return new Is_Solid(position, "is_solid", sourceLocation);
	}

	@Override
	public Expression createIsPassable(Expression position, SourceLocation sourceLocation) {
		
		return new Is_Passable(position, "is_passable", sourceLocation);
	}

	@Override
	public Expression createIsFriend(Expression unit, SourceLocation sourceLocation) {
		
		return new Is_Friend(unit, "is_friend", sourceLocation);
	}

	@Override
	public Expression createIsEnemy(Expression unit, SourceLocation sourceLocation) {
		
		return new Is_Enemy(unit, "is_enemy", sourceLocation);
	}

	@Override
	public Expression createIsAlive(Expression unit, SourceLocation sourceLocation) {
		
		return new Is_Alive(unit, "is_alive", sourceLocation);
	}

	@Override
	public Expression createCarriesItem(Expression unit, SourceLocation sourceLocation) {
		return new Carries_Item(unit, "carries_item", sourceLocation);
	}

	@Override
	public Expression createNot(Expression expression, SourceLocation sourceLocation) {
		return new NotExpression(expression, "not", sourceLocation);
	}

	@Override
	public Expression createAnd(Expression left, Expression right, SourceLocation sourceLocation) {
		return new AndExpression(left, right, "and", sourceLocation);
	}

	@Override
	public Expression createOr(Expression left, Expression right, SourceLocation sourceLocation) {
		return new OrExpression(left, right, "or", sourceLocation);
	}

	@Override
	public Expression createHerePosition(SourceLocation sourceLocation) {
		return new HerePositionExpression("here", sourceLocation);
	}

	@Override
	public Expression createLogPosition(SourceLocation sourceLocation) {
		return new LogPositionExpression("log", sourceLocation);
	}

	@Override
	public Expression createBoulderPosition(SourceLocation sourceLocation) {
		return new BoulderPositionExpression("boulder", sourceLocation);
	}

	@Override
	public Expression createWorkshopPosition(SourceLocation sourceLocation) {
		return new WorkshopPositionExpression("workshop", sourceLocation);
	}

	@Override
	public Expression createSelectedPosition(SourceLocation sourceLocation) {
		return new SelectedPositionExpression("selected_position", sourceLocation);
	}

	@Override
	public Expression createNextToPosition(Expression position, SourceLocation sourceLocation) {
		return new Next_ToExpression(position, "next_to", sourceLocation);
	}

	@Override
	public Expression createLiteralPosition(int x, int y, int z, SourceLocation sourceLocation) {
		return new LiteralPositionExpression(x, y, z, "literal_position", sourceLocation);
	}

	@Override
	public Expression createThis(SourceLocation sourceLocation) {
		return new ThisUnitExpression("this", sourceLocation);
	}

	@Override
	public Expression createFriend(SourceLocation sourceLocation) {
		return new FriendUnitExpression("friend", sourceLocation);
	}

	@Override
	public Expression createEnemy(SourceLocation sourceLocation) {
		return new EnemyUnitExpression("enemy", sourceLocation);
	}

	@Override
	public Expression createAny(SourceLocation sourceLocation) {
		return new AnyUnitExpression("any", sourceLocation);
	}

	@Override
	public Expression createTrue(SourceLocation sourceLocation) {
		return new TrueExpression("true", sourceLocation);
	}

	@Override
	public Expression createFalse(SourceLocation sourceLocation) {
		return new FalseExpression("false", sourceLocation);
	}

	@Override
	public Expression createPositionOf(Expression unit, SourceLocation sourceLocation) {
		return new PositionOfExpression(unit, "position_of", sourceLocation);
	}
	
	
}	