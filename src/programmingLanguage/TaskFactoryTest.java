package programmingLanguage;

import static org.junit.Assert.*;
import java.util.Collections;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import hillbillies.model.Task;
import hillbillies.model.Unit;
import hillbillies.model.World;
import hillbillies.part2.listener.DefaultTerrainChangeListener;
import hillbillies.part3.facade.Facade;
import hillbillies.part3.programs.TaskParser;
import ogp.framework.util.ModelException;

public class TaskFactoryTest {
	
	private Facade facade;

	private static final int TYPE_ROCK = 1;
	private static final int TYPE_TREE = 2;
	private static final int TYPE_WORKSHOP = 3;

	@Before
	public void setUp() throws Exception {
		this.facade = new Facade();
	}

	@Test
	public void createMoveTo() throws ModelException{
	int[][][] types = new int[3][3][3];
	types[1][1][0] = TYPE_ROCK;
	types[1][1][1] = TYPE_ROCK;
	types[1][1][2] = TYPE_TREE;
	types[2][2][2] = TYPE_WORKSHOP;

	World world = facade.createWorld(types, new DefaultTerrainChangeListener());
	Unit unit = facade.createUnit("Test", new int[] { 0, 0, 0 }, 50, 50, 50, 50, true);
	facade.addUnit(unit, world);
	
	List<Task> tasks = TaskParser.parseTasksFromString("name: \"moveTo task\"\npriority: 10\nactivities: moveTo selected;", facade.createTaskFactory(), 
													Collections.singletonList(new int[] { 2,2,2 }));
	
	assertNotNull(tasks);
	
	assertEquals(1, tasks.size());
	
	Task task = tasks.get(0);
	assertEquals("moveTo task", facade.getName(task));
	assertEquals(10, facade.getPriority(task));
	
	}
	
	@Test
	public void createWork() throws ModelException{
		int[][][] types = new int[3][3][3];
		types[1][1][0] = TYPE_ROCK;
		types[1][1][1] = TYPE_ROCK;
		types[1][1][2] = TYPE_TREE;
		types[2][2][2] = TYPE_WORKSHOP;

		World world = facade.createWorld(types, new DefaultTerrainChangeListener());
		Unit unit = facade.createUnit("Test", new int[] { 0, 0, 0 }, 50, 50, 50, 50, true);
		facade.addUnit(unit, world);
		
		List<Task> tasks = TaskParser.parseTasksFromString("name: \"work task\"\npriority: 11\nactivities: work selected;", facade.createTaskFactory(), 
														Collections.singletonList(new int[] { 1,1,1 }));
		
		assertNotNull(tasks);
		
		assertEquals(1, tasks.size());
		
		Task task = tasks.get(0);
		assertEquals("work task", facade.getName(task));
		assertEquals(11, facade.getPriority(task));

	}
	
	@Test
	public void createSequence() throws ModelException{
		int[][][] types = new int[3][3][3];
		types[1][1][0] = TYPE_ROCK;
		types[1][1][1] = TYPE_ROCK;
		types[1][1][2] = TYPE_TREE;
		types[2][2][2] = TYPE_WORKSHOP;

		World world = facade.createWorld(types, new DefaultTerrainChangeListener());
		Unit unit = facade.createUnit("Test", new int[] { 0, 0, 0 }, 50, 50, 50, 50, true);
		facade.addUnit(unit, world);
		
		List<Task> tasks = TaskParser.parseTasksFromString("name: \"move and dig\"\npriority: 11\nactivities: if is_solid(selected) then moveTo (next_to selected); work selected; fi", facade.createTaskFactory(), 
														Collections.singletonList(new int[] { 1,1,0 }));
		
		assertNotNull(tasks);
		
		assertEquals(1, tasks.size());
		
		Task task = tasks.get(0);
		assertEquals("move and dig", facade.getName(task));
		assertEquals(11, facade.getPriority(task));
	}
	
	@Test
	public void createFollow() throws ModelException{
		int[][][] types = new int[3][3][3];
		types[1][1][0] = TYPE_ROCK;
		types[1][1][1] = TYPE_ROCK;
		types[1][1][2] = TYPE_TREE;
		types[2][2][2] = TYPE_WORKSHOP;

		World world = facade.createWorld(types, new DefaultTerrainChangeListener());
		Unit unit = facade.createUnit("Test", new int[] { 0, 0, 0 }, 50, 50, 50, 50, true);
		facade.addUnit(unit, world);
		
		List<Task> tasks = TaskParser.parseTasksFromString("name: \"follow task\"\npriority: 11\nactivities: follow enemy;", facade.createTaskFactory(), 
														Collections.singletonList(new int[] { 1,1,0 }));
		
		assertNotNull(tasks);
		
		assertEquals(1, tasks.size());
		
		Task task = tasks.get(0);
		assertEquals("follow task", facade.getName(task));
		assertEquals(11, facade.getPriority(task));
	}
	
	@Test
	public void createAttack() throws ModelException{
		int[][][] types = new int[3][3][3];
		types[1][1][0] = TYPE_ROCK;
		types[1][1][1] = TYPE_ROCK;
		types[1][1][2] = TYPE_TREE;
		types[2][2][2] = TYPE_WORKSHOP;

		World world = facade.createWorld(types, new DefaultTerrainChangeListener());
		Unit unit = facade.createUnit("Test", new int[] { 0, 0, 0 }, 50, 50, 50, 50, true);
		facade.addUnit(unit, world);
		
		List<Task> tasks = TaskParser.parseTasksFromString("name: \"attack task\"\npriority: 11\nactivities: attack enemy;", facade.createTaskFactory(), 
														Collections.singletonList(new int[] { 1,1,0 }));
		
		assertNotNull(tasks);
		
		assertEquals(1, tasks.size());
		
		Task task = tasks.get(0);
		assertEquals("attack task", facade.getName(task));
		assertEquals(11, facade.getPriority(task));
	}
	
	@Test
	public void createOperateWorkshop() throws ModelException{
		int[][][] types = new int[3][3][3];
		types[1][1][0] = TYPE_ROCK;
		types[1][1][1] = TYPE_ROCK;
		types[1][1][2] = TYPE_TREE;
		types[2][2][2] = TYPE_WORKSHOP;

		World world = facade.createWorld(types, new DefaultTerrainChangeListener());
		Unit unit = facade.createUnit("Test", new int[] { 0, 0, 0 }, 50, 50, 50, 50, true);
		facade.addUnit(unit, world);
		
		List<Task> tasks = TaskParser.parseTasksFromString("name: \"operate_workshop\"\npriority: -100\nactivities: w := workshop; moveTo boulder; work here; moveTo w; work here; moveTo w; work here; work here;", facade.createTaskFactory(), 
														Collections.singletonList(new int[] { 1,1,0 }));
		
		assertNotNull(tasks);
		
		assertEquals(1, tasks.size());
		
		Task task = tasks.get(0);
		assertEquals("operate_workshop", facade.getName(task));
		assertEquals(-100, facade.getPriority(task));
	}
	
	@Test
	public void createDigTunnel() throws ModelException{
		int[][][] types = new int[3][3][3];
		types[1][1][0] = TYPE_ROCK;
		types[1][1][1] = TYPE_ROCK;
		types[1][1][2] = TYPE_TREE;
		types[2][2][2] = TYPE_WORKSHOP;

		World world = facade.createWorld(types, new DefaultTerrainChangeListener());
		Unit unit = facade.createUnit("Test", new int[] { 0, 0, 0 }, 50, 50, 50, 50, true);
		facade.addUnit(unit, world);
		
		List<Task> tasks = TaskParser.parseTasksFromString("name: \"digtunnel_if\"\npriority: 1000\nactivities: if(carries_item(this)) then work here; fi if (is_solid(1,1,1)) then moveTo(0,0,0); work (1,1,1); fi if(is_solid(1,2,1)) then moveTo (1,1,1); work (1,2,1); fi", facade.createTaskFactory(), 
														Collections.singletonList(new int[] { 1,1,0 }));
		
		assertNotNull(tasks);
		
		assertEquals(1, tasks.size());
		
		Task task = tasks.get(0);
		assertEquals("digtunnel_if", facade.getName(task));
		assertEquals(1000, facade.getPriority(task));
	}
	
	@Test
	public void createTaskIllegal_illegalBreak() throws ModelException{
		int[][][] types = new int[3][3][3];
		types[1][1][0] = TYPE_ROCK;
		types[1][1][1] = TYPE_ROCK;
		types[1][1][2] = TYPE_TREE;
		types[2][2][2] = TYPE_WORKSHOP;

		World world = facade.createWorld(types, new DefaultTerrainChangeListener());
		Unit unit = facade.createUnit("Test", new int[] { 0, 0, 0 }, 50, 50, 50, 50, true);
		facade.addUnit(unit, world);
		
		List<Task> tasks = TaskParser.parseTasksFromString("name: \"illegal task\"\npriority: -100\nactivities: break;", facade.createTaskFactory(), 
														Collections.singletonList(new int[] { 1,1,0 }));
		
		assertTrue(tasks.isEmpty());
	}	
	
	@Test
	public void createTaskIllegal_ReadVariableBeforeAssignMent() throws ModelException{
		int[][][] types = new int[3][3][3];
		types[1][1][0] = TYPE_ROCK;
		types[1][1][1] = TYPE_ROCK;
		types[1][1][2] = TYPE_TREE;
		types[2][2][2] = TYPE_WORKSHOP;

		World world = facade.createWorld(types, new DefaultTerrainChangeListener());
		Unit unit = facade.createUnit("Test", new int[] { 0, 0, 0 }, 50, 50, 50, 50, true);
		facade.addUnit(unit, world);
		
		List<Task> tasks = TaskParser.parseTasksFromString("name: \"illegal task\"\npriority: -100\nactivities: moveTo w; w := workshop;", facade.createTaskFactory(), 
														Collections.singletonList(new int[] { 1,1,0 }));
		
		assertTrue(tasks.isEmpty());
	}
	
	@Test
	public void createTaskIllegal_TypeCastError() throws ModelException{
		int[][][] types = new int[3][3][3];
		types[1][1][0] = TYPE_ROCK;
		types[1][1][1] = TYPE_ROCK;
		types[1][1][2] = TYPE_TREE;
		types[2][2][2] = TYPE_WORKSHOP;

		World world = facade.createWorld(types, new DefaultTerrainChangeListener());
		Unit unit = facade.createUnit("Test", new int[] { 0, 0, 0 }, 50, 50, 50, 50, true);
		facade.addUnit(unit, world);
		
		List<Task> tasks = TaskParser.parseTasksFromString("name: \"illegal task\"\npriority: -100\nactivities: moveTo (friend);", facade.createTaskFactory(), 
														Collections.singletonList(new int[] { 1,1,0 }));
		
		assertTrue(tasks.isEmpty());
	}


}
