package programmingLanguage.expressions;

import hillbillies.exceptions.TaskException;
import hillbillies.exceptions.UnitException;
import hillbillies.model.Unit;
import hillbillies.part3.programs.SourceLocation;
import programmingLanguage.ExpressionException;

public class AnyUnitExpression extends UnitExpression{

	public AnyUnitExpression(String name, SourceLocation source) {
		super(name, source);
	}
	
	@Override
	public Unit execute() throws IllegalStateException, ExpressionException{
		if(this.isTerminated())
			throw new IllegalStateException("Terminated expression!");
		try {
			return this.task.getAssignedUnit().getAnyUnit();
		} catch (TaskException | UnitException e) {
			throw new ExpressionException(e.getMessage());
		} 
	}

}
