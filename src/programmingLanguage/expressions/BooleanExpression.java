package programmingLanguage.expressions;

import hillbillies.model.Task;
import hillbillies.part3.programs.SourceLocation;
import programmingLanguage.ExpressionException;

public abstract class BooleanExpression extends Expression{
	public BooleanExpression(Expression expression, String name, SourceLocation source){
		super(name, source);
		this.expression = (Expression) expression;
	}
	public Expression expression;
	
	@Override
	public abstract Boolean execute() throws ExpressionException;
	
	@Override
	public void setTask(Task task){
		this.task = task;
		this.expression.setTask(task);
	}
	
	@Override
	public void terminate() {
		if(!this.isTerminated()){
			this.setTerminated(true);
			this.sourceLocation = null;
			this.statement = null;
			
			this.expression.terminate();
			this.expression = null;
		}	
	}
}
