package programmingLanguage.expressions;

import hillbillies.model.Unit;
import hillbillies.part3.programs.SourceLocation;
import programmingLanguage.ExpressionException;

public class Carries_Item extends BooleanExpression{

	public Carries_Item(Expression expression, String name, SourceLocation source) {
		super(expression, name, source);
	}

	@Override
	public Boolean execute() throws ExpressionException, IllegalStateException{
		if(this.isTerminated())
			throw new IllegalStateException("Terminated expression!");

		if(((Unit)this.expression.execute()).isCarryingMaterial())
			return true;
		else return false;
	}
	
	@Override
	public boolean isWellFormed() throws ExpressionException{
		if(this.expression instanceof UnitExpression)
			return true;
		else return false;
	}
}
