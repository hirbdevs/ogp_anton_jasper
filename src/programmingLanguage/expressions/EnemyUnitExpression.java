package programmingLanguage.expressions;

import hillbillies.exceptions.TaskException;
import hillbillies.exceptions.UnitException;
import hillbillies.model.Unit;
import hillbillies.part3.programs.SourceLocation;
import programmingLanguage.ExpressionException;

public class EnemyUnitExpression extends UnitExpression{

	public EnemyUnitExpression(String name, SourceLocation source) {
		super(name, source);
	}
	
	@Override
	public Unit execute() throws IllegalStateException, ExpressionException{
		if(this.isTerminated())
			throw new IllegalStateException("Terminated expression!");
		try {
			return this.task.getAssignedUnit().getEnemy();
		} catch (TaskException | UnitException e) {
			throw new ExpressionException(e.getMessage());
		}
	}

}
