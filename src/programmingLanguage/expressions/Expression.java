package programmingLanguage.expressions;

import be.kuleuven.cs.som.annotate.Basic;
import be.kuleuven.cs.som.annotate.Raw;
import hillbillies.model.Task;
import hillbillies.part3.programs.SourceLocation;
import programmingLanguage.ExpressionException;
import programmingLanguage.Statements.Statement;

public abstract class Expression {

	public Expression(String name, SourceLocation sourcelocation){
		this.name = name;	
		this.sourceLocation = sourcelocation;
	}
	
	public SourceLocation sourceLocation;
	
	public void setStatement(Statement statement){
		this.statement = statement;
	}
	
	public Statement statement;
	
	public String name;
	
	public Task task;
	
	public abstract Object execute() throws ExpressionException;
	
	public void setTask(Task task){
		this.task = task;
	}
	
	public boolean isWellFormed() throws ExpressionException{
		return true;
	}
	
	public void terminate() {
		if(!this.isTerminated()){
			this.isTerminated = true;
			this.sourceLocation = null;
			this.statement = null;
			
		}	
	}
	
	/**
	 * Check whether this expression is terminated.
	 * @return
	 */
	@Basic @Raw
	public boolean isTerminated(){
		return isTerminated;
	}
	
	/**
	 * 
	 * @param isTerminated
	 */
	//TODO
	public void setTerminated(boolean isTerminated){
		this.isTerminated = isTerminated;
	}
	
	/**
	 * Variable registering whether this Expression is terminated.
	 */
	private boolean isTerminated;
}
