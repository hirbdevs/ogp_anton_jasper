package programmingLanguage.expressions;

import hillbillies.part3.programs.SourceLocation;

public class FalseExpression extends Expression{

	public FalseExpression(String name, SourceLocation source) {
		super(name, source);
	}
	
	@Override
	public Boolean execute() throws IllegalStateException{
		if(this.isTerminated())
			throw new IllegalStateException("Terminated expression!");
		return false;
	}

}
