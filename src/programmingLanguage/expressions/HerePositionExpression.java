package programmingLanguage.expressions;

import hillbillies.model.Position;
import hillbillies.part3.programs.SourceLocation;

public class HerePositionExpression extends PositionExpression{

	public HerePositionExpression(String name, SourceLocation source) {
		super(name, source);
		
	}

	@Override
	public Position execute() throws IllegalStateException {
		if(this.isTerminated())
			throw new IllegalStateException("Terminated expression!");
		return this.task.getAssignedUnit().getPositionGameObject();
	}
}
