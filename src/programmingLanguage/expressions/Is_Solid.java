package programmingLanguage.expressions;

import hillbillies.model.Position;
import hillbillies.model.World;
import hillbillies.part3.programs.SourceLocation;
import programmingLanguage.ExpressionException;

public class Is_Solid extends BooleanExpression{

	public Is_Solid(Expression position, String name, SourceLocation source) {
		super(position, name, source);
		if (position instanceof PositionExpression)
			System.out.println("");
			
	}
	
	@Override
	public Boolean execute() throws ExpressionException, IllegalStateException{
		if(this.isTerminated())
			throw new IllegalStateException("Terminated expression!");
		if (World.getInstance().getCubeByPosition((Position)this.expression.execute()).isSolidCube())
			return true;
		else return false;
	}
	
	@Override
	public boolean isWellFormed() throws ExpressionException{
		if(this.expression instanceof PositionExpression)
			return true;
		else return false;
	}

}
