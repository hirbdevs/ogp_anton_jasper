package programmingLanguage.expressions;

import hillbillies.exceptions.IllegalPositionException;
import hillbillies.model.Position;
import hillbillies.part3.programs.SourceLocation;

public class LiteralPositionExpression extends PositionExpression{

	public LiteralPositionExpression(int x, int y, int z, String name, SourceLocation source){
		super(name, source);
		try {
			this.position = new Position((double)x, (double)y, (double)z);
		} catch (IllegalPositionException e) {
			e.printStackTrace();
		}
	}
	
	public Position position;
	
	@Override
	public Position execute() throws IllegalStateException{
		if(this.isTerminated())
			throw new IllegalStateException("Terminated expression!");
		return this.position;
	}
	
	@Override
	public void terminate() {
		if(!this.isTerminated()){
			this.setTerminated(true);
			this.sourceLocation = null;
			this.statement = null;
			
			this.position = null;
		}	
	}
}
