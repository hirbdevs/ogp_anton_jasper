package programmingLanguage.expressions;

import hillbillies.exceptions.TaskException;
import hillbillies.exceptions.UnitException;
import hillbillies.model.Position;
import hillbillies.part3.programs.SourceLocation;
import programmingLanguage.ExpressionException;

public class LogPositionExpression extends PositionExpression{

	public LogPositionExpression(String name, SourceLocation source) {
		super(name, source);
	}
	
	@Override
	public Position execute() throws IllegalStateException, ExpressionException {
		if(this.isTerminated())
			throw new IllegalStateException("Terminated expression!");
		try {
			return this.task.getAssignedUnit().getClosestLog().getPositionGameObject();
		} catch (UnitException e) {
			throw new ExpressionException(e.getMessage());
		} catch (TaskException e) {
			throw new ExpressionException(e.getMessage());
		} 
	}
}
