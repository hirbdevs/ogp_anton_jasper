package programmingLanguage.expressions;

import hillbillies.model.Cube;
import hillbillies.model.Position;
import hillbillies.model.Task;
import hillbillies.model.World;
import hillbillies.part3.programs.SourceLocation;
import programmingLanguage.ExpressionException;

public class Next_ToExpression extends PositionExpression{

	public Next_ToExpression(Expression position, String name, SourceLocation source) {
		super(name, source);
		this.position = (PositionExpression) position;
	}
	
	PositionExpression position;

	@Override
	public Position execute() throws ExpressionException, IllegalStateException{
		if(this.isTerminated())
			throw new IllegalStateException("Terminated expression!");
		Cube cube = World.getInstance().getCubeByPosition((Position)position.execute());
		
		for(Cube neighbour : World.getInstance().getNeighbouringCubes(cube))
			if(neighbour.isPassableCube()){
				return neighbour.getPosition();}
		return null;
	}
	
	@Override
	public void setTask(Task task){
		this.task = task;
		this.position.setTask(task);
	}
	
	@Override
	public boolean isWellFormed() throws ExpressionException{
		if(position.execute() instanceof Position)
			return true;
		else return false;
	}
	
	@Override
	public void terminate() {
		if(!this.isTerminated()){
			this.setTerminated(true);
			this.sourceLocation = null;
			this.statement = null;
			
			this.position.terminate();
			this.position = null;
		}	
	}
}
