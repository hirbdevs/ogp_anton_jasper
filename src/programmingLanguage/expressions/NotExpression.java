package programmingLanguage.expressions;

import hillbillies.model.Task;
import hillbillies.part3.programs.SourceLocation;
import programmingLanguage.ExpressionException;

public class NotExpression extends BooleanExpression{

	public NotExpression(Expression expression, String name, SourceLocation source) {
		super(expression, name, source);		
	}

	@Override
	public Boolean execute() throws ExpressionException, IllegalStateException{
		if(this.isTerminated())
			throw new IllegalStateException("Terminated expression!");
		if((Boolean)this.expression.execute())
			return false;
		else return true;
	}
	
	@Override
	public void setTask(Task task){
		this.task = task;
		this.expression.setTask(task);
	}
	
	@Override
	public boolean isWellFormed() throws ExpressionException{
		if (this.expression instanceof BooleanExpression)
			return true;
		else return false;
	}
}
