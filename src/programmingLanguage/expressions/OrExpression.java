package programmingLanguage.expressions;

import hillbillies.model.Task;
import hillbillies.part3.programs.SourceLocation;
import programmingLanguage.ExpressionException;

public class OrExpression extends BooleanExpression{

	public OrExpression(Expression expression1,Expression expression2, String name, SourceLocation source) {
		super(expression1, name, source);
		this.expression2 = (Expression) expression2;
	}

	public Expression expression2;
	
	@Override
	public Boolean execute() throws ExpressionException, IllegalStateException{
		if(this.isTerminated())
			throw new IllegalStateException("Terminated expression!");
		if(((Boolean)this.expression.execute()) || ((Boolean)this.expression2.execute()))
			return true;
		else return false;
	}
	
	
	
	@Override
	public void setTask(Task task){
		this.task = task;
		this.expression.setTask(task);
		this.expression2.setTask(task);
	}
	
	@Override
	public boolean isWellFormed() throws ExpressionException{
		if(this.expression instanceof BooleanExpression && this.expression2 instanceof BooleanExpression)
			return true;
		else return false;
	}
	
	@Override
	public void terminate() {
		if(!this.isTerminated()){
			this.setTerminated(true);
			this.sourceLocation = null;
			this.statement = null;
			
			this.expression2.terminate();
			this.expression2 = null;
		}	
	}
}
