package programmingLanguage.expressions;

import hillbillies.model.Position;
import hillbillies.part3.programs.SourceLocation;
import programmingLanguage.ExpressionException;

public abstract class PositionExpression extends Expression {

	public PositionExpression(String name, SourceLocation source) {
		super(name, source);
		
	}

	@Override
	public abstract Position execute() throws ExpressionException;
}
