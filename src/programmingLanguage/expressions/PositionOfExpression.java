package programmingLanguage.expressions;

import hillbillies.model.Position;
import hillbillies.model.Task;
import hillbillies.model.Unit;
import hillbillies.part3.programs.SourceLocation;
import programmingLanguage.ExpressionException;

public class PositionOfExpression extends PositionExpression{

	public PositionOfExpression(Expression unit, String name, SourceLocation source) {
		super(name, source);
		this.unit = unit;
	}

	public Expression unit;
	
	@Override
	public Position execute() throws ExpressionException, IllegalStateException {
		if(this.isTerminated())
			throw new IllegalStateException("Terminated expression!");
		return ((Unit)this.unit.execute()).getPositionGameObject();
	}
	
	@Override
	public void terminate() {
		if(!this.isTerminated()){
			this.setTerminated(true);
			this.sourceLocation = null;
			this.statement = null;
			this.statement.terminate();
			
			this.unit.terminate();
			this.unit = null;
		}	
	}
	
	@Override
	public void setTask(Task task){
		this.task = task;
		this.unit.setTask(task);
	}

	@Override
	public boolean isWellFormed() throws ExpressionException{
		if (unit instanceof UnitExpression)
			return true;
		else return false;
	}
}
