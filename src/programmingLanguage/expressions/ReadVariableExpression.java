package programmingLanguage.expressions;

import hillbillies.part3.programs.SourceLocation;
import programmingLanguage.ExpressionException;
import programmingLanguage.Statements.AttackStatement;
import programmingLanguage.Statements.FollowStatement;
import programmingLanguage.Statements.MoveToStatement;
import programmingLanguage.Statements.Statement;
import programmingLanguage.Statements.WorkStatement;

public class ReadVariableExpression extends Expression{

	public ReadVariableExpression(String variable, String name, SourceLocation sourcelocation) {
		super(name, sourcelocation);
		this.variable = variable;
	}
	
	public String variable;

	@Override
	public Object execute() throws ExpressionException, IllegalStateException {
		if(this.isTerminated())
			throw new IllegalStateException("Terminated expression!");
		Object result = null;
		for(String variableName : this.task.Variables.keySet()){
			if (variableName.equals(variable)){
					result = this.task.Variables.get(variableName);
		}}
		if(result != null) return result;
		else 
			throw new ExpressionException("No such variable in the Task.");
	}

	@Override
	public boolean isWellFormed() throws ExpressionException{
		for(String name : this.task.Variables.keySet())
			if(name.equals(variable))
				return true;
		return false;
	}
	
	public boolean isInstanceOf(Statement statement){
		for(String name : this.task.Variables.keySet())
			if(name.equals(variable)){
				if(statement instanceof MoveToStatement){
					if (this.task.Variables.get(name) instanceof PositionExpression)
						return true;}
				else if(statement instanceof WorkStatement){
					if(this.task.Variables.get(name) instanceof PositionExpression)
						return true;}
				else if(statement instanceof AttackStatement){
					if(this.task.Variables.get(name) instanceof UnitExpression)
						return true;}
				else if (statement instanceof FollowStatement){
					if(this.task.Variables.get(name) instanceof UnitExpression)
						return true;}
				else
					return false;
			}else return false;
		return false;
	}
}
