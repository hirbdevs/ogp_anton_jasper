package programmingLanguage.expressions;

import hillbillies.exceptions.IllegalPositionException;
import hillbillies.model.Position;
import hillbillies.part3.programs.SourceLocation;

public class SelectedPositionExpression extends PositionExpression{

	public SelectedPositionExpression(String name, SourceLocation source) {
		super(name, source);
	}

	@Override
	public Position execute() throws IllegalStateException{
		if(this.isTerminated())
			throw new IllegalStateException("Terminated expression!");
		int x = this.task.getSelectedCube()[0];
		int y = this.task.getSelectedCube()[1];
		int z = this.task.getSelectedCube()[2];
		
		Position pos;
		try {
			pos = new Position((double)x, (double)y, (double)z);
			return pos;
		} catch (IllegalPositionException e) {
			e.printStackTrace();
			return null;
		}	
	}

}
