package programmingLanguage.expressions;

import hillbillies.model.Unit;
import hillbillies.part3.programs.SourceLocation;

public class ThisUnitExpression extends UnitExpression{

	public ThisUnitExpression(String name, SourceLocation source) {
		super(name, source);
	}
	
	@Override
	public Unit execute() throws IllegalStateException{
		if(this.isTerminated())
			throw new IllegalStateException("Terminated expression!");
		return this.task.getAssignedUnit();
	}

}
