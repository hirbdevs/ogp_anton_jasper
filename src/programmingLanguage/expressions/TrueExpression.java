package programmingLanguage.expressions;

import hillbillies.part3.programs.SourceLocation;

public class TrueExpression extends Expression{

	public TrueExpression(String name, SourceLocation source) {
		super(name, source);	
	}
	
	@Override
	public Boolean execute() throws IllegalStateException{
		if(this.isTerminated())
			throw new IllegalStateException("Terminated expression!");
		return true;
	}

}
