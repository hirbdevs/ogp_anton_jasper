package programmingLanguage.expressions;

import hillbillies.model.Unit;
import hillbillies.part3.programs.SourceLocation;
import programmingLanguage.ExpressionException;

public abstract class UnitExpression extends Expression{

	public UnitExpression(String name, SourceLocation source) {
		super(name, source);
	}
	
	@Override
	public abstract Unit execute() throws ExpressionException;
	
}
