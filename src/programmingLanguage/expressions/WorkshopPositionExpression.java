package programmingLanguage.expressions;

import hillbillies.exceptions.TaskException;
import hillbillies.exceptions.UnitException;
import hillbillies.model.Position;
import hillbillies.part3.programs.SourceLocation;
import programmingLanguage.ExpressionException;

public class WorkshopPositionExpression extends PositionExpression{

	public WorkshopPositionExpression(String name, SourceLocation source){
		super(name, source);
	}
	
	@Override
	public Position execute() throws IllegalStateException, ExpressionException{
		
		if(this.isTerminated())
			throw new IllegalStateException("Terminated expression!");
		try {
			return this.task.getAssignedUnit().getClosestWorkshop().getPosition();
		} catch (TaskException | UnitException e) {
			throw new ExpressionException(e.getMessage());
		}
	}
	
	
}
